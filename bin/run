#!/usr/bin/env ruby
# frozen_string_literal: true

require 'bundler/setup'
require 'logger'
require 'pry'

require 'selenium-webdriver'

require_relative '../lib/http_client'
require_relative '../lib/schema_validator'
require_relative '../lib/web_driver'

require_relative '../app/extractor'
require_relative '../app/transformer'
require_relative '../app/loader'
require_relative '../app/job'

# ------------------------------------------------------------------------------
# Setup

logger = Logger.new($stdout)
logger.level = Logger::DEBUG

# ------------------------------------------------------------------------------
# Args

args = { only: nil, geo: nil }
ARGV.each do |arg|
  if (match = /^ONLY=(.*?)$/.match(arg))
    if match[1] != ''
      args[:only] = match[1].split(',')
    else
      args[:only] = []
    end
  end

  if (match = /^GEO=(.*?)$/.match(arg))
    if match[1] != ''
      args[:geo] = match[1].split(',')
    else
      args[:geo] = []
    end
  end
end

# ------------------------------------------------------------------------------
# Jobs

jobs = [
  {
    name: 'symbol',
    job:  Job::SymbolJob.new(logger)
  },
  {
    name: 'metadata',
    job:  Job::MetadataJob.new(logger) # Requires `symbol`
  },
  {
    name: 'dividend',
    job:  Job::DividendJob.new(logger) # Requires `symbol`
  },
  {
    name: 'calendar',
    job:  Job::CalendarJob.new(logger) # Requires `symbol`
  },
  {
    name: 'price',
    job:  Job::PriceJob.new(logger) # Requires `symbol`
  }
]

# ------------------------------------------------------------------------------
# Run

jobs.each do |instance|
  if args[:only].include?(instance[:name]) || args[:only].empty?
    instance[:job].call(args[:geo])
  end
end
