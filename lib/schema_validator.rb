# frozen_string_literal: true

require 'json_schemer'
require 'pathname'
require 'pp'

class SchemaValidator
  def initialize(logger, input: nil, output: nil)
    input_path  = input  && Pathname.new(File.join(Dir.pwd, input))
    output_path = output && Pathname.new(File.join(Dir.pwd, output))

    @input  = input  && JSONSchemer.schema(input_path)
    @output = output && JSONSchemer.schema(output_path)

    @logger = logger
  end

  def validate_input!(data)
    raise 'Validation error: input schema not defined!' unless @input

    validate!(data, @input, 'input')
  end

  def validate_output!(data)
    raise 'Validation error: output schema not defined!' unless @output

    validate!(data, @output, 'output')
  end

  private

  def validate!(data, schema, type)
    errors = schema.validate(data).to_a
    return self unless errors.any?

    @logger.error('Schema validation failed!')

    message = StringIO.new
    PP.pp(errors, message)
    @logger.debug("Schema errors:\n#{message.string}")

    raise "Validation error: data does not match #{type} schema!"
  end
end
