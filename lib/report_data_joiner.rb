# frozen_string_literal: true

require 'fileutils'
require 'pp'
require 'yaml'

require_relative '../scripts/valuation/lib/reconciler'

class ReportDataJoiner
  def call
    reconciler = Reconciler.new
    exchanges  = ['RO']
    data       = {}

    exchanges.each do |exchange|
      files = Dir.glob("data/valuation/#{exchange}/*/*/*")

      files.each do |file|
        parts   = file.split('/')
        symbol  = parts[3]
        year    = parts[4]
        quarter = parts[5].split('.')[0]

        raw  = File.read(file)
        yml  = YAML.safe_load(raw)

        data[exchange] = {} unless data[exchange]
        data[exchange][symbol] = {} unless data[exchange][symbol]
        data[exchange][symbol][year] = {} unless data[exchange][symbol][year]
        data[exchange][symbol][year][quarter] = yml

        reconciler.call(yml)
      end
    end

    data
  end
end
