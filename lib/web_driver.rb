class WebDriver
  def initialize
    @driver = nil
  end

  def navigate
    driver.navigate
  end

  def find_element(type, selector)
    driver.find_element(type, selector)
  end

  def find_elements(type, selector)
    driver.find_elements(type, selector)
  end

  def current_url
    driver.current_url
  end

  def execute_script(script, element)
    driver.execute_script(script, element)
  end

  def scroll_to_view(element)
    driver.execute_script("arguments[0].scrollIntoView();", element)
  end

  def reset!
    @driver = nil
  end

  def with_retries(total = 3, delay: 1, default: nil)
    retries = 0

    begin
      yield
    rescue Selenium::WebDriver::Error::NoSuchElementError
      default
    rescue Net::ReadTimeout,
           Selenium::WebDriver::Error::UnknownError,
           Selenium::WebDriver::Error::InvalidSessionIdError => e
      pp e
      puts e.backtrace

      retries += 1
      reset!

      sleep delay
      retry unless retries > total

      default
    rescue StandardError => e
      pp e
      puts e.backtrace

      retries += 1

      sleep delay
      retry unless retries > total

      default
    end
  end

  private

  def driver
    @driver ||= begin
      options = Selenium::WebDriver::Firefox::Options.new(args: ['-headless'])
      Selenium::WebDriver.for(:firefox, capabilities: options)
    end
  end
end
