# frozen_string_literal: true

require 'net/http'

class HTTPClient
  def initialize(headers: nil)
    @default_headers = {
      'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:88.0) Gecko/20100101 Firefox/88.0'
    }

    headers&.each_pair do |key, value|
      @default_headers[key] = value
    end
  end

  def get(url, params: nil, headers: nil)
    uri = URI(url)

    Net::HTTP.start(uri.host, uri.port, use_ssl: true) do |http|
      uri.query = URI.encode_www_form(params) if params
      request   = Net::HTTP::Get.new(uri)

      @default_headers.each_pair do |key, value|
        request[key] = value
      end

      headers&.each_pair do |key, value|
        request[key] = value
      end

      return http.request(request)
    end
  end

  def post(url, body: nil, headers: nil)
    uri = URI(url)

    Net::HTTP.start(uri.host, uri.port, use_ssl: true) do |http|
      request = Net::HTTP::Post.new(uri)

      @default_headers.each_pair do |key, value|
        request[key] = value
      end

      headers&.each_pair do |key, value|
        request[key] = value
      end

      return http.request(request, body)
    end
  end
end
