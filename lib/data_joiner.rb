# frozen_string_literal: true

require 'json_schemer'

class DataJoiner
  def call(paths)
    schema_path = File.join(Dir.pwd, 'schemas', 'job-data-schema.json')
    schema      = JSONSchemer.schema(Pathname.new(schema_path))

    registry = {}

    paths.each do |path|
      file = File.join(Dir.pwd, 'artifacts', *path)
      text = File.read(file)
      data = JSON.parse(text)

      errors = schema.validate(data).to_a
      if errors.any?
        puts "Schema validation failed for #{path}"
        pp errors

        exit(1)
      end

      data.each do |element|
        id    = element['id']
        value = element['value']

        existing = registry[id]
        if existing
          existing.merge!(value)
        else
          registry[id] = value
        end
      end
    end

    registry
  end
end
