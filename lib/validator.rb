# frozen_string_literal: true

require 'json_schemer'
require 'pathname'
require 'pp'

class Validator
  def initialize(logger, input: nil, output: nil)
    @input  = input  && JSONSchemer.schema(Pathname.new(input))
    @output = output && JSONSchemer.schema(Pathname.new(output))

    @logger = logger
  end

  def input?
    !@input.nil?
  end

  def input!(data)
    raise 'Validation error: input schema not defined!' unless @input

    validate!(data, @input, 'input')
  end

  def output?
    !@output.nil?
  end

  def output!(data)
    raise 'Validation error: output schema not defined!' unless @output

    validate!(data, @output, 'output')
  end

  private

  def validate!(data, schema, type)
    errors = schema.validate(data).to_a
    return true unless errors.any?

    @logger.error('Schema validation failed!')

    message = StringIO.new
    PP.pp(errors, message)
    @logger.debug("Schema errors:\n#{message.string}")

    raise "Validation error: data does not match #{type} schema!"
  end
end
