#-------------------------------------------------------------------------------
# Base

FROM ruby:3.0.1-alpine

SHELL ["/bin/ash", "-c"]

#-------------------------------------------------------------------------------
# Working directory

RUN set -exo pipefail                   && \
                                           \
    echo "Create the working directory" && \
    mkdir /work

WORKDIR /work

#-------------------------------------------------------------------------------
# Packages

RUN set -exo pipefail              && \
                                      \
    echo 'Install system packages' && \
    apk add --update --no-cache \
      build-base                \
      firefox                   \
      less                      \
      shadow

RUN set -ex                                                              && \
                                                                            \
    echo "Installing geckodriver"                                        && \
                                                                            \
    SHA=ec164910a3de7eec71e596bd2a1814ae27ba4c9d112b611680a6470dbe2ce27b && \
    VERSION=v0.29.1                                                      && \
    PACKAGE=geckodriver-$VERSION-linux64.tar.gz                          && \
    BASE=https://github.com/mozilla/geckodriver/releases/download        && \
    DOWNLOAD=$BASE/$VERSION/$PACKAGE                                     && \
                                                                            \
    wget -O "$PACKAGE" "$DOWNLOAD"                                       && \
    sha256sum "$PACKAGE"                                                 && \
    echo "$SHA  $PACKAGE" | sha256sum -c -                               && \
    tar -xzf "$PACKAGE" -C /usr/local/bin                                && \
    rm -rf "$PACKAGE"

#-------------------------------------------------------------------------------
# User

ARG HOST_USER_UID=1000
ARG HOST_USER_GID=1000

RUN set -ex                                                 && \
                                                               \
    echo 'Creating notroot user and group from host'        && \
    groupadd -g $HOST_USER_GID notroot                      && \
    useradd -lm -u $HOST_USER_UID -g $HOST_USER_GID notroot && \
                                                               \
    echo 'Giving notroot permissions to /work'              && \
    chown -R notroot:notroot /work

USER notroot

#-------------------------------------------------------------------------------
# Gems

COPY --chown=notroot:notroot Gemfile* /

RUN set -ex                && \
                              \
    echo 'Installing gems' && \
    bundle install

#-------------------------------------------------------------------------------
# Command

CMD ["/bin/sh"]
