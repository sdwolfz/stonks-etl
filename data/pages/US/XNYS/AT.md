---
title: "ATLANTIC POWER CORP (AT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>ATLANTIC POWER CORP</td></tr>
    <tr><td>Symbol</td><td>AT</td></tr>
    <tr><td>Web</td><td><a href="https://www.atlanticpower.com">www.atlanticpower.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2015 | 0.107 |
| 2014 | 0.294 |
| 2013 | 0.165 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
