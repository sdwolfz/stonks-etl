---
title: "TIDEWATER INC NEW (TDW)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>TIDEWATER INC NEW</td></tr>
    <tr><td>Symbol</td><td>TDW</td></tr>
    <tr><td>Web</td><td><a href="https://www.tdw.com">www.tdw.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2015 | 1.0 |
| 2014 | 1.0 |
| 2013 | 0.5 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
