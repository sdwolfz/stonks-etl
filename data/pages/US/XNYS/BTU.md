---
title: "PEABODY ENERGY CO (BTU)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>PEABODY ENERGY CO</td></tr>
    <tr><td>Symbol</td><td>BTU</td></tr>
    <tr><td>Web</td><td><a href="https://www.peabodyenergy.com">www.peabodyenergy.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 2.41 |
| 2018 | 0.485 |
| 2015 | 0.039 |
| 2014 | 0.34 |
| 2013 | 0.17 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
