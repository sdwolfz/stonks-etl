---
title: "CHINA YUCHAI INTERNATIONAL (CYD)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>CHINA YUCHAI INTERNATIONAL</td></tr>
    <tr><td>Symbol</td><td>CYD</td></tr>
    <tr><td>Web</td><td><a href="https://www.cyilimited.com">www.cyilimited.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.85 |
| 2019 | 0.85 |
| 2018 | 2.21 |
| 2016 | 0.85 |
| 2015 | 1.1 |
| 2013 | 0.1 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
