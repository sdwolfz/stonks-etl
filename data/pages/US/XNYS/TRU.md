---
title: "TRANSUNION (TRU)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>TRANSUNION</td></tr>
    <tr><td>Symbol</td><td>TRU</td></tr>
    <tr><td>Web</td><td><a href="https://www.transunion.com">www.transunion.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.075 |
| 2020 | 0.3 |
| 2019 | 0.3 |
| 2018 | 0.225 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
