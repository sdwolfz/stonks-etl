---
title: "OTIS WORLDWIDE CORP (OTIS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>OTIS WORLDWIDE CORP</td></tr>
    <tr><td>Symbol</td><td>OTIS</td></tr>
    <tr><td>Web</td><td><a href="https://www.otis.com">www.otis.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.44 |
| 2020 | 0.6 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
