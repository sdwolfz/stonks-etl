---
title: " (WLKP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>WLKP</td></tr>
    <tr><td>Web</td><td><a href="https://www.wlkpartners.com">www.wlkpartners.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.942 |
| 2020 | 1.884 |
| 2019 | 1.801 |
| 2018 | 1.614 |
| 2017 | 1.441 |
| 2016 | 1.286 |
| 2015 | 1.148 |
| 2014 | 0.17 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
