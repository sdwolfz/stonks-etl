---
title: "AVANGRID INC (AGR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>AVANGRID INC</td></tr>
    <tr><td>Symbol</td><td>AGR</td></tr>
    <tr><td>Web</td><td><a href="https://www.avangrid.com">www.avangrid.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.88 |
| 2020 | 1.76 |
| 2019 | 1.76 |
| 2018 | 1.744 |
| 2017 | 1.728 |
| 2016 | 1.728 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
