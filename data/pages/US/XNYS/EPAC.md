---
title: "ENERPAC TOOL GROUP CORP (EPAC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>ENERPAC TOOL GROUP CORP</td></tr>
    <tr><td>Symbol</td><td>EPAC</td></tr>
    <tr><td>Web</td><td><a href="https://www.enerpactoolgroup.com">www.enerpactoolgroup.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.04 |
| 2019 | 0.04 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
