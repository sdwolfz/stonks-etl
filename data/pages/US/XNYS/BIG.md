---
title: "BIG LOTS INC (BIG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>BIG LOTS INC</td></tr>
    <tr><td>Symbol</td><td>BIG</td></tr>
    <tr><td>Web</td><td><a href="https://www.biglots.com">www.biglots.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.3 |
| 2020 | 1.2 |
| 2019 | 1.2 |
| 2018 | 1.2 |
| 2017 | 1.0 |
| 2016 | 0.84 |
| 2015 | 0.76 |
| 2014 | 0.51 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
