---
title: "HYATT HOTELS CORP (H)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>HYATT HOTELS CORP</td></tr>
    <tr><td>Symbol</td><td>H</td></tr>
    <tr><td>Web</td><td><a href="https://www.hyatt.com">www.hyatt.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.2 |
| 2019 | 0.76 |
| 2018 | 0.6 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
