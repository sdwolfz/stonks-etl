---
title: "GAMCO INVESTORS INC (GBL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>GAMCO INVESTORS INC</td></tr>
    <tr><td>Symbol</td><td>GBL</td></tr>
    <tr><td>Web</td><td><a href="https://www.gabelli.com">www.gabelli.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 2.02 |
| 2020 | 0.08 |
| 2019 | 0.08 |
| 2018 | 0.08 |
| 2017 | 0.1 |
| 2016 | 0.08 |
| 2015 | 0.28 |
| 2014 | 0.25 |
| 2013 | 0.12 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
