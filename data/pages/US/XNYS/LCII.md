---
title: "LCI INDUSTRIES (LCII)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>LCI INDUSTRIES</td></tr>
    <tr><td>Symbol</td><td>LCII</td></tr>
    <tr><td>Web</td><td><a href="https://www.lci1.com">www.lci1.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.75 |
| 2020 | 2.8 |
| 2019 | 2.55 |
| 2018 | 2.35 |
| 2017 | 2.05 |
| 2016 | 0.5 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
