---
title: "VISTRA CORP (VST)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>VISTRA CORP</td></tr>
    <tr><td>Symbol</td><td>VST</td></tr>
    <tr><td>Web</td><td><a href="https://www.vistracorp.com">www.vistracorp.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.3 |
| 2020 | 0.54 |
| 2019 | 0.5 |
| 2016 | 2.32 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
