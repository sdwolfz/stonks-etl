---
title: " (ALEX)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>ALEX</td></tr>
    <tr><td>Web</td><td><a href="https://www.alexanderbaldwin.com">www.alexanderbaldwin.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.31 |
| 2020 | 0.34 |
| 2019 | 0.69 |
| 2017 | 16.132 |
| 2016 | 0.25 |
| 2015 | 0.21 |
| 2014 | 0.17 |
| 2013 | 0.04 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
