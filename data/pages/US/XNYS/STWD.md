---
title: " (STWD)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>STWD</td></tr>
    <tr><td>Web</td><td><a href="https://www.starwoodpropertytrust.com">www.starwoodpropertytrust.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.48 |
| 2020 | 1.92 |
| 2019 | 1.92 |
| 2018 | 1.92 |
| 2017 | 1.92 |
| 2016 | 1.92 |
| 2015 | 1.92 |
| 2014 | 1.92 |
| 2013 | 0.92 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
