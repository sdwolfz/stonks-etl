---
title: "CAPRI HOLDINGS LTD (CPRI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>CAPRI HOLDINGS LTD</td></tr>
    <tr><td>Symbol</td><td>CPRI</td></tr>
    <tr><td>Web</td><td><a href="https://www.capriholdings.com">www.capriholdings.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
