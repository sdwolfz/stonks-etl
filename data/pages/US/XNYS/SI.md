---
title: "SILVERGATE CAPITAL CORPORATION (SI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>SILVERGATE CAPITAL CORPORATION</td></tr>
    <tr><td>Symbol</td><td>SI</td></tr>
    <tr><td>Web</td><td><a href="https://www.silvergatebank.com">www.silvergatebank.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2014 | 3.013 |
| 2013 | 3.006 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
