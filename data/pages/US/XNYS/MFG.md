---
title: " (MFG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>MFG</td></tr>
    <tr><td>Web</td><td><a href="https://www.mizuho-fg.co.jp">www.mizuho-fg.co.jp</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.107 |
| 2019 | 0.105 |
| 2018 | 0.102 |
| 2017 | 0.059 |
| 2016 | 0.119 |
| 2014 | 0.111 |
| 2013 | 0.11 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
