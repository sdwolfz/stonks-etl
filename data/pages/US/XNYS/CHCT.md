---
title: " (CHCT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>CHCT</td></tr>
    <tr><td>Web</td><td><a href="https://www.chct.reit">www.chct.reit</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.858 |
| 2020 | 1.685 |
| 2019 | 1.644 |
| 2018 | 1.606 |
| 2017 | 1.565 |
| 2016 | 1.524 |
| 2015 | 0.517 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
