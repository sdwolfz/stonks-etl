---
title: "MATADOR RESOURCES COMPANY (MTDR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>MATADOR RESOURCES COMPANY</td></tr>
    <tr><td>Symbol</td><td>MTDR</td></tr>
    <tr><td>Web</td><td><a href="https://www.matadorresources.com">www.matadorresources.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.05 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
