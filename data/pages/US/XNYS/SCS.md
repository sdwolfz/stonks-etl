---
title: "STEELCASE INC (SCS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>STEELCASE INC</td></tr>
    <tr><td>Symbol</td><td>SCS</td></tr>
    <tr><td>Web</td><td><a href="https://www.steelcase.com">www.steelcase.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.1 |
| 2020 | 0.37 |
| 2019 | 0.58 |
| 2018 | 0.54 |
| 2017 | 0.508 |
| 2016 | 0.592 |
| 2015 | 0.336 |
| 2014 | 0.42 |
| 2013 | 0.3 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
