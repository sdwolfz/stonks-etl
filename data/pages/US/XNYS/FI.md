---
title: "FRANKS INTERNATIONAL NV (FI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>FRANKS INTERNATIONAL NV</td></tr>
    <tr><td>Symbol</td><td>FI</td></tr>
    <tr><td>Web</td><td><a href="https://www.franksinternational.com">www.franksinternational.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2017 | 0.225 |
| 2016 | 0.45 |
| 2015 | 0.6 |
| 2014 | 0.45 |
| 2013 | 0.075 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
