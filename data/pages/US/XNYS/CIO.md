---
title: " (CIO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>CIO</td></tr>
    <tr><td>Web</td><td><a href="https://www.cityofficereit.com">www.cityofficereit.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.3 |
| 2020 | 0.685 |
| 2019 | 0.94 |
| 2018 | 0.94 |
| 2017 | 0.94 |
| 2016 | 0.94 |
| 2015 | 0.705 |
| 2014 | 0.653 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
