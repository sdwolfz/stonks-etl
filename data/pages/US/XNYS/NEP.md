---
title: " (NEP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>NEP</td></tr>
    <tr><td>Web</td><td><a href="https://www.nexteraenergypartners.com">www.nexteraenergypartners.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.252 |
| 2020 | 2.262 |
| 2019 | 1.966 |
| 2018 | 1.713 |
| 2017 | 1.49 |
| 2016 | 1.298 |
| 2015 | 0.905 |
| 2014 | 0.188 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
