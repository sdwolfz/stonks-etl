---
title: " (DLNG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>DLNG</td></tr>
    <tr><td>Web</td><td><a href="https://www.dynagaspartners.com">www.dynagaspartners.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 0.126 |
| 2018 | 1.173 |
| 2017 | 1.692 |
| 2016 | 1.692 |
| 2015 | 1.692 |
| 2014 | 1.295 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
