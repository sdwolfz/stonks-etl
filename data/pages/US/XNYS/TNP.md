---
title: "TSAKOS ENERGY NAVIGATION (TNP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>TSAKOS ENERGY NAVIGATION</td></tr>
    <tr><td>Symbol</td><td>TNP</td></tr>
    <tr><td>Web</td><td><a href="https://www.tenn.gr">www.tenn.gr</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.5 |
| 2019 | 0.1 |
| 2018 | 0.15 |
| 2017 | 0.2 |
| 2016 | 0.29 |
| 2015 | 0.24 |
| 2014 | 0.15 |
| 2013 | 0.1 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
