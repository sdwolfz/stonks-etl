---
title: " (AKO.A)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>AKO.A</td></tr>
    <tr><td>Web</td><td><a href="https://www.koandina.com">www.koandina.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.139 |
| 2020 | 0.504 |
| 2019 | 0.48 |
| 2018 | 0.559 |
| 2017 | 0.675 |
| 2016 | 0.48 |
| 2015 | 0.408 |
| 2014 | 0.333 |
| 2013 | 0.567 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
