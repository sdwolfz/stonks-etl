---
title: "MDH ACQUISITION CORP (MDH)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>MDH ACQUISITION CORP</td></tr>
    <tr><td>Symbol</td><td>MDH</td></tr>
    <tr><td>Web</td><td><a href="https://www.mclartydiversified.com">www.mclartydiversified.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
