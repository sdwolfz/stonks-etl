---
title: "APARTMENT INCOME REIT CORP (AIRC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>APARTMENT INCOME REIT CORP</td></tr>
    <tr><td>Symbol</td><td>AIRC</td></tr>
    <tr><td>Web</td><td><a href="https://www.aircommunities.com">www.aircommunities.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.86 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
