---
title: "FRONTLINE LTD (FRO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>FRONTLINE LTD</td></tr>
    <tr><td>Symbol</td><td>FRO</td></tr>
    <tr><td>Web</td><td><a href="https://www.frontline.bm">www.frontline.bm</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 1.6 |
| 2019 | 0.1 |
| 2017 | 0.3 |
| 2016 | 1.05 |
| 2015 | 0.25 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
