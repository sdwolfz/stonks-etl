---
title: "FB FINANCIAL CORPORATION (FBK)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>FB FINANCIAL CORPORATION</td></tr>
    <tr><td>Symbol</td><td>FBK</td></tr>
    <tr><td>Web</td><td><a href="https://www.firstbankonline.com">www.firstbankonline.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.22 |
| 2020 | 0.36 |
| 2019 | 0.32 |
| 2018 | 0.2 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
