---
title: " (OXLCM)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>OXLCM</td></tr>
    <tr><td>Web</td><td><a href="https://www.oxfordlanecapital.com">www.oxfordlanecapital.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.703 |
| 2020 | 1.688 |
| 2019 | 1.688 |
| 2018 | 1.688 |
| 2017 | 0.923 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
