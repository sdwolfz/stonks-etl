---
title: "MASIMO CORPORATION (MASI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>MASIMO CORPORATION</td></tr>
    <tr><td>Symbol</td><td>MASI</td></tr>
    <tr><td>Web</td><td><a href="https://www.masimo.com">www.masimo.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2012 | 1.0 |
| 2010 | 2.75 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
