---
title: "BANK PRINCETON NEW JERSEY (BPRN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>BANK PRINCETON NEW JERSEY</td></tr>
    <tr><td>Symbol</td><td>BPRN</td></tr>
    <tr><td>Web</td><td><a href="https://www.thebankofprinceton.com">www.thebankofprinceton.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.3 |
| 2020 | 0.4 |
| 2019 | 0.19 |
| 2018 | 0.03 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
