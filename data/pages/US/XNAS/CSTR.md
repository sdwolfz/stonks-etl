---
title: "CAPSTAR FINANCIAL HOLDINGS INC (CSTR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>CAPSTAR FINANCIAL HOLDINGS INC</td></tr>
    <tr><td>Symbol</td><td>CSTR</td></tr>
    <tr><td>Web</td><td><a href="https://www.capstarbank.com">www.capstarbank.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.11 |
| 2020 | 0.2 |
| 2019 | 0.19 |
| 2018 | 0.08 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
