---
title: "NASDAQ INC (NDAQ)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>NASDAQ INC</td></tr>
    <tr><td>Symbol</td><td>NDAQ</td></tr>
    <tr><td>Web</td><td><a href="https://www.nasdaq.com">www.nasdaq.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.03 |
| 2020 | 1.94 |
| 2019 | 1.85 |
| 2018 | 1.7 |
| 2017 | 1.46 |
| 2016 | 1.21 |
| 2015 | 0.9 |
| 2014 | 0.58 |
| 2013 | 0.52 |
| 2012 | 0.39 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
