---
title: "CHICKEN SOUP FOR THE SOUL ENTMT INC (CSSE)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>CHICKEN SOUP FOR THE SOUL ENTMT INC</td></tr>
    <tr><td>Symbol</td><td>CSSE</td></tr>
    <tr><td>Web</td><td><a href="https://www.cssentertainment.com">www.cssentertainment.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2018 | 0.45 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
