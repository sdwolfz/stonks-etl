---
title: "CHINA NATURAL RESOURCES INC (CHNR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>CHINA NATURAL RESOURCES INC</td></tr>
    <tr><td>Symbol</td><td>CHNR</td></tr>
    <tr><td>Web</td><td><a href="https://www.chnr.net">www.chnr.net</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
