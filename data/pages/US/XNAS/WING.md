---
title: "WINGSTOP INC (WING)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>WINGSTOP INC</td></tr>
    <tr><td>Symbol</td><td>WING</td></tr>
    <tr><td>Web</td><td><a href="https://www.wingstop.com">www.wingstop.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.28 |
| 2020 | 5.5 |
| 2019 | 0.4 |
| 2018 | 6.54 |
| 2017 | 0.14 |
| 2016 | 2.9 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
