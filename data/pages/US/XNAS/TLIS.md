---
title: "TALIS BIOMEDICAL CORP (TLIS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>TALIS BIOMEDICAL CORP</td></tr>
    <tr><td>Symbol</td><td>TLIS</td></tr>
    <tr><td>Web</td><td><a href="https://www.talis.bio">www.talis.bio</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
