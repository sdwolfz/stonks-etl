---
title: "DAVE & BUSTER'S ENTERTAINMENT INC (PLAY)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>DAVE & BUSTER'S ENTERTAINMENT INC</td></tr>
    <tr><td>Symbol</td><td>PLAY</td></tr>
    <tr><td>Web</td><td><a href="https://www.daveandbusters.com">www.daveandbusters.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.16 |
| 2019 | 0.46 |
| 2018 | 0.3 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
