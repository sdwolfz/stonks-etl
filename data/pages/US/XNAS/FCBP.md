---
title: "FIRST CHOICE BANCORP (FCBP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>FIRST CHOICE BANCORP</td></tr>
    <tr><td>Symbol</td><td>FCBP</td></tr>
    <tr><td>Web</td><td><a href="https://www.firstchoicebankca.com">www.firstchoicebankca.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.5 |
| 2020 | 1.0 |
| 2019 | 0.85 |
| 2018 | 0.6 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
