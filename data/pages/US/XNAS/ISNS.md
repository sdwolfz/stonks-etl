---
title: "IMAGE SENSING SYSTEMS INC (ISNS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>IMAGE SENSING SYSTEMS INC</td></tr>
    <tr><td>Symbol</td><td>ISNS</td></tr>
    <tr><td>Web</td><td><a href="https://www.imagesensing.com">www.imagesensing.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.12 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
