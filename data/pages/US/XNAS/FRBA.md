---
title: "FIRST BANK WILLIAMSTOWN (NJ) (FRBA)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>FIRST BANK WILLIAMSTOWN (NJ)</td></tr>
    <tr><td>Symbol</td><td>FRBA</td></tr>
    <tr><td>Web</td><td><a href="https://www.firstbanknj.com">www.firstbanknj.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.06 |
| 2020 | 0.12 |
| 2019 | 0.12 |
| 2018 | 0.12 |
| 2017 | 0.08 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
