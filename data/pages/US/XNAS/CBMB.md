---
title: "CBM BANCORP INC (CBMB)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>CBM BANCORP INC</td></tr>
    <tr><td>Symbol</td><td>CBMB</td></tr>
    <tr><td>Web</td><td><a href="https://www.chesapeakebank.com">www.chesapeakebank.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.5 |
| 2020 | 0.5 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
