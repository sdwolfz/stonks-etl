---
title: " (LMRKP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>LMRKP</td></tr>
    <tr><td>Web</td><td><a href="https://www.landmarkmlp.com">www.landmarkmlp.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.5 |
| 2020 | 2.0 |
| 2019 | 2.0 |
| 2018 | 2.0 |
| 2017 | 2.0 |
| 2016 | 1.561 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
