---
title: "JERASH HOLDINGS US INC (JRSH)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>JERASH HOLDINGS US INC</td></tr>
    <tr><td>Symbol</td><td>JRSH</td></tr>
    <tr><td>Web</td><td><a href="https://www.jerashholdings.com">www.jerashholdings.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.05 |
| 2020 | 0.2 |
| 2019 | 0.2 |
| 2018 | 0.05 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
