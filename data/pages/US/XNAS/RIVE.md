---
title: "RIVERVIEW FINANCIAL CORP NEW (RIVE)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>RIVERVIEW FINANCIAL CORP NEW</td></tr>
    <tr><td>Symbol</td><td>RIVE</td></tr>
    <tr><td>Web</td><td><a href="https://www.riverviewbankpa.com">www.riverviewbankpa.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.15 |
| 2019 | 0.35 |
| 2018 | 0.2 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
