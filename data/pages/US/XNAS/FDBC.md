---
title: "FIDELITY D&D BANCORP (FDBC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>FIDELITY D&D BANCORP</td></tr>
    <tr><td>Symbol</td><td>FDBC</td></tr>
    <tr><td>Web</td><td><a href="https://www.bankatfidelity.com">www.bankatfidelity.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.6 |
| 2020 | 1.14 |
| 2019 | 1.06 |
| 2018 | 0.98 |
| 2017 | 0.26 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
