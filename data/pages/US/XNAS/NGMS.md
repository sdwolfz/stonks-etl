---
title: "NEOGAMES S A (NGMS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>NEOGAMES S A</td></tr>
    <tr><td>Symbol</td><td>NGMS</td></tr>
    <tr><td>Web</td><td><a href="https://www.neogames.com">www.neogames.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
