---
title: "GIGAMEDIA LIMITED (GIGM)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>GIGAMEDIA LIMITED</td></tr>
    <tr><td>Symbol</td><td>GIGM</td></tr>
    <tr><td>Web</td><td><a href="https://www.gigamedia.com">www.gigamedia.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2002 | 2.0 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
