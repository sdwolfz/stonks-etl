---
title: "AMERISAFE INC (AMSF)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>AMERISAFE INC</td></tr>
    <tr><td>Symbol</td><td>AMSF</td></tr>
    <tr><td>Web</td><td><a href="https://www.amerisafe.com">www.amerisafe.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.58 |
| 2020 | 4.58 |
| 2019 | 4.5 |
| 2018 | 4.38 |
| 2017 | 4.3 |
| 2016 | 3.97 |
| 2015 | 3.6 |
| 2014 | 1.98 |
| 2013 | 0.32 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
