---
title: "SOUTH PLAINS FINANCIAL INC (SPFI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>SOUTH PLAINS FINANCIAL INC</td></tr>
    <tr><td>Symbol</td><td>SPFI</td></tr>
    <tr><td>Web</td><td><a href="https://www.spfi.bank">www.spfi.bank</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.12 |
| 2020 | 0.14 |
| 2019 | 0.06 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
