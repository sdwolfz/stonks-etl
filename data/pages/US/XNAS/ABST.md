---
title: "ABSOLUTE SOFTWARE CORP (ABST)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>ABSOLUTE SOFTWARE CORP</td></tr>
    <tr><td>Symbol</td><td>ABST</td></tr>
    <tr><td>Web</td><td><a href="https://www.absolute.com">www.absolute.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.14 |
| 2020 | 0.061 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
