---
title: "Tritax Euro. (EBOX)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Tritax Euro.</td></tr>
    <tr><td>Symbol</td><td>EBOX</td></tr>
    <tr><td>Web</td><td><a href="https://www.tritax.co.uk">www.tritax.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 4.4 |
| 2019 | 3.4 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
