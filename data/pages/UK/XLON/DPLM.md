---
title: "Diploma (DPLM)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Diploma</td></tr>
    <tr><td>Symbol</td><td>DPLM</td></tr>
    <tr><td>Web</td><td><a href="https://www.diplomaplc.com">www.diplomaplc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 30.0 |
| 2019 | 29.0 |
| 2018 | 25.5 |
| 2017 | 23.0 |
| 2016 | 20.0 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
