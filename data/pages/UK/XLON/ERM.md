---
title: "Euromoney (ERM)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Euromoney</td></tr>
    <tr><td>Symbol</td><td>ERM</td></tr>
    <tr><td>Web</td><td><a href="https://www.euromoneyplc.com">www.euromoneyplc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 11.4 |
| 2019 | 33.1 |
| 2018 | 32.5 |
| 2017 | 30.6 |
| 2016 | 23.4 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
