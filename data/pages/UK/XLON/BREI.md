---
title: "Bmo Real Est (BREI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Bmo Real Est</td></tr>
    <tr><td>Symbol</td><td>BREI</td></tr>
    <tr><td>Web</td><td><a href="https://www.bmogam.com">www.bmogam.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 3.75 |
| 2019 | 5.0 |
| 2018 | 5.0 |
| 2017 | 5.0 |
| 2016 | 5.0 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
