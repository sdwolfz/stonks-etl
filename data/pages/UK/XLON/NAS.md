---
title: "North Atl.smlr (NAS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>North Atl.smlr</td></tr>
    <tr><td>Symbol</td><td>NAS</td></tr>
    <tr><td>Web</td><td><a href="https://www.navalue.co.uk">www.navalue.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 30.0 |
| 2019 | 30.0 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
