---
title: "Fidelity China Special Situations PLC (FCSS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Fidelity China Special Situations PLC</td></tr>
    <tr><td>Symbol</td><td>FCSS</td></tr>
    <tr><td>Web</td><td><a href="https://www.fidelityinvestmenttrusts.com">www.fidelityinvestmenttrusts.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 4.25 |
| 2019 | 3.85 |
| 2018 | 3.5 |
| 2017 | 2.5 |
| 2016 | 1.8 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
