---
title: "Morrison (WM) (MRW)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Morrison (WM)</td></tr>
    <tr><td>Symbol</td><td>MRW</td></tr>
    <tr><td>Web</td><td><a href="https://www.morrisons.co.uk">www.morrisons.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 8.77 |
| 2019 | 8.6 |
| 2018 | 8.43 |
| 2017 | 5.43 |
| 2016 | 5.0 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
