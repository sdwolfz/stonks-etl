---
title: "Wetherspoon (J.D) (JDW)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Wetherspoon (J.D)</td></tr>
    <tr><td>Symbol</td><td>JDW</td></tr>
    <tr><td>Web</td><td><a href="https://www.jdwetherspoon.co.uk">www.jdwetherspoon.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 12.0 |
| 2018 | 12.0 |
| 2017 | 12.0 |
| 2016 | 12.0 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
