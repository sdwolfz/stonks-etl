---
title: "Gym Grp (GYM)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Gym Grp</td></tr>
    <tr><td>Symbol</td><td>GYM</td></tr>
    <tr><td>Web</td><td><a href="https://www.thegymgroup.com">www.thegymgroup.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 1.6 |
| 2018 | 1.3 |
| 2017 | 1.2 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
