---
title: "Great Portland (GPOR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Great Portland</td></tr>
    <tr><td>Symbol</td><td>GPOR</td></tr>
    <tr><td>Web</td><td><a href="https://www.gpe.co.uk">www.gpe.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 12.6 |
| 2019 | 12.2 |
| 2018 | 43.45 |
| 2017 | 11.72 |
| 2016 | 10.67 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
