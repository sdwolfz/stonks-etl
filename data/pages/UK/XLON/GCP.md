---
title: "GCP Infrastructure Investments (GCP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>GCP Infrastructure Investments</td></tr>
    <tr><td>Symbol</td><td>GCP</td></tr>
    <tr><td>Web</td><td><a href="https://www.graviscapital.com">www.graviscapital.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 7.6 |
| 2019 | 7.6 |
| 2018 | 7.6 |
| 2017 | 7.6 |
| 2016 | 7.6 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
