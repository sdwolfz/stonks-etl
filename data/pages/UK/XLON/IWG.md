---
title: "IWG (IWG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>IWG</td></tr>
    <tr><td>Symbol</td><td>IWG</td></tr>
    <tr><td>Web</td><td><a href="https://www.iwgplc.com">www.iwgplc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 6.95 |
| 2018 | 6.3 |
| 2017 | 5.7 |
| 2016 | 5.1 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
