---
title: "Jpmorg.gbl.g&i (JGGI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Jpmorg.gbl.g&i</td></tr>
    <tr><td>Symbol</td><td>JGGI</td></tr>
    <tr><td>Web</td><td><a href="https://www.jpmfoverseas.com">www.jpmfoverseas.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 13.04 |
| 2019 | 12.52 |
| 2018 | 12.16 |
| 2017 | 6.6 |
| 2016 | 3.2 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
