---
title: "Smiths Group (SMIN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Smiths Group</td></tr>
    <tr><td>Symbol</td><td>SMIN</td></tr>
    <tr><td>Web</td><td><a href="https://www.smiths.com">www.smiths.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 35.0 |
| 2019 | 45.9 |
| 2018 | 44.55 |
| 2017 | 43.25 |
| 2016 | 42.0 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
