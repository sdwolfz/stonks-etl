---
title: "Bellway (BWY)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Bellway</td></tr>
    <tr><td>Symbol</td><td>BWY</td></tr>
    <tr><td>Web</td><td><a href="https://www.bellway.co.uk">www.bellway.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 97.7 |
| 2019 | 150.4 |
| 2018 | 143.0 |
| 2017 | 122.0 |
| 2016 | 108.0 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
