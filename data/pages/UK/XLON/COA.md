---
title: "Coats (COA)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Coats</td></tr>
    <tr><td>Symbol</td><td>COA</td></tr>
    <tr><td>Web</td><td><a href="https://www.coats.com">www.coats.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 1.3 |
| 2019 | 0.55 |
| 2018 | 1.66 |
| 2017 | 1.44 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
