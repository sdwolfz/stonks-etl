---
title: "Arrow Global (ARW)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Arrow Global</td></tr>
    <tr><td>Symbol</td><td>ARW</td></tr>
    <tr><td>Web</td><td><a href="https://www.arrowglobal.net">www.arrowglobal.net</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 13.1 |
| 2018 | 12.7 |
| 2017 | 11.3 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
