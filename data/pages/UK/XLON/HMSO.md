---
title: "Hammerson (HMSO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Hammerson</td></tr>
    <tr><td>Symbol</td><td>HMSO</td></tr>
    <tr><td>Web</td><td><a href="https://www.hammerson.com">www.hammerson.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 25.9 |
| 2018 | 25.9 |
| 2017 | 25.5 |
| 2016 | 24.0 |
| 2015 | 22.3 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
