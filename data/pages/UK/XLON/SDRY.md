---
title: "Superdry (SDRY)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Superdry</td></tr>
    <tr><td>Symbol</td><td>SDRY</td></tr>
    <tr><td>Web</td><td><a href="https://www.corporate.superdry.com">www.corporate.superdry.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 2.0 |
| 2019 | 36.5 |
| 2018 | 31.2 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
