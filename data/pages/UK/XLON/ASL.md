---
title: "Aberforth Smaller Companies Trust Plc (ASL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Aberforth Smaller Companies Trust Plc</td></tr>
    <tr><td>Symbol</td><td>ASL</td></tr>
    <tr><td>Web</td><td><a href="https://www.aberforth.co.uk">www.aberforth.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2018 | 38.0 |
| 2017 | 35.5 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
