---
title: "Xps Pensions (XPS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Xps Pensions</td></tr>
    <tr><td>Symbol</td><td>XPS</td></tr>
    <tr><td>Web</td><td><a href="https://www.xpsgroup.com">www.xpsgroup.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 6.6 |
| 2019 | 6.6 |
| 2018 | 6.3 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
