---
title: "B&M (BME)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>B&M</td></tr>
    <tr><td>Symbol</td><td>BME</td></tr>
    <tr><td>Web</td><td><a href="https://www.bmstores.co.uk">www.bmstores.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 8.1 |
| 2019 | 7.6 |
| 2019 | 7.6 |
| 2018 | 7.2 |
| 2017 | 11.12 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
