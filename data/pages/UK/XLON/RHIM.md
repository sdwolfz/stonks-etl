---
title: "RHI Magnesita (RHIM)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>RHI Magnesita</td></tr>
    <tr><td>Symbol</td><td>RHIM</td></tr>
    <tr><td>Web</td><td><a href="https://www.rhi-ag.com">www.rhi-ag.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 150.0 |
| 2019 | 50.0 |
| 2018 | 150.0 |
| 2017 | 75.0 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
