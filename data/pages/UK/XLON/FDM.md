---
title: "FDM Group (FDM)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>FDM Group</td></tr>
    <tr><td>Symbol</td><td>FDM</td></tr>
    <tr><td>Web</td><td><a href="https://www.fdmgroup.com">www.fdmgroup.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 34.5 |
| 2018 | 30.0 |
| 2017 | 26.0 |
| 2016 | 19.6 |
| 2015 | 21.5 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
