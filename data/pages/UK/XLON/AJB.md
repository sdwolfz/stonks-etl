---
title: "Aj Bell (AJB)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Aj Bell</td></tr>
    <tr><td>Symbol</td><td>AJB</td></tr>
    <tr><td>Web</td><td><a href="https://www.ajbell.co.uk">www.ajbell.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 6.16 |
| 2019 | 4.83 |
| 2018 | 55.0 |
| 2017 | 28.25 |
| 2016 | 25.75 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
