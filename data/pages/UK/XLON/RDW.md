---
title: "Redrow (RDW)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Redrow</td></tr>
    <tr><td>Symbol</td><td>RDW</td></tr>
    <tr><td>Web</td><td><a href="https://www.redrow.co.uk">www.redrow.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 59.0 |
| 2018 | 28.0 |
| 2017 | 17.0 |
| 2016 | 10.0 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
