---
title: "Hikma Pharmaceuticals (HIK)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Hikma Pharmaceuticals</td></tr>
    <tr><td>Symbol</td><td>HIK</td></tr>
    <tr><td>Web</td><td><a href="https://www.hikma.com">www.hikma.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 50.0 |
| 2019 | 44.0 |
| 2018 | 38.0 |
| 2017 | 34.0 |
| 2016 | 33.0 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
