---
title: "HSBC Holdings (HSBA)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>HSBC Holdings</td></tr>
    <tr><td>Symbol</td><td>HSBA</td></tr>
    <tr><td>Web</td><td><a href="https://www.hsbc.com">www.hsbc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 15.0 |
| 2019 | 30.0 |
| 2018 | 51.0 |
| 2017 | 51.0 |
| 2016 | 51.0 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
