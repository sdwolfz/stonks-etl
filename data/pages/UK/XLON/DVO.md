---
title: "Devro (DVO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Devro</td></tr>
    <tr><td>Symbol</td><td>DVO</td></tr>
    <tr><td>Web</td><td><a href="https://www.devro.com">www.devro.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 9.0 |
| 2018 | 9.0 |
| 2017 | 8.8 |
| 2016 | 8.8 |
| 2015 | 8.8 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
