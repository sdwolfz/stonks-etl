---
title: "4Imprint (FOUR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>4Imprint</td></tr>
    <tr><td>Symbol</td><td>FOUR</td></tr>
    <tr><td>Web</td><td><a href="https://investors.4imprint.com">investors.4imprint.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2018 | 70.0 |
| 2017 | 118.1 |
| 2016 | 52.5 |
| 2016 | 38.89 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
