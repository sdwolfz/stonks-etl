---
title: "Ultra Electronics (ULE)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Ultra Electronics</td></tr>
    <tr><td>Symbol</td><td>ULE</td></tr>
    <tr><td>Web</td><td><a href="https://www.ultra-electronics.com">www.ultra-electronics.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 54.2 |
| 2018 | 51.6 |
| 2017 | 49.6 |
| 2016 | 47.8 |
| 2015 | 46.1 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
