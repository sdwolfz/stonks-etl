---
title: "Ibstock (IBST)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Ibstock</td></tr>
    <tr><td>Symbol</td><td>IBST</td></tr>
    <tr><td>Web</td><td><a href="https://www.ibstockplc.co.uk">www.ibstockplc.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 9.7 |
| 2018 | 9.5 |
| 2017 | 9.1 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
