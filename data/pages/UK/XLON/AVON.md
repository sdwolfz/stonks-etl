---
title: "Avon Rubber (AVON)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Avon Rubber</td></tr>
    <tr><td>Symbol</td><td>AVON</td></tr>
    <tr><td>Web</td><td><a href="https://www.avon-rubber.com">www.avon-rubber.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 27.08 |
| 2019 | 20.83 |
| 2018 | 16.02 |
| 2017 | 10.43 |
| 2016 | 8.02 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
