---
title: "Workspace (WKP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Workspace</td></tr>
    <tr><td>Symbol</td><td>WKP</td></tr>
    <tr><td>Web</td><td><a href="https://www.workspacegroup.co.uk">www.workspacegroup.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 36.16 |
| 2019 | 32.87 |
| 2018 | 27.39 |
| 2017 | 21.07 |
| 2016 | 15.05 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
