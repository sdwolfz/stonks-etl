---
title: "TERAPLAST SA (TRP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xbse/">XBSE</a></td></tr>
    <tr><td>Name</td><td>TERAPLAST SA</td></tr>
    <tr><td>Symbol</td><td>TRP</td></tr>
    <tr><td>Web</td><td><a href="https://www.teraplast.ro">www.teraplast.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.026 |
| 2017 | 0.0118 |
| 2016 | 0.0169 |
| 2015 | 0.021 |

### Reports

<style>
  .reports-table {
    width: 100%;
    display: block;
    overflow-x: auto;
    overflow-y: scroll;
    scrollbar-width: none;
    max-height: 50rem;
    border: 0.5rem solid #D8D8D8;
  }
  .reports-table table {
    border: none;
    padding: 0;
  }
  .reports-table th {
    border: none;
    padding: 0;
  }
  .reports-table td {
    border: none
    padding: 0;
  }

  table.reports {
    border-collapse: collapse;
  }

  table.reports thead th, table.reports tbody td {
    padding: 3px 5px;
  }

  table.reports td {
    border: 0px solid #FFFFFF;
  }

  table.reports thead .header-top {
    background-color: #000000;
    color: #FFFFFF;
  }

  table.reports thead .header-assets {
    background-color: #3D85C6;
    color: #FFFFFF;
  }

  table.reports thead .header-assets-non {
    background-color: #A2C4C9;
  }

  table.reports thead .header-assets-cur {
    background-color: #9FC5E8;
  }

  table.reports thead .header-assets-tot {
    background-color: #3D85C6;
    color: #FFFFFF;
  }

  table.reports thead .header-equity {
    background-color: #6AA84F;
    color: #FFFFFF;
  }

  table.reports thead .header-lia-non {
    background-color: #D5A6BD;
  }

  table.reports thead .header-lia-cur {
    background-color: #EA9999;
  }

  table.reports thead .header-lia-tot {
    background-color: #A64D79;
    color: #FFFFFF;
  }

  table.reports thead .header-equity {
    background-color: #6AA84F;
    color: #FFFFFF;
  }

  table.reports thead .header-liabilities {
    background-color: #A64D79;
    color: #FFFFFF;
  }

  table.reports thead .header-revenue {
    background-color: #674EA6;
    color: #FFFFFF;
  }

  table.reports thead .header-cashflow {
    background-color: #E69138;
    color: #FFFFFF;
  }

  table.reports tbody td.number {
    text-align: right;
  }

  table.reports tbody td.year-even, table.reports tbody td.quarter-even {
    text-align: center;
    background-color: #B7B7B7;
  }

  table.reports tbody td.year-odd, table.reports tbody td.quarter-odd {
    text-align: center;
    background-color: #D9D9D9;
  }

  table.reports tbody td.assets-non-h2 {
    background-color: #D0E0E3;
  }
  table.reports tbody td.assets-cur-h2 {
    background-color: #CFE2F3;
  }
  table.reports tbody td.assets-tot-h2 {
    background-color: #CFE2F3;
  }
  table.reports tbody td.assets-non-t3 {
    background-color: #A2C4C9;
  }
  table.reports tbody td.assets-cur-t3 {
    background-color: #9FC5E8;
  }
  table.reports tbody td.assets-tot-t3 {
    background-color: #9FC5E8;
  }
  table.reports tbody td.assets-non-a4 {
    background-color: #76A5AF;
  }
  table.reports tbody td.assets-cur-a4 {
    background-color: #6FA8DC;
  }
  table.reports tbody td.assets-tot-a4 {
    background-color: #6FA8DC;
  }

  table.reports tbody td.equity-h2 {
    background-color: #D9EAD3;
  }
  table.reports tbody td.equity-t3 {
    background-color: #B6D7A8;
  }
  table.reports tbody td.equity-a4 {
    background-color: #93C47D;
  }

  table.reports tbody td.liabilities-non-h2 {
    background-color: #EAD1DC;
  }
  table.reports tbody td.liabilities-cur-h2 {
    background-color: #F4CCCC;
  }
  table.reports tbody td.liabilities-tot-h2 {
    background-color: #F4CCCC;
  }
  table.reports tbody td.liabilities-non-t3 {
    background-color: #D5A6BD;
  }
  table.reports tbody td.liabilities-cur-t3 {
    background-color: #EA9999;
  }
  table.reports tbody td.liabilities-tot-t3 {
    background-color: #EA9999;
  }
  table.reports tbody td.liabilities-non-a4 {
    background-color: #C27BA0;
  }
  table.reports tbody td.liabilities-cur-a4 {
    background-color: #E06666;
  }
  table.reports tbody td.liabilities-tot-a4 {
    background-color: #E06666;
  }

  table.reports tbody td.revenue-h2 {
    background-color: #D9D2E9;
  }
  table.reports tbody td.revenue-t3 {
    background-color: #B4A7D6;
  }
  table.reports tbody td.revenue-a4 {
    background-color: #8E7CC3;
  }

  table.reports tbody td.cashflow-h2 {
    background-color: #FCE5CD;
  }
  table.reports tbody td.cashflow-t3 {
    background-color: #F9CB9C;
  }
  table.reports tbody td.cashflow-a4 {
    background-color: #F6B26B;
  }
</style>

<div class="reports-table">
  <table class="reports">
    <thead>
      <tr>
        <th rowspan="3" class="header-top">Year</th>
        <th rowspan="3" class="header-top">Quarter</th>
        <th colspan="7" class="header-top">Balance</th>
        <th colspan="9" class="header-top">Revenue</th>
        <th colspan="8" class="header-top">Cashflow</th>
      </tr>
      <tr>
        <th colspan="3" class="header-assets">Assets</th>
        <th             class="header-equity">Equity</th>
        <th colspan="3" class="header-liabilities">Liabilities</th>
        <th rowspan="2" class="header-revenue">Sales</th>
        <th rowspan="2" class="header-revenue">Operating Expenses</th>
        <th rowspan="2" class="header-revenue">Operating Profit</th>
        <th rowspan="2" class="header-revenue">Financial Resut</th>
        <th rowspan="2" class="header-revenue">Profit Before Taxes</th>
        <th rowspan="2" class="header-revenue">Tax On Gains</th>
        <th rowspan="2" class="header-revenue">Net Profit</th>
        <th rowspan="2" class="header-revenue">Other</th>
        <th rowspan="2" class="header-revenue">Total Result</th>
        <th rowspan="2" class="header-cashflow">Operating</th>
        <th rowspan="2" class="header-cashflow">Investment</th>
        <th rowspan="2" class="header-cashflow">Financing</th>
        <th rowspan="2" class="header-cashflow">Total</th>
        <th rowspan="2" class="header-cashflow">Cash at Start</th>
        <th rowspan="2" class="header-cashflow">Cash at End</th>
        <th rowspan="2" class="header-cashflow">Capex</th>
        <th rowspan="2" class="header-cashflow">Free Cash Flow</th>
      </tr>
      <tr>
        <th class="header-assets-non">Noncurrent</th>
        <th class="header-assets-cur">Current</th>
        <th class="header-assets-tot">Total</th>
        <th class="header-equity">Total</th>
        <th class="header-lia-non">Noncurrent</th>
        <th class="header-lia-cur">Current</th>
        <th class="header-lia-tot">Total</th>
      <tr>
    </thead>
    <tbody>
      <tr class="row-q1">
        <td rowspan="1" class="year-even">2021</td>
        <td class="quarter-even">Q1</td>
        <td class="number assets-non-q1">178,591,260</td>
        <td class="number assets-cur-q1">550,187,222</td>
        <td class="number assets-tot-q1">728,778,482</td>
        <td class="number equity-q1">535,163,056</td>
        <td class="number liabilities-non-q1">59,577,712</td>
        <td class="number liabilities-cur-q1">134,037,713</td>
        <td class="number liabilities-tot-q1">193,615,425</td>
        <td class="number revenue-q1">108,648,254</td>
        <td class="number revenue-q1">-95,449,418</td>
        <td class="number revenue-q1">13,198,836</td>
        <td class="number revenue-q1">-1,429,726</td>
        <td class="number revenue-q1">11,769,110</td>
        <td class="number revenue-q1">-112,229</td>
        <td class="number revenue-q1">11,656,881</td>
        <td class="number revenue-q1">188,987,708</td>
        <td class="number revenue-q1">200,644,589</td>
        <td class="number cashflow-q1">-13,683,870</td>
        <td class="number cashflow-q1">363,783,377</td>
        <td class="number cashflow-q1">-77,298,517</td>
        <td class="number cashflow-q1">272,800,990</td>
        <td class="number cashflow-q1">16,620,936</td>
        <td class="number cashflow-q1">289,421,926</td>
        <td class="number cashflow-q1">-13,623,237</td>
        <td class="number cashflow-q1">-27,307,107</td>
      </tr>
      <tr class="row-a4">
        <td rowspan="4" class="year-odd">2020</td>
        <td class="quarter-odd">A4</td>
        <td class="number assets-non-a4">174,507,735</td>
        <td class="number assets-cur-a4">557,289,577</td>
        <td class="number assets-tot-a4">731,797,312</td>
        <td class="number equity-a4">334,198,504</td>
        <td class="number liabilities-non-a4">59,911,876</td>
        <td class="number liabilities-cur-a4">337,686,932</td>
        <td class="number liabilities-tot-a4">397,598,808</td>
        <td class="number revenue-a4">398,857,585</td>
        <td class="number revenue-a4">-361,007,549</td>
        <td class="number revenue-a4">37,850,036</td>
        <td class="number revenue-a4">-4,148,908</td>
        <td class="number revenue-a4">33,701,128</td>
        <td class="number revenue-a4">-3,999,294</td>
        <td class="number revenue-a4">29,701,834</td>
        <td class="number revenue-a4">45,098,616</td>
        <td class="number revenue-a4">74,800,450</td>
        <td class="number cashflow-a4">158,763,768</td>
        <td class="number cashflow-a4">-61,331,054</td>
        <td class="number cashflow-a4">-72,804,699</td>
        <td class="number cashflow-a4">24,628,015</td>
        <td class="number cashflow-a4">29,472,744</td>
        <td class="number cashflow-a4">54,100,759</td>
        <td class="number cashflow-a4">-62,474,858</td>
        <td class="number cashflow-a4">96,288,910</td>
      </tr>
      <tr class="row-t3">
        <td class="quarter-odd">T3</td>
        <td class="number assets-non-t3">344,067,974</td>
        <td class="number assets-cur-t3">409,212,764</td>
        <td class="number assets-tot-t3">753,280,738</td>
        <td class="number equity-t3">331,792,623</td>
        <td class="number liabilities-non-t3">147,309,017</td>
        <td class="number liabilities-cur-t3">274,179,098</td>
        <td class="number liabilities-tot-t3">421,488,115</td>
        <td class="number revenue-t3">816,235,465</td>
        <td class="number revenue-t3">-740,586,692</td>
        <td class="number revenue-t3">75,648,773</td>
        <td class="number revenue-t3">-5,711,709</td>
        <td class="number revenue-t3">69,937,064</td>
        <td class="number revenue-t3">-9,652,711</td>
        <td class="number revenue-t3">60,284,353</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">60,284,353</td>
        <td class="number cashflow-t3">87,156,626</td>
        <td class="number cashflow-t3">-33,279,206</td>
        <td class="number cashflow-t3">-57,028,300</td>
        <td class="number cashflow-t3">-3,150,880</td>
        <td class="number cashflow-t3">29,472,744</td>
        <td class="number cashflow-t3">26,321,866</td>
        <td class="number cashflow-t3">-42,044,873</td>
        <td class="number cashflow-t3">45,111,753</td>
      </tr>
      <tr class="row-h2">
        <td class="quarter-odd">H2</td>
        <td class="number assets-non-h2">338,929,277</td>
        <td class="number assets-cur-h2">414,250,945</td>
        <td class="number assets-tot-h2">753,180,222</td>
        <td class="number equity-h2">301,887,137</td>
        <td class="number liabilities-non-h2">133,525,765</td>
        <td class="number liabilities-cur-h2">317,767,318</td>
        <td class="number liabilities-tot-h2">451,293,083</td>
        <td class="number revenue-h2">486,645,621</td>
        <td class="number revenue-h2">-449,467,770</td>
        <td class="number revenue-h2">37,177,851</td>
        <td class="number revenue-h2">-3,195,059</td>
        <td class="number revenue-h2">33,982,792</td>
        <td class="number revenue-h2">-4,438,164</td>
        <td class="number revenue-h2">29,544,628</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">29,544,628</td>
        <td class="number cashflow-h2">38,695,783</td>
        <td class="number cashflow-h2">-30,544,533</td>
        <td class="number cashflow-h2">-8,469,617</td>
        <td class="number cashflow-h2">-318,367</td>
        <td class="number cashflow-h2">29,472,744</td>
        <td class="number cashflow-h2">29,154,381</td>
        <td class="number cashflow-h2">-29,617,825</td>
        <td class="number cashflow-h2">9,077,958</td>
      </tr>
      <tr class="row-q1">
        <td class="quarter-odd">Q1</td>
        <td class="number assets-non-q1">349,346,096</td>
        <td class="number assets-cur-q1">386,611,113</td>
        <td class="number assets-tot-q1">735,957,209</td>
        <td class="number equity-q1">282,291,694</td>
        <td class="number liabilities-non-q1">139,291,792</td>
        <td class="number liabilities-cur-q1">314,373,724</td>
        <td class="number liabilities-tot-q1">453,665,516</td>
        <td class="number revenue-q1">84,890,735</td>
        <td class="number revenue-q1">-78,448,546</td>
        <td class="number revenue-q1">6,442,189</td>
        <td class="number revenue-q1">-411,710</td>
        <td class="number revenue-q1">6,030,479</td>
        <td class="number revenue-q1">-724,234</td>
        <td class="number revenue-q1">5,306,244</td>
        <td class="number revenue-q1">833,498</td>
        <td class="number revenue-q1">6,139,743</td>
        <td class="number cashflow-q1">-14,895,716</td>
        <td class="number cashflow-q1">-24,168,334</td>
        <td class="number cashflow-q1">30,613,903</td>
        <td class="number cashflow-q1">-8,450,147</td>
        <td class="number cashflow-q1">14,352,404</td>
        <td class="number cashflow-q1">5,902,257</td>
        <td class="number cashflow-q1">-19,409,734</td>
        <td class="number cashflow-q1">-34,305,450</td>
      </tr>
      <tr class="row-a4">
        <td rowspan="4" class="year-even">2019</td>
        <td class="quarter-even">A4</td>
        <td class="number assets-non-a4">341,642,950</td>
        <td class="number assets-cur-a4">351,737,296</td>
        <td class="number assets-tot-a4">693,380,246</td>
        <td class="number equity-a4">276,148,399</td>
        <td class="number liabilities-non-a4">95,916,602</td>
        <td class="number liabilities-cur-a4">321,315,245</td>
        <td class="number liabilities-tot-a4">417,231,847</td>
        <td class="number revenue-a4">343,291,513</td>
        <td class="number revenue-a4">-326,286,881</td>
        <td class="number revenue-a4">17,004,632</td>
        <td class="number revenue-a4">-5,922,699</td>
        <td class="number revenue-a4">11,081,933</td>
        <td class="number revenue-a4">-1,324,931</td>
        <td class="number revenue-a4">9,757,020</td>
        <td class="number revenue-a4">29,322,970</td>
        <td class="number revenue-a4">39,079,990</td>
        <td class="number cashflow-a4">75,499,640</td>
        <td class="number cashflow-a4">-86,981,761</td>
        <td class="number cashflow-a4">18,137,296</td>
        <td class="number cashflow-a4">6,655,173</td>
        <td class="number cashflow-a4">22,817,571</td>
        <td class="number cashflow-a4">29,472,744</td>
        <td class="number cashflow-a4">-95,271,858</td>
        <td class="number cashflow-a4">-19,772,218</td>
      </tr>
      <tr class="row-t3">
        <td class="quarter-even">T3</td>
        <td class="number assets-non-t3">305,777,485</td>
        <td class="number assets-cur-t3">423,418,622</td>
        <td class="number assets-tot-t3">729,196,107</td>
        <td class="number equity-t3">270,762,333</td>
        <td class="number liabilities-non-t3">86,963,577</td>
        <td class="number liabilities-cur-t3">371,470,197</td>
        <td class="number liabilities-tot-t3">458,433,774</td>
        <td class="number revenue-t3">719,136,708</td>
        <td class="number revenue-t3">-672,781,312</td>
        <td class="number revenue-t3">46,355,396</td>
        <td class="number revenue-t3">-7,849,621</td>
        <td class="number revenue-t3">38,505,775</td>
        <td class="number revenue-t3">-4,756,252</td>
        <td class="number revenue-t3">33,749,523</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">33,749,523</td>
        <td class="number cashflow-t3">41,103,225</td>
        <td class="number cashflow-t3">-65,983,369</td>
        <td class="number cashflow-t3">24,629,867</td>
        <td class="number cashflow-t3">-250,278</td>
        <td class="number cashflow-t3">22,817,571</td>
        <td class="number cashflow-t3">22,567,293</td>
        <td class="number cashflow-t3">-64,068,838</td>
        <td class="number cashflow-t3">-22,965,613</td>
      </tr>
      <tr class="row-h2">
        <td class="quarter-even">H2</td>
        <td class="number assets-non-h2">276,148,451</td>
        <td class="number assets-cur-h2">409,345,194</td>
        <td class="number assets-tot-h2">685,493,645</td>
        <td class="number equity-h2">252,318,054</td>
        <td class="number liabilities-non-h2">81,636,764</td>
        <td class="number liabilities-cur-h2">351,538,827</td>
        <td class="number liabilities-tot-h2">433,175,591</td>
        <td class="number revenue-h2">416,338,192</td>
        <td class="number revenue-h2">-394,278,305</td>
        <td class="number revenue-h2">22,059,887</td>
        <td class="number revenue-h2">-5,296,846</td>
        <td class="number revenue-h2">16,763,041</td>
        <td class="number revenue-h2">-2,095,848</td>
        <td class="number revenue-h2">14,667,193</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">14,667,193</td>
        <td class="number cashflow-h2">14,725,589</td>
        <td class="number cashflow-h2">-30,610,368</td>
        <td class="number cashflow-h2">19,812,114</td>
        <td class="number cashflow-h2">3,927,335</td>
        <td class="number cashflow-h2">22,817,571</td>
        <td class="number cashflow-h2">26,744,906</td>
        <td class="number cashflow-h2">-28,956,420</td>
        <td class="number cashflow-h2">-14,230,831</td>
      </tr>
      <tr class="row-q1">
        <td class="quarter-even">Q1</td>
        <td class="number assets-non-q1">298,801,504</td>
        <td class="number assets-cur-q1">362,701,673</td>
        <td class="number assets-tot-q1">661,503,177</td>
        <td class="number equity-q1">241,354,419</td>
        <td class="number liabilities-non-q1">131,479,853</td>
        <td class="number liabilities-cur-q1">288,668,906</td>
        <td class="number liabilities-tot-q1">420,148,759</td>
        <td class="number revenue-q1">174,643,424</td>
        <td class="number revenue-q1">-168,396,054</td>
        <td class="number revenue-q1">6,247,370</td>
        <td class="number revenue-q1">-2,936,129</td>
        <td class="number revenue-q1">3,311,241</td>
        <td class="number revenue-q1">3,516</td>
        <td class="number revenue-q1">3,314,757</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">3,314,757</td>
        <td class="number cashflow-q1">-6,639,053</td>
        <td class="number cashflow-q1">-15,475,821</td>
        <td class="number cashflow-q1">18,123,983</td>
        <td class="number cashflow-q1">-3,990,891</td>
        <td class="number cashflow-q1">22,817,571</td>
        <td class="number cashflow-q1">18,826,679</td>
        <td class="number cashflow-q1">-16,128,152</td>
        <td class="number cashflow-q1">-22,767,205</td>
      </tr>
      <tr class="row-a4">
        <td rowspan="4" class="year-odd">2018</td>
        <td class="quarter-odd">A4</td>
        <td class="number assets-non-a4">294,296,137</td>
        <td class="number assets-cur-a4">347,950,331</td>
        <td class="number assets-tot-a4">642,246,468</td>
        <td class="number equity-a4">238,115,865</td>
        <td class="number liabilities-non-a4">133,798,312</td>
        <td class="number liabilities-cur-a4">270,332,291</td>
        <td class="number liabilities-tot-a4">404,130,603</td>
        <td class="number revenue-a4">804,512,197</td>
        <td class="number revenue-a4">-769,739,965</td>
        <td class="number revenue-a4">34,772,232</td>
        <td class="number revenue-a4">-8,612,853</td>
        <td class="number revenue-a4">26,159,379</td>
        <td class="number revenue-a4">-3,520,673</td>
        <td class="number revenue-a4">22,638,706</td>
        <td class="number revenue-a4">-563,754</td>
        <td class="number revenue-a4">22,074,952</td>
        <td class="number cashflow-a4">7,886,493</td>
        <td class="number cashflow-a4">-37,006,455</td>
        <td class="number cashflow-a4">39,921,730</td>
        <td class="number cashflow-a4">10,801,769</td>
        <td class="number cashflow-a4">12,015,802</td>
        <td class="number cashflow-a4">22,817,571</td>
        <td class="number cashflow-a4">-32,059,265</td>
        <td class="number cashflow-a4">-24,172,772</td>
      </tr>
      <tr class="row-t3">
        <td class="quarter-odd">T3</td>
        <td class="number assets-non-t3">257,397,134</td>
        <td class="number assets-cur-t3">375,747,487</td>
        <td class="number assets-tot-t3">633,144,621</td>
        <td class="number equity-t3">248,113,431</td>
        <td class="number liabilities-non-t3">82,162,614</td>
        <td class="number liabilities-cur-t3">302,868,576</td>
        <td class="number liabilities-tot-t3">385,031,190</td>
        <td class="number revenue-t3">590,483,294</td>
        <td class="number revenue-t3">-557,955,833</td>
        <td class="number revenue-t3">32,527,461</td>
        <td class="number revenue-t3">-6,650,323</td>
        <td class="number revenue-t3">25,877,138</td>
        <td class="number revenue-t3">-3,589,418</td>
        <td class="number revenue-t3">22,287,720</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">22,287,720</td>
        <td class="number cashflow-t3">-21,491,073</td>
        <td class="number cashflow-t3">-32,605,754</td>
        <td class="number cashflow-t3">56,795,598</td>
        <td class="number cashflow-t3">2,698,770</td>
        <td class="number cashflow-t3">12,015,802</td>
        <td class="number cashflow-t3">14,714,572</td>
        <td class="number cashflow-t3">-24,934,925</td>
        <td class="number cashflow-t3">-46,425,998</td>
      </tr>
      <tr class="row-h2">
        <td class="quarter-odd">H2</td>
        <td class="number assets-non-h2">256,348,816</td>
        <td class="number assets-cur-h2">365,498,691</td>
        <td class="number assets-tot-h2">621,847,507</td>
        <td class="number equity-h2">236,327,443</td>
        <td class="number liabilities-non-h2">83,172,727</td>
        <td class="number liabilities-cur-h2">302,347,337</td>
        <td class="number liabilities-tot-h2">385,520,064</td>
        <td class="number revenue-h2">338,494,358</td>
        <td class="number revenue-h2">-322,507,841</td>
        <td class="number revenue-h2">15,986,517</td>
        <td class="number revenue-h2">-3,833,818</td>
        <td class="number revenue-h2">12,152,698</td>
        <td class="number revenue-h2">-1,629,252</td>
        <td class="number revenue-h2">10,523,446</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">10,523,446</td>
        <td class="number cashflow-h2">-37,584,902</td>
        <td class="number cashflow-h2">-19,006,393</td>
        <td class="number cashflow-h2">56,163,400</td>
        <td class="number cashflow-h2">-427,895</td>
        <td class="number cashflow-h2">12,015,802</td>
        <td class="number cashflow-h2">11,587,907</td>
        <td class="number cashflow-h2">-11,419,777</td>
        <td class="number cashflow-h2">-49,004,679</td>
      </tr>
      <tr class="row-q1">
        <td class="quarter-odd">Q1</td>
        <td class="number assets-non-q1">252,827,886</td>
        <td class="number assets-cur-q1">341,399,680</td>
        <td class="number assets-tot-q1">594,227,566</td>
        <td class="number equity-q1">235,599,777</td>
        <td class="number liabilities-non-q1">85,355,750</td>
        <td class="number liabilities-cur-q1">273,272,038</td>
        <td class="number liabilities-tot-q1">358,627,788</td>
        <td class="number revenue-q1">130,720,442</td>
        <td class="number revenue-q1">-130,097,104</td>
        <td class="number revenue-q1">623,338</td>
        <td class="number revenue-q1">-1,157,732</td>
        <td class="number revenue-q1">-534,394</td>
        <td class="number revenue-q1">-540,325</td>
        <td class="number revenue-q1">-1,074,719</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">-1,074,719</td>
        <td class="number cashflow-q1">-21,859,761</td>
        <td class="number cashflow-q1">-13,904,361</td>
        <td class="number cashflow-q1">37,945,373</td>
        <td class="number cashflow-q1">2,181,251</td>
        <td class="number cashflow-q1">12,015,802</td>
        <td class="number cashflow-q1">14,197,053</td>
        <td class="number cashflow-q1">-6,137,107</td>
        <td class="number cashflow-q1">-27,996,868</td>
      </tr>
      <tr class="row-a4">
        <td rowspan="4" class="year-even">2017</td>
        <td class="quarter-even">A4</td>
        <td class="number assets-non-a4">248,913,444</td>
        <td class="number assets-cur-a4">203,501,955</td>
        <td class="number assets-tot-a4">452,415,399</td>
        <td class="number equity-a4">224,743,737</td>
        <td class="number liabilities-non-a4">80,967,093</td>
        <td class="number liabilities-cur-a4">146,704,569</td>
        <td class="number liabilities-tot-a4">227,671,662</td>
        <td class="number revenue-a4">422,270,070</td>
        <td class="number revenue-a4">-397,468,470</td>
        <td class="number revenue-a4">24,801,600</td>
        <td class="number revenue-a4">-3,538,275</td>
        <td class="number revenue-a4">21,263,325</td>
        <td class="number revenue-a4">-2,693,989</td>
        <td class="number revenue-a4">18,569,336</td>
        <td class="number revenue-a4">0</td>
        <td class="number revenue-a4">18,569,336</td>
        <td class="number cashflow-a4">11,837,924</td>
        <td class="number cashflow-a4">-112,648,137</td>
        <td class="number cashflow-a4">96,793,636</td>
        <td class="number cashflow-a4">-4,016,571</td>
        <td class="number cashflow-a4">16,032,373</td>
        <td class="number cashflow-a4">12,015,802</td>
        <td class="number cashflow-a4">-54,561,546</td>
        <td class="number cashflow-a4">-42,723,622</td>
      </tr>
      <tr class="row-t3">
        <td class="quarter-even">T3</td>
        <td class="number assets-non-t3">236,965,584</td>
        <td class="number assets-cur-t3">211,970,600</td>
        <td class="number assets-tot-t3">448,936,184</td>
        <td class="number equity-t3">223,675,213</td>
        <td class="number liabilities-non-t3">64,096,032</td>
        <td class="number liabilities-cur-t3">161,164,940</td>
        <td class="number liabilities-tot-t3">225,260,972</td>
        <td class="number revenue-t3">294,633,800</td>
        <td class="number revenue-t3">-273,946,369</td>
        <td class="number revenue-t3">20,687,431</td>
        <td class="number revenue-t3">-2,054,500</td>
        <td class="number revenue-t3">18,632,931</td>
        <td class="number revenue-t3">-2,713,161</td>
        <td class="number revenue-t3">15,919,770</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">15,919,770</td>
        <td class="number cashflow-t3">18,331,279</td>
        <td class="number cashflow-t3">-87,280,763</td>
        <td class="number cashflow-t3">80,526,223</td>
        <td class="number cashflow-t3">11,576,739</td>
        <td class="number cashflow-t3">16,032,373</td>
        <td class="number cashflow-t3">27,609,112</td>
        <td class="number cashflow-t3">-40,060,072</td>
        <td class="number cashflow-t3">-21,728,793</td>
      </tr>
      <tr class="row-h2">
        <td class="quarter-even">H2</td>
        <td class="number assets-non-h2">218,455,845</td>
        <td class="number assets-cur-h2">211,877,448</td>
        <td class="number assets-tot-h2">430,333,293</td>
        <td class="number equity-h2">215,640,841</td>
        <td class="number liabilities-non-h2">64,616,346</td>
        <td class="number liabilities-cur-h2">150,076,106</td>
        <td class="number liabilities-tot-h2">214,692,452</td>
        <td class="number revenue-h2">171,671,498</td>
        <td class="number revenue-h2">-161,495,804</td>
        <td class="number revenue-h2">10,175,694</td>
        <td class="number revenue-h2">-806,207</td>
        <td class="number revenue-h2">9,369,487</td>
        <td class="number revenue-h2">-1,479,224</td>
        <td class="number revenue-h2">7,890,263</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">7,890,263</td>
        <td class="number cashflow-h2">9,714,256</td>
        <td class="number cashflow-h2">-68,405,291</td>
        <td class="number cashflow-h2">66,813,355</td>
        <td class="number cashflow-h2">8,122,320</td>
        <td class="number cashflow-h2">16,032,373</td>
        <td class="number cashflow-h2">24,154,693</td>
        <td class="number cashflow-h2">-28,874,683</td>
        <td class="number cashflow-h2">-19,160,427</td>
      </tr>
      <tr class="row-q1">
        <td class="quarter-even">Q1</td>
        <td class="number assets-non-q1">129,272,678</td>
        <td class="number assets-cur-q1">108,956,845</td>
        <td class="number assets-tot-q1">238,229,523</td>
        <td class="number equity-q1">184,919,108</td>
        <td class="number liabilities-non-q1">13,326,968</td>
        <td class="number liabilities-cur-q1">39,983,449</td>
        <td class="number liabilities-tot-q1">53,310,417</td>
        <td class="number revenue-q1">66,578,476</td>
        <td class="number revenue-q1">-64,213,502</td>
        <td class="number revenue-q1">2,364,974</td>
        <td class="number revenue-q1">-284,150</td>
        <td class="number revenue-q1">2,080,824</td>
        <td class="number revenue-q1">-428,224</td>
        <td class="number revenue-q1">1,652,600</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">1,652,600</td>
        <td class="number cashflow-q1">-2,158,871</td>
        <td class="number cashflow-q1">-1,938,425</td>
        <td class="number cashflow-q1">7,572,037</td>
        <td class="number cashflow-q1">3,474,741</td>
        <td class="number cashflow-q1">16,032,373</td>
        <td class="number cashflow-q1">19,507,113</td>
        <td class="number cashflow-q1">-1,955,385</td>
        <td class="number cashflow-q1">-4,114,256</td>
      </tr>
      <tr class="row-a4">
        <td rowspan="4" class="year-odd">2016</td>
        <td class="quarter-odd">A4</td>
        <td class="number assets-non-a4">157,570,491</td>
        <td class="number assets-cur-a4">149,336,024</td>
        <td class="number assets-tot-a4">306,906,515</td>
        <td class="number equity-a4">216,923,646</td>
        <td class="number liabilities-non-a4">17,313,185</td>
        <td class="number liabilities-cur-a4">72,669,684</td>
        <td class="number liabilities-tot-a4">89,982,869</td>
        <td class="number revenue-a4">398,788,273</td>
        <td class="number revenue-a4">-351,444,813</td>
        <td class="number revenue-a4">47,343,460</td>
        <td class="number revenue-a4">-2,210,495</td>
        <td class="number revenue-a4">45,132,970</td>
        <td class="number revenue-a4">-5,716,799</td>
        <td class="number revenue-a4">39,416,171</td>
        <td class="number revenue-a4">2,500,219</td>
        <td class="number revenue-a4">41,916,390</td>
        <td class="number cashflow-a4">42,139,093</td>
        <td class="number cashflow-a4">-13,194,669</td>
        <td class="number cashflow-a4">-25,467,821</td>
        <td class="number cashflow-a4">3,476,603</td>
        <td class="number cashflow-a4">12,555,770</td>
        <td class="number cashflow-a4">16,032,373</td>
        <td class="number cashflow-a4">-13,290,630</td>
        <td class="number cashflow-a4">28,848,463</td>
      </tr>
      <tr class="row-t3">
        <td class="quarter-odd">T3</td>
        <td class="number assets-non-t3">157,570,491</td>
        <td class="number assets-cur-t3">149,336,024</td>
        <td class="number assets-tot-t3">306,906,515</td>
        <td class="number equity-t3">216,923,646</td>
        <td class="number liabilities-non-t3">17,313,185</td>
        <td class="number liabilities-cur-t3">72,669,684</td>
        <td class="number liabilities-tot-t3">89,982,869</td>
        <td class="number revenue-t3">310,909,168</td>
        <td class="number revenue-t3">-273,280,142</td>
        <td class="number revenue-t3">37,629,026</td>
        <td class="number revenue-t3">-1,290,775</td>
        <td class="number revenue-t3">36,338,251</td>
        <td class="number revenue-t3">-6,008,092</td>
        <td class="number revenue-t3">30,330,159</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">30,330,159</td>
        <td class="number cashflow-t3">17,506,977</td>
        <td class="number cashflow-t3">-10,305,084</td>
        <td class="number cashflow-t3">-11,067,756</td>
        <td class="number cashflow-t3">-3,865,863</td>
        <td class="number cashflow-t3">12,555,770</td>
        <td class="number cashflow-t3">8,689,908</td>
        <td class="number cashflow-t3">-10,401,045</td>
        <td class="number cashflow-t3">7,105,932</td>
      </tr>
      <tr class="row-h2">
        <td class="quarter-odd">H2</td>
        <td class="number assets-non-h2">157,570,491</td>
        <td class="number assets-cur-h2">149,336,024</td>
        <td class="number assets-tot-h2">306,906,515</td>
        <td class="number equity-h2">216,923,646</td>
        <td class="number liabilities-non-h2">17,313,185</td>
        <td class="number liabilities-cur-h2">72,669,684</td>
        <td class="number liabilities-tot-h2">89,982,869</td>
        <td class="number revenue-h2">192,759,524</td>
        <td class="number revenue-h2">-168,737,931</td>
        <td class="number revenue-h2">24,021,593</td>
        <td class="number revenue-h2">-1,276,034</td>
        <td class="number revenue-h2">22,745,559</td>
        <td class="number revenue-h2">-3,679,851</td>
        <td class="number revenue-h2">19,065,708</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">19,065,708</td>
        <td class="number cashflow-h2">13,324,439</td>
        <td class="number cashflow-h2">-5,110,999</td>
        <td class="number cashflow-h2">-8,833,734</td>
        <td class="number cashflow-h2">-620,294</td>
        <td class="number cashflow-h2">12,555,770</td>
        <td class="number cashflow-h2">11,935,476</td>
        <td class="number cashflow-h2">-5,206,960</td>
        <td class="number cashflow-h2">8,117,479</td>
      </tr>
      <tr class="row-q1">
        <td class="quarter-odd">Q1</td>
        <td class="number assets-non-q1">129,810,107</td>
        <td class="number assets-cur-q1">104,296,579</td>
        <td class="number assets-tot-q1">234,106,686</td>
        <td class="number equity-q1">184,322,513</td>
        <td class="number liabilities-non-q1">12,114,255</td>
        <td class="number liabilities-cur-q1">37,669,917</td>
        <td class="number liabilities-tot-q1">49,784,172</td>
        <td class="number revenue-q1">40,355,222</td>
        <td class="number revenue-q1">-40,405,771</td>
        <td class="number revenue-q1">-50,549</td>
        <td class="number revenue-q1">691,128</td>
        <td class="number revenue-q1">640,579</td>
        <td class="number revenue-q1">-44,001</td>
        <td class="number revenue-q1">596,578</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">596,578</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
      </tr>
    </tbody>
  </table>
</div>

## Valuation

**Coming soon...**
