---
title: "TURBOMECANICA S.A. (TBM)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xbse/">XBSE</a></td></tr>
    <tr><td>Name</td><td>TURBOMECANICA S.A.</td></tr>
    <tr><td>Symbol</td><td>TBM</td></tr>
    <tr><td>Web</td><td><a href="https://www.turbomecanica.ro">www.turbomecanica.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2018 | 0.04 |
| 2017 | 0.0242 |
| 2016 | 0.0105 |
| 2006 | 0.03 |
| 2005 | 1.09 |
| 2004 | 0.9922 |
| 2003 | 0.617 |
| 2002 | 0.6348 |
| 2001 | 0.4792 |
| 2000 | 1.5281 |

### Reports

<style>
  .reports-table {
    width: 100%;
    display: block;
    overflow-x: auto;
    overflow-y: scroll;
    scrollbar-width: none;
    max-height: 50rem;
    border: 0.5rem solid #D8D8D8;
  }
  .reports-table table {
    border: none;
    padding: 0;
  }
  .reports-table th {
    border: none;
    padding: 0;
  }
  .reports-table td {
    border: none
    padding: 0;
  }

  table.reports {
    border-collapse: collapse;
  }

  table.reports thead th, table.reports tbody td {
    padding: 3px 5px;
  }

  table.reports td {
    border: 0px solid #FFFFFF;
  }

  table.reports thead .header-top {
    background-color: #000000;
    color: #FFFFFF;
  }

  table.reports thead .header-assets {
    background-color: #3D85C6;
    color: #FFFFFF;
  }

  table.reports thead .header-assets-non {
    background-color: #A2C4C9;
  }

  table.reports thead .header-assets-cur {
    background-color: #9FC5E8;
  }

  table.reports thead .header-assets-tot {
    background-color: #3D85C6;
    color: #FFFFFF;
  }

  table.reports thead .header-equity {
    background-color: #6AA84F;
    color: #FFFFFF;
  }

  table.reports thead .header-lia-non {
    background-color: #D5A6BD;
  }

  table.reports thead .header-lia-cur {
    background-color: #EA9999;
  }

  table.reports thead .header-lia-tot {
    background-color: #A64D79;
    color: #FFFFFF;
  }

  table.reports thead .header-equity {
    background-color: #6AA84F;
    color: #FFFFFF;
  }

  table.reports thead .header-liabilities {
    background-color: #A64D79;
    color: #FFFFFF;
  }

  table.reports thead .header-revenue {
    background-color: #674EA6;
    color: #FFFFFF;
  }

  table.reports thead .header-cashflow {
    background-color: #E69138;
    color: #FFFFFF;
  }

  table.reports tbody td.number {
    text-align: right;
  }

  table.reports tbody td.year-even, table.reports tbody td.quarter-even {
    text-align: center;
    background-color: #B7B7B7;
  }

  table.reports tbody td.year-odd, table.reports tbody td.quarter-odd {
    text-align: center;
    background-color: #D9D9D9;
  }

  table.reports tbody td.assets-non-h2 {
    background-color: #D0E0E3;
  }
  table.reports tbody td.assets-cur-h2 {
    background-color: #CFE2F3;
  }
  table.reports tbody td.assets-tot-h2 {
    background-color: #CFE2F3;
  }
  table.reports tbody td.assets-non-t3 {
    background-color: #A2C4C9;
  }
  table.reports tbody td.assets-cur-t3 {
    background-color: #9FC5E8;
  }
  table.reports tbody td.assets-tot-t3 {
    background-color: #9FC5E8;
  }
  table.reports tbody td.assets-non-a4 {
    background-color: #76A5AF;
  }
  table.reports tbody td.assets-cur-a4 {
    background-color: #6FA8DC;
  }
  table.reports tbody td.assets-tot-a4 {
    background-color: #6FA8DC;
  }

  table.reports tbody td.equity-h2 {
    background-color: #D9EAD3;
  }
  table.reports tbody td.equity-t3 {
    background-color: #B6D7A8;
  }
  table.reports tbody td.equity-a4 {
    background-color: #93C47D;
  }

  table.reports tbody td.liabilities-non-h2 {
    background-color: #EAD1DC;
  }
  table.reports tbody td.liabilities-cur-h2 {
    background-color: #F4CCCC;
  }
  table.reports tbody td.liabilities-tot-h2 {
    background-color: #F4CCCC;
  }
  table.reports tbody td.liabilities-non-t3 {
    background-color: #D5A6BD;
  }
  table.reports tbody td.liabilities-cur-t3 {
    background-color: #EA9999;
  }
  table.reports tbody td.liabilities-tot-t3 {
    background-color: #EA9999;
  }
  table.reports tbody td.liabilities-non-a4 {
    background-color: #C27BA0;
  }
  table.reports tbody td.liabilities-cur-a4 {
    background-color: #E06666;
  }
  table.reports tbody td.liabilities-tot-a4 {
    background-color: #E06666;
  }

  table.reports tbody td.revenue-h2 {
    background-color: #D9D2E9;
  }
  table.reports tbody td.revenue-t3 {
    background-color: #B4A7D6;
  }
  table.reports tbody td.revenue-a4 {
    background-color: #8E7CC3;
  }

  table.reports tbody td.cashflow-h2 {
    background-color: #FCE5CD;
  }
  table.reports tbody td.cashflow-t3 {
    background-color: #F9CB9C;
  }
  table.reports tbody td.cashflow-a4 {
    background-color: #F6B26B;
  }
</style>

<div class="reports-table">
  <table class="reports">
    <thead>
      <tr>
        <th rowspan="3" class="header-top">Year</th>
        <th rowspan="3" class="header-top">Quarter</th>
        <th colspan="7" class="header-top">Balance</th>
        <th colspan="9" class="header-top">Revenue</th>
        <th colspan="8" class="header-top">Cashflow</th>
      </tr>
      <tr>
        <th colspan="3" class="header-assets">Assets</th>
        <th             class="header-equity">Equity</th>
        <th colspan="3" class="header-liabilities">Liabilities</th>
        <th rowspan="2" class="header-revenue">Sales</th>
        <th rowspan="2" class="header-revenue">Operating Expenses</th>
        <th rowspan="2" class="header-revenue">Operating Profit</th>
        <th rowspan="2" class="header-revenue">Financial Resut</th>
        <th rowspan="2" class="header-revenue">Profit Before Taxes</th>
        <th rowspan="2" class="header-revenue">Tax On Gains</th>
        <th rowspan="2" class="header-revenue">Net Profit</th>
        <th rowspan="2" class="header-revenue">Other</th>
        <th rowspan="2" class="header-revenue">Total Result</th>
        <th rowspan="2" class="header-cashflow">Operating</th>
        <th rowspan="2" class="header-cashflow">Investment</th>
        <th rowspan="2" class="header-cashflow">Financing</th>
        <th rowspan="2" class="header-cashflow">Total</th>
        <th rowspan="2" class="header-cashflow">Cash at Start</th>
        <th rowspan="2" class="header-cashflow">Cash at End</th>
        <th rowspan="2" class="header-cashflow">Capex</th>
        <th rowspan="2" class="header-cashflow">Free Cash Flow</th>
      </tr>
      <tr>
        <th class="header-assets-non">Noncurrent</th>
        <th class="header-assets-cur">Current</th>
        <th class="header-assets-tot">Total</th>
        <th class="header-equity">Total</th>
        <th class="header-lia-non">Noncurrent</th>
        <th class="header-lia-cur">Current</th>
        <th class="header-lia-tot">Total</th>
      <tr>
    </thead>
    <tbody>
      <tr class="row-a4">
        <td rowspan="4" class="year-even">2020</td>
        <td class="quarter-even">A4</td>
        <td class="number assets-non-a4">0</td>
        <td class="number assets-cur-a4">0</td>
        <td class="number assets-tot-a4">0</td>
        <td class="number equity-a4">0</td>
        <td class="number liabilities-non-a4">0</td>
        <td class="number liabilities-cur-a4">0</td>
        <td class="number liabilities-tot-a4">0</td>
        <td class="number revenue-a4">0</td>
        <td class="number revenue-a4">0</td>
        <td class="number revenue-a4">0</td>
        <td class="number revenue-a4">0</td>
        <td class="number revenue-a4">0</td>
        <td class="number revenue-a4">0</td>
        <td class="number revenue-a4">0</td>
        <td class="number revenue-a4">0</td>
        <td class="number revenue-a4">0</td>
        <td class="number cashflow-a4">0</td>
        <td class="number cashflow-a4">0</td>
        <td class="number cashflow-a4">0</td>
        <td class="number cashflow-a4">0</td>
        <td class="number cashflow-a4">0</td>
        <td class="number cashflow-a4">0</td>
        <td class="number cashflow-a4">0</td>
        <td class="number cashflow-a4">0</td>
      </tr>
      <tr class="row-t3">
        <td class="quarter-even">T3</td>
        <td class="number assets-non-t3">58,296,544</td>
        <td class="number assets-cur-t3">86,072,853</td>
        <td class="number assets-tot-t3">144,369,397</td>
        <td class="number equity-t3">90,672,089</td>
        <td class="number liabilities-non-t3">9,745,369</td>
        <td class="number liabilities-cur-t3">43,951,939</td>
        <td class="number liabilities-tot-t3">53,697,308</td>
        <td class="number revenue-t3">73,758,262</td>
        <td class="number revenue-t3">-61,924,820</td>
        <td class="number revenue-t3">11,833,442</td>
        <td class="number revenue-t3">-1,608,836</td>
        <td class="number revenue-t3">10,224,606</td>
        <td class="number revenue-t3">-1,529,863</td>
        <td class="number revenue-t3">8,694,743</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">8,694,743</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
      </tr>
      <tr class="row-h2">
        <td class="quarter-even">H2</td>
        <td class="number assets-non-h2">59,351,585</td>
        <td class="number assets-cur-h2">83,249,677</td>
        <td class="number assets-tot-h2">142,601,262</td>
        <td class="number equity-h2">92,199,494</td>
        <td class="number liabilities-non-h2">9,577,866</td>
        <td class="number liabilities-cur-h2">40,823,902</td>
        <td class="number liabilities-tot-h2">50,401,767</td>
        <td class="number revenue-h2">54,796,174</td>
        <td class="number revenue-h2">-42,243,631</td>
        <td class="number revenue-h2">12,552,543</td>
        <td class="number revenue-h2">-979,004</td>
        <td class="number revenue-h2">11,573,539</td>
        <td class="number revenue-h2">-1,485,305</td>
        <td class="number revenue-h2">10,088,234</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">10,088,234</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
      </tr>
      <tr class="row-q1">
        <td class="quarter-even">Q1</td>
        <td class="number assets-non-q1">57,699,721</td>
        <td class="number assets-cur-q1">88,721,606</td>
        <td class="number assets-tot-q1">146,421,327</td>
        <td class="number equity-q1">85,896,407</td>
        <td class="number liabilities-non-q1">7,376,080</td>
        <td class="number liabilities-cur-q1">53,148,839</td>
        <td class="number liabilities-tot-q1">60,524,919</td>
        <td class="number revenue-q1">31,063,329</td>
        <td class="number revenue-q1">-25,419,580</td>
        <td class="number revenue-q1">5,643,749</td>
        <td class="number revenue-q1">-473,884</td>
        <td class="number revenue-q1">5,169,865</td>
        <td class="number revenue-q1">-1,250,804</td>
        <td class="number revenue-q1">3,919,061</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">3,919,061</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
      </tr>
      <tr class="row-a4">
        <td rowspan="4" class="year-odd">2019</td>
        <td class="quarter-odd">A4</td>
        <td class="number assets-non-a4">58,863,143</td>
        <td class="number assets-cur-a4">66,275,509</td>
        <td class="number assets-tot-a4">125,138,651</td>
        <td class="number equity-a4">81,977,346</td>
        <td class="number liabilities-non-a4">7,566,106</td>
        <td class="number liabilities-cur-a4">35,595,199</td>
        <td class="number liabilities-tot-a4">43,161,305</td>
        <td class="number revenue-a4">114,960,057</td>
        <td class="number revenue-a4">-87,097,223</td>
        <td class="number revenue-a4">27,862,834</td>
        <td class="number revenue-a4">-2,161,896</td>
        <td class="number revenue-a4">25,700,938</td>
        <td class="number revenue-a4">-3,781,636</td>
        <td class="number revenue-a4">21,919,302</td>
        <td class="number revenue-a4">-509,474</td>
        <td class="number revenue-a4">21,409,828</td>
        <td class="number cashflow-a4">15,021,122</td>
        <td class="number cashflow-a4">-8,283,177</td>
        <td class="number cashflow-a4">-9,623,240</td>
        <td class="number cashflow-a4">-2,885,295</td>
        <td class="number cashflow-a4">6,990,513</td>
        <td class="number cashflow-a4">4,105,218</td>
        <td class="number cashflow-a4">-8,284,297</td>
        <td class="number cashflow-a4">6,736,825</td>
      </tr>
      <tr class="row-t3">
        <td class="quarter-odd">T3</td>
        <td class="number assets-non-t3">60,302,240</td>
        <td class="number assets-cur-t3">69,690,512</td>
        <td class="number assets-tot-t3">129,992,751</td>
        <td class="number equity-t3">78,277,858</td>
        <td class="number liabilities-non-t3">7,811,749</td>
        <td class="number liabilities-cur-t3">43,903,145</td>
        <td class="number liabilities-tot-t3">51,714,894</td>
        <td class="number revenue-t3">77,814,296</td>
        <td class="number revenue-t3">-55,593,105</td>
        <td class="number revenue-t3">22,221,191</td>
        <td class="number revenue-t3">-1,613,327</td>
        <td class="number revenue-t3">20,607,864</td>
        <td class="number revenue-t3">-2,897,525</td>
        <td class="number revenue-t3">17,710,339</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">17,710,339</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
      </tr>
      <tr class="row-h2">
        <td class="quarter-odd">H2</td>
        <td class="number assets-non-h2">61,579,000</td>
        <td class="number assets-cur-h2">65,079,023</td>
        <td class="number assets-tot-h2">126,658,023</td>
        <td class="number equity-h2">75,041,998</td>
        <td class="number liabilities-non-h2">7,929,114</td>
        <td class="number liabilities-cur-h2">43,686,911</td>
        <td class="number liabilities-tot-h2">51,616,025</td>
        <td class="number revenue-h2">51,518,745</td>
        <td class="number revenue-h2">-33,709,125</td>
        <td class="number revenue-h2">17,809,620</td>
        <td class="number revenue-h2">-952,325</td>
        <td class="number revenue-h2">16,857,295</td>
        <td class="number revenue-h2">-2,382,816</td>
        <td class="number revenue-h2">14,474,479</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">14,474,479</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
      </tr>
      <tr class="row-q1">
        <td class="quarter-odd">Q1</td>
        <td class="number assets-non-q1">60,917,815</td>
        <td class="number assets-cur-q1">61,290,856</td>
        <td class="number assets-tot-q1">122,208,671</td>
        <td class="number equity-q1">78,799,763</td>
        <td class="number liabilities-non-q1">6,317,925</td>
        <td class="number liabilities-cur-q1">37,090,982</td>
        <td class="number liabilities-tot-q1">43,408,908</td>
        <td class="number revenue-q1">18,000,678</td>
        <td class="number revenue-q1">-13,746,963</td>
        <td class="number revenue-q1">4,253,715</td>
        <td class="number revenue-q1">-386,920</td>
        <td class="number revenue-q1">3,866,795</td>
        <td class="number revenue-q1">-412,249</td>
        <td class="number revenue-q1">3,454,546</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">3,454,546</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
      </tr>
      <tr class="row-a4">
        <td rowspan="4" class="year-even">2018</td>
        <td class="quarter-even">A4</td>
        <td class="number assets-non-a4">60,786,009</td>
        <td class="number assets-cur-a4">49,408,595</td>
        <td class="number assets-tot-a4">110,194,604</td>
        <td class="number equity-a4">75,345,217</td>
        <td class="number liabilities-non-a4">5,370,154</td>
        <td class="number liabilities-cur-a4">29,479,233</td>
        <td class="number liabilities-tot-a4">34,849,387</td>
        <td class="number revenue-a4">112,387,772</td>
        <td class="number revenue-a4">-80,270,641</td>
        <td class="number revenue-a4">32,117,131</td>
        <td class="number revenue-a4">-2,894,519</td>
        <td class="number revenue-a4">29,222,612</td>
        <td class="number revenue-a4">-4,374,895</td>
        <td class="number revenue-a4">24,847,717</td>
        <td class="number revenue-a4">-674,648</td>
        <td class="number revenue-a4">24,173,069</td>
        <td class="number cashflow-a4">27,676,002</td>
        <td class="number cashflow-a4">7,014,459</td>
        <td class="number cashflow-a4">-37,336,804</td>
        <td class="number cashflow-a4">-2,646,343</td>
        <td class="number cashflow-a4">9,636,857</td>
        <td class="number cashflow-a4">6,990,514</td>
        <td class="number cashflow-a4">-3,354,001</td>
        <td class="number cashflow-a4">24,322,001</td>
      </tr>
      <tr class="row-t3">
        <td class="quarter-even">T3</td>
        <td class="number assets-non-t3">62,233,244</td>
        <td class="number assets-cur-t3">55,512,573</td>
        <td class="number assets-tot-t3">117,745,816</td>
        <td class="number equity-t3">64,345,653</td>
        <td class="number liabilities-non-t3">8,965,775</td>
        <td class="number liabilities-cur-t3">44,434,388</td>
        <td class="number liabilities-tot-t3">53,400,164</td>
        <td class="number revenue-t3">65,487,127</td>
        <td class="number revenue-t3">-47,467,166</td>
        <td class="number revenue-t3">18,019,961</td>
        <td class="number revenue-t3">-2,279,145</td>
        <td class="number revenue-t3">15,740,816</td>
        <td class="number revenue-t3">-2,567,311</td>
        <td class="number revenue-t3">13,173,505</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">13,173,505</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
      </tr>
      <tr class="row-h2">
        <td class="quarter-even">H2</td>
        <td class="number assets-non-h2">63,024,570</td>
        <td class="number assets-cur-h2">49,362,353</td>
        <td class="number assets-tot-h2">112,386,923</td>
        <td class="number equity-h2">63,046,070</td>
        <td class="number liabilities-non-h2">9,003,380</td>
        <td class="number liabilities-cur-h2">40,337,472</td>
        <td class="number liabilities-tot-h2">49,340,853</td>
        <td class="number revenue-h2">46,929,792</td>
        <td class="number revenue-h2">-31,283,701</td>
        <td class="number revenue-h2">15,646,091</td>
        <td class="number revenue-h2">-1,495,468</td>
        <td class="number revenue-h2">14,150,623</td>
        <td class="number revenue-h2">-2,276,701</td>
        <td class="number revenue-h2">11,873,922</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">11,873,922</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
      </tr>
      <tr class="row-q1">
        <td class="quarter-even">Q1</td>
        <td class="number assets-non-q1">64,865,237</td>
        <td class="number assets-cur-q1">63,800,240</td>
        <td class="number assets-tot-q1">128,665,477</td>
        <td class="number equity-q1">61,904,143</td>
        <td class="number liabilities-non-q1">9,231,957</td>
        <td class="number liabilities-cur-q1">57,529,377</td>
        <td class="number liabilities-tot-q1">66,761,334</td>
        <td class="number revenue-q1">17,580,872</td>
        <td class="number revenue-q1">-14,437,990</td>
        <td class="number revenue-q1">3,142,882</td>
        <td class="number revenue-q1">-758,327</td>
        <td class="number revenue-q1">2,384,555</td>
        <td class="number revenue-q1">-593,068</td>
        <td class="number revenue-q1">1,791,487</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">1,791,487</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
      </tr>
      <tr class="row-a4">
        <td rowspan="4" class="year-odd">2017</td>
        <td class="quarter-odd">A4</td>
        <td class="number assets-non-a4">66,747,565</td>
        <td class="number assets-cur-a4">57,894,863</td>
        <td class="number assets-tot-a4">124,642,427</td>
        <td class="number equity-a4">60,112,656</td>
        <td class="number liabilities-non-a4">10,589,580</td>
        <td class="number liabilities-cur-a4">53,940,192</td>
        <td class="number liabilities-tot-a4">64,529,773</td>
        <td class="number revenue-a4">100,766,069</td>
        <td class="number revenue-a4">-78,894,106</td>
        <td class="number revenue-a4">21,871,963</td>
        <td class="number revenue-a4">-3,413,950</td>
        <td class="number revenue-a4">18,458,013</td>
        <td class="number revenue-a4">-4,807,092</td>
        <td class="number revenue-a4">13,650,921</td>
        <td class="number revenue-a4">0</td>
        <td class="number revenue-a4">13,650,921</td>
        <td class="number cashflow-a4">3,310,652</td>
        <td class="number cashflow-a4">-3,035,069</td>
        <td class="number cashflow-a4">-2,140,216</td>
        <td class="number cashflow-a4">-1,864,633</td>
        <td class="number cashflow-a4">11,501,493</td>
        <td class="number cashflow-a4">9,636,856</td>
        <td class="number cashflow-a4">-3,051,490</td>
        <td class="number cashflow-a4">259,162</td>
      </tr>
      <tr class="row-t3">
        <td class="quarter-odd">T3</td>
        <td class="number assets-non-t3">65,326,326</td>
        <td class="number assets-cur-t3">57,941,198</td>
        <td class="number assets-tot-t3">123,267,525</td>
        <td class="number equity-t3">60,250,652</td>
        <td class="number liabilities-non-t3">13,037,641</td>
        <td class="number liabilities-cur-t3">49,979,232</td>
        <td class="number liabilities-tot-t3">63,016,874</td>
        <td class="number revenue-t3">73,537,647</td>
        <td class="number revenue-t3">-49,656,495</td>
        <td class="number revenue-t3">23,881,152</td>
        <td class="number revenue-t3">-2,683,732</td>
        <td class="number revenue-t3">21,197,420</td>
        <td class="number revenue-t3">-3,328,959</td>
        <td class="number revenue-t3">17,868,461</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">17,868,461</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
      </tr>
      <tr class="row-h2">
        <td class="quarter-odd">H2</td>
        <td class="number assets-non-h2">64,890,368</td>
        <td class="number assets-cur-h2">48,568,690</td>
        <td class="number assets-tot-h2">113,459,058</td>
        <td class="number equity-h2">49,207,267</td>
        <td class="number liabilities-non-h2">9,162,120</td>
        <td class="number liabilities-cur-h2">55,089,669</td>
        <td class="number liabilities-tot-h2">64,251,789</td>
        <td class="number revenue-h2">40,859,734</td>
        <td class="number revenue-h2">-31,180,177</td>
        <td class="number revenue-h2">9,679,557</td>
        <td class="number revenue-h2">-1,723,690</td>
        <td class="number revenue-h2">7,955,867</td>
        <td class="number revenue-h2">-1,130,791</td>
        <td class="number revenue-h2">6,825,076</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">6,825,076</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
      </tr>
      <tr class="row-q1">
        <td class="quarter-odd">Q1</td>
        <td class="number assets-non-q1">62,692,528</td>
        <td class="number assets-cur-q1">49,502,203</td>
        <td class="number assets-tot-q1">112,194,731</td>
        <td class="number equity-q1">48,479,581</td>
        <td class="number liabilities-non-q1">9,231,408</td>
        <td class="number liabilities-cur-q1">54,483,740</td>
        <td class="number liabilities-tot-q1">63,715,148</td>
        <td class="number revenue-q1">19,358,538</td>
        <td class="number revenue-q1">-15,747,644</td>
        <td class="number revenue-q1">3,610,894</td>
        <td class="number revenue-q1">-852,232</td>
        <td class="number revenue-q1">2,758,662</td>
        <td class="number revenue-q1">-540,418</td>
        <td class="number revenue-q1">2,218,244</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">2,218,244</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
      </tr>
      <tr class="row-a4">
        <td rowspan="4" class="year-even">2016</td>
        <td class="quarter-even">A4</td>
        <td class="number assets-non-a4">67,418,225</td>
        <td class="number assets-cur-a4">49,322,823</td>
        <td class="number assets-tot-a4">116,741,048</td>
        <td class="number equity-a4">46,298,318</td>
        <td class="number liabilities-non-a4">9,290,302</td>
        <td class="number liabilities-cur-a4">61,152,428</td>
        <td class="number liabilities-tot-a4">70,442,730</td>
        <td class="number revenue-a4">83,030,250</td>
        <td class="number revenue-a4">-69,561,868</td>
        <td class="number revenue-a4">13,468,382</td>
        <td class="number revenue-a4">-131,569</td>
        <td class="number revenue-a4">13,336,813</td>
        <td class="number revenue-a4">-2,463,768</td>
        <td class="number revenue-a4">10,873,045</td>
        <td class="number revenue-a4">-221,439</td>
        <td class="number revenue-a4">10,651,606</td>
        <td class="number cashflow-a4">16,384,987</td>
        <td class="number cashflow-a4">-5,162,401</td>
        <td class="number cashflow-a4">-6,839,951</td>
        <td class="number cashflow-a4">4,382,635</td>
        <td class="number cashflow-a4">7,118,858</td>
        <td class="number cashflow-a4">11,501,493</td>
        <td class="number cashflow-a4">-5,175,568</td>
        <td class="number cashflow-a4">11,209,419</td>
      </tr>
      <tr class="row-t3">
        <td class="quarter-even">T3</td>
        <td class="number assets-non-t3">65,873,710</td>
        <td class="number assets-cur-t3">48,190,933</td>
        <td class="number assets-tot-t3">114,064,643</td>
        <td class="number equity-t3">43,216,939</td>
        <td class="number liabilities-non-t3">13,216,165</td>
        <td class="number liabilities-cur-t3">57,631,539</td>
        <td class="number liabilities-tot-t3">70,847,704</td>
        <td class="number revenue-t3">45,428,027</td>
        <td class="number revenue-t3">-37,162,011</td>
        <td class="number revenue-t3">8,266,016</td>
        <td class="number revenue-t3">598,505</td>
        <td class="number revenue-t3">8,864,521</td>
        <td class="number revenue-t3">-1,375,324</td>
        <td class="number revenue-t3">7,489,197</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">7,489,197</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
      </tr>
      <tr class="row-h2">
        <td class="quarter-even">H2</td>
        <td class="number assets-non-h2">65,500,301</td>
        <td class="number assets-cur-h2">49,843,103</td>
        <td class="number assets-tot-h2">115,343,405</td>
        <td class="number equity-h2">39,893,159</td>
        <td class="number liabilities-non-h2">13,216,165</td>
        <td class="number liabilities-cur-h2">62,234,080</td>
        <td class="number liabilities-tot-h2">75,450,245</td>
        <td class="number revenue-h2">26,037,174</td>
        <td class="number revenue-h2">-22,160,161</td>
        <td class="number revenue-h2">3,877,013</td>
        <td class="number revenue-h2">1,333,869</td>
        <td class="number revenue-h2">5,210,882</td>
        <td class="number revenue-h2">-1,075,568</td>
        <td class="number revenue-h2">4,135,314</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">4,135,314</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
      </tr>
      <tr class="row-q1">
        <td class="quarter-even">Q1</td>
        <td class="number assets-non-q1">65,572,421</td>
        <td class="number assets-cur-q1">46,523,195</td>
        <td class="number assets-tot-q1">112,095,616</td>
        <td class="number equity-q1">37,705,030</td>
        <td class="number liabilities-non-q1">13,215,554</td>
        <td class="number liabilities-cur-q1">61,175,030</td>
        <td class="number liabilities-tot-q1">74,390,584</td>
        <td class="number revenue-q1">9,335,996</td>
        <td class="number revenue-q1">-6,505,008</td>
        <td class="number revenue-q1">2,830,988</td>
        <td class="number revenue-q1">-717,605</td>
        <td class="number revenue-q1">2,113,383</td>
        <td class="number revenue-q1">-470,168</td>
        <td class="number revenue-q1">1,643,215</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">1,643,215</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
      </tr>
    </tbody>
  </table>
</div>

## Valuation

**Coming soon...**
