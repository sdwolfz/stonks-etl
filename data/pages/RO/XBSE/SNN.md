---
title: "S.N. NUCLEARELECTRICA S.A. (SNN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xbse/">XBSE</a></td></tr>
    <tr><td>Name</td><td>S.N. NUCLEARELECTRICA S.A.</td></tr>
    <tr><td>Symbol</td><td>SNN</td></tr>
    <tr><td>Web</td><td><a href="https://www.nuclearelectrica.ro">www.nuclearelectrica.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 1.565148 |
| 2019 | 1.653063 |
| 2018 | 1.256706 |
| 2017 | 1.61 |
| 2017 | 0.9 |
| 2016 | 0.364825 |
| 2016 | 0.33 |
| 2015 | 0.33 |
| 2014 | 0.3 |
| 2013 | 1.21 |

### Reports

<style>
  .reports-table {
    width: 100%;
    display: block;
    overflow-x: auto;
    overflow-y: scroll;
    scrollbar-width: none;
    max-height: 50rem;
    border: 0.5rem solid #D8D8D8;
  }
  .reports-table table {
    border: none;
    padding: 0;
  }
  .reports-table th {
    border: none;
    padding: 0;
  }
  .reports-table td {
    border: none
    padding: 0;
  }

  table.reports {
    border-collapse: collapse;
  }

  table.reports thead th, table.reports tbody td {
    padding: 3px 5px;
  }

  table.reports td {
    border: 0px solid #FFFFFF;
  }

  table.reports thead .header-top {
    background-color: #000000;
    color: #FFFFFF;
  }

  table.reports thead .header-assets {
    background-color: #3D85C6;
    color: #FFFFFF;
  }

  table.reports thead .header-assets-non {
    background-color: #A2C4C9;
  }

  table.reports thead .header-assets-cur {
    background-color: #9FC5E8;
  }

  table.reports thead .header-assets-tot {
    background-color: #3D85C6;
    color: #FFFFFF;
  }

  table.reports thead .header-equity {
    background-color: #6AA84F;
    color: #FFFFFF;
  }

  table.reports thead .header-lia-non {
    background-color: #D5A6BD;
  }

  table.reports thead .header-lia-cur {
    background-color: #EA9999;
  }

  table.reports thead .header-lia-tot {
    background-color: #A64D79;
    color: #FFFFFF;
  }

  table.reports thead .header-equity {
    background-color: #6AA84F;
    color: #FFFFFF;
  }

  table.reports thead .header-liabilities {
    background-color: #A64D79;
    color: #FFFFFF;
  }

  table.reports thead .header-revenue {
    background-color: #674EA6;
    color: #FFFFFF;
  }

  table.reports thead .header-cashflow {
    background-color: #E69138;
    color: #FFFFFF;
  }

  table.reports tbody td.number {
    text-align: right;
  }

  table.reports tbody td.year-even, table.reports tbody td.quarter-even {
    text-align: center;
    background-color: #B7B7B7;
  }

  table.reports tbody td.year-odd, table.reports tbody td.quarter-odd {
    text-align: center;
    background-color: #D9D9D9;
  }

  table.reports tbody td.assets-non-h2 {
    background-color: #D0E0E3;
  }
  table.reports tbody td.assets-cur-h2 {
    background-color: #CFE2F3;
  }
  table.reports tbody td.assets-tot-h2 {
    background-color: #CFE2F3;
  }
  table.reports tbody td.assets-non-t3 {
    background-color: #A2C4C9;
  }
  table.reports tbody td.assets-cur-t3 {
    background-color: #9FC5E8;
  }
  table.reports tbody td.assets-tot-t3 {
    background-color: #9FC5E8;
  }
  table.reports tbody td.assets-non-a4 {
    background-color: #76A5AF;
  }
  table.reports tbody td.assets-cur-a4 {
    background-color: #6FA8DC;
  }
  table.reports tbody td.assets-tot-a4 {
    background-color: #6FA8DC;
  }

  table.reports tbody td.equity-h2 {
    background-color: #D9EAD3;
  }
  table.reports tbody td.equity-t3 {
    background-color: #B6D7A8;
  }
  table.reports tbody td.equity-a4 {
    background-color: #93C47D;
  }

  table.reports tbody td.liabilities-non-h2 {
    background-color: #EAD1DC;
  }
  table.reports tbody td.liabilities-cur-h2 {
    background-color: #F4CCCC;
  }
  table.reports tbody td.liabilities-tot-h2 {
    background-color: #F4CCCC;
  }
  table.reports tbody td.liabilities-non-t3 {
    background-color: #D5A6BD;
  }
  table.reports tbody td.liabilities-cur-t3 {
    background-color: #EA9999;
  }
  table.reports tbody td.liabilities-tot-t3 {
    background-color: #EA9999;
  }
  table.reports tbody td.liabilities-non-a4 {
    background-color: #C27BA0;
  }
  table.reports tbody td.liabilities-cur-a4 {
    background-color: #E06666;
  }
  table.reports tbody td.liabilities-tot-a4 {
    background-color: #E06666;
  }

  table.reports tbody td.revenue-h2 {
    background-color: #D9D2E9;
  }
  table.reports tbody td.revenue-t3 {
    background-color: #B4A7D6;
  }
  table.reports tbody td.revenue-a4 {
    background-color: #8E7CC3;
  }

  table.reports tbody td.cashflow-h2 {
    background-color: #FCE5CD;
  }
  table.reports tbody td.cashflow-t3 {
    background-color: #F9CB9C;
  }
  table.reports tbody td.cashflow-a4 {
    background-color: #F6B26B;
  }
</style>

<div class="reports-table">
  <table class="reports">
    <thead>
      <tr>
        <th rowspan="3" class="header-top">Year</th>
        <th rowspan="3" class="header-top">Quarter</th>
        <th colspan="7" class="header-top">Balance</th>
        <th colspan="9" class="header-top">Revenue</th>
        <th colspan="8" class="header-top">Cashflow</th>
      </tr>
      <tr>
        <th colspan="3" class="header-assets">Assets</th>
        <th             class="header-equity">Equity</th>
        <th colspan="3" class="header-liabilities">Liabilities</th>
        <th rowspan="2" class="header-revenue">Sales</th>
        <th rowspan="2" class="header-revenue">Operating Expenses</th>
        <th rowspan="2" class="header-revenue">Operating Profit</th>
        <th rowspan="2" class="header-revenue">Financial Resut</th>
        <th rowspan="2" class="header-revenue">Profit Before Taxes</th>
        <th rowspan="2" class="header-revenue">Tax On Gains</th>
        <th rowspan="2" class="header-revenue">Net Profit</th>
        <th rowspan="2" class="header-revenue">Other</th>
        <th rowspan="2" class="header-revenue">Total Result</th>
        <th rowspan="2" class="header-cashflow">Operating</th>
        <th rowspan="2" class="header-cashflow">Investment</th>
        <th rowspan="2" class="header-cashflow">Financing</th>
        <th rowspan="2" class="header-cashflow">Total</th>
        <th rowspan="2" class="header-cashflow">Cash at Start</th>
        <th rowspan="2" class="header-cashflow">Cash at End</th>
        <th rowspan="2" class="header-cashflow">Capex</th>
        <th rowspan="2" class="header-cashflow">Free Cash Flow</th>
      </tr>
      <tr>
        <th class="header-assets-non">Noncurrent</th>
        <th class="header-assets-cur">Current</th>
        <th class="header-assets-tot">Total</th>
        <th class="header-equity">Total</th>
        <th class="header-lia-non">Noncurrent</th>
        <th class="header-lia-cur">Current</th>
        <th class="header-lia-tot">Total</th>
      <tr>
    </thead>
    <tbody>
      <tr class="row-q1">
        <td rowspan="1" class="year-even">2021</td>
        <td class="quarter-even">Q1</td>
        <td class="number assets-non-q1">0</td>
        <td class="number assets-cur-q1">0</td>
        <td class="number assets-tot-q1">0</td>
        <td class="number equity-q1">0</td>
        <td class="number liabilities-non-q1">0</td>
        <td class="number liabilities-cur-q1">0</td>
        <td class="number liabilities-tot-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
      </tr>
      <tr class="row-a4">
        <td rowspan="4" class="year-odd">2020</td>
        <td class="quarter-odd">A4</td>
        <td class="number assets-non-a4">5,999,643,468</td>
        <td class="number assets-cur-a4">2,844,601,772</td>
        <td class="number assets-tot-a4">8,844,245,240</td>
        <td class="number equity-a4">7,519,329,908</td>
        <td class="number liabilities-non-a4">722,100,501</td>
        <td class="number liabilities-cur-a4">602,814,831</td>
        <td class="number liabilities-tot-a4">1,324,915,332</td>
        <td class="number revenue-a4">2,500,166,078</td>
        <td class="number revenue-a4">-1,728,780,034</td>
        <td class="number revenue-a4">771,386,044</td>
        <td class="number revenue-a4">43,911,697</td>
        <td class="number revenue-a4">815,297,741</td>
        <td class="number revenue-a4">-116,086,386</td>
        <td class="number revenue-a4">699,211,355</td>
        <td class="number revenue-a4">11,746,094</td>
        <td class="number revenue-a4">710,957,449</td>
        <td class="number cashflow-a4">1,237,323,222</td>
        <td class="number cashflow-a4">-1,767,332,505</td>
        <td class="number cashflow-a4">-716,624,947</td>
        <td class="number cashflow-a4">-1,246,634,229</td>
        <td class="number cashflow-a4">1,793,501,617</td>
        <td class="number cashflow-a4">546,867,387</td>
        <td class="number cashflow-a4">-243,794,947</td>
        <td class="number cashflow-a4">993,528,275</td>
      </tr>
      <tr class="row-t3">
        <td class="quarter-odd">T3</td>
        <td class="number assets-non-t3">0</td>
        <td class="number assets-cur-t3">0</td>
        <td class="number assets-tot-t3">0</td>
        <td class="number equity-t3">0</td>
        <td class="number liabilities-non-t3">0</td>
        <td class="number liabilities-cur-t3">0</td>
        <td class="number liabilities-tot-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
      </tr>
      <tr class="row-h2">
        <td class="quarter-odd">H2</td>
        <td class="number assets-non-h2">0</td>
        <td class="number assets-cur-h2">0</td>
        <td class="number assets-tot-h2">0</td>
        <td class="number equity-h2">0</td>
        <td class="number liabilities-non-h2">0</td>
        <td class="number liabilities-cur-h2">0</td>
        <td class="number liabilities-tot-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
      </tr>
      <tr class="row-q1">
        <td class="quarter-odd">Q1</td>
        <td class="number assets-non-q1">0</td>
        <td class="number assets-cur-q1">0</td>
        <td class="number assets-tot-q1">0</td>
        <td class="number equity-q1">0</td>
        <td class="number liabilities-non-q1">0</td>
        <td class="number liabilities-cur-q1">0</td>
        <td class="number liabilities-tot-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
      </tr>
      <tr class="row-a4">
        <td rowspan="4" class="year-even">2019</td>
        <td class="quarter-even">A4</td>
        <td class="number assets-non-a4">6,305,089,387</td>
        <td class="number assets-cur-a4">2,505,682,168</td>
        <td class="number assets-tot-a4">8,810,771,555</td>
        <td class="number equity-a4">7,334,689,593</td>
        <td class="number liabilities-non-a4">936,156,877</td>
        <td class="number liabilities-cur-a4">539,925,085</td>
        <td class="number liabilities-tot-a4">1,476,081,962</td>
        <td class="number revenue-a4">2,417,422,837</td>
        <td class="number revenue-a4">-1,788,007,394</td>
        <td class="number revenue-a4">629,415,443</td>
        <td class="number revenue-a4">1,756,531</td>
        <td class="number revenue-a4">631,171,974</td>
        <td class="number revenue-a4">-95,607,765</td>
        <td class="number revenue-a4">535,563,109</td>
        <td class="number revenue-a4">28,461,962</td>
        <td class="number revenue-a4">564,025,071</td>
        <td class="number cashflow-a4">992,501,618</td>
        <td class="number cashflow-a4">-222,565,706</td>
        <td class="number cashflow-a4">-588,393,037</td>
        <td class="number cashflow-a4">181,542,875</td>
        <td class="number cashflow-a4">1,611,958,742</td>
        <td class="number cashflow-a4">1,793,501,617</td>
        <td class="number cashflow-a4">-252,421,334</td>
        <td class="number cashflow-a4">740,080,284</td>
      </tr>
      <tr class="row-t3">
        <td class="quarter-even">T3</td>
        <td class="number assets-non-t3">0</td>
        <td class="number assets-cur-t3">0</td>
        <td class="number assets-tot-t3">0</td>
        <td class="number equity-t3">0</td>
        <td class="number liabilities-non-t3">0</td>
        <td class="number liabilities-cur-t3">0</td>
        <td class="number liabilities-tot-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
      </tr>
      <tr class="row-h2">
        <td class="quarter-even">H2</td>
        <td class="number assets-non-h2">0</td>
        <td class="number assets-cur-h2">0</td>
        <td class="number assets-tot-h2">0</td>
        <td class="number equity-h2">0</td>
        <td class="number liabilities-non-h2">0</td>
        <td class="number liabilities-cur-h2">0</td>
        <td class="number liabilities-tot-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
      </tr>
      <tr class="row-q1">
        <td class="quarter-even">Q1</td>
        <td class="number assets-non-q1">0</td>
        <td class="number assets-cur-q1">0</td>
        <td class="number assets-tot-q1">0</td>
        <td class="number equity-q1">0</td>
        <td class="number liabilities-non-q1">0</td>
        <td class="number liabilities-cur-q1">0</td>
        <td class="number liabilities-tot-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
      </tr>
      <tr class="row-a4">
        <td rowspan="4" class="year-odd">2018</td>
        <td class="quarter-odd">A4</td>
        <td class="number assets-non-a4">6,673,098,518</td>
        <td class="number assets-cur-a4">2,193,165,640</td>
        <td class="number assets-tot-a4">8,866,264,158</td>
        <td class="number equity-a4">7,178,990,289</td>
        <td class="number liabilities-non-a4">1,122,870,108</td>
        <td class="number liabilities-cur-a4">564,403,761</td>
        <td class="number liabilities-tot-a4">1,687,273,869</td>
        <td class="number revenue-a4">2,178,862,501</td>
        <td class="number revenue-a4">-1,642,333,345</td>
        <td class="number revenue-a4">536,529,156</td>
        <td class="number revenue-a4">36,048,912</td>
        <td class="number revenue-a4">572,578,068</td>
        <td class="number revenue-a4">-162,012,099</td>
        <td class="number revenue-a4">410,565,969</td>
        <td class="number revenue-a4">41,202,404</td>
        <td class="number revenue-a4">451,768,373</td>
        <td class="number cashflow-a4">1,023,881,146</td>
        <td class="number cashflow-a4">-163,456,059</td>
        <td class="number cashflow-a4">-962,300,424</td>
        <td class="number cashflow-a4">-101,875,337</td>
        <td class="number cashflow-a4">1,713,834,079</td>
        <td class="number cashflow-a4">1,611,958,742</td>
        <td class="number cashflow-a4">-141,517,979</td>
        <td class="number cashflow-a4">882,363,167</td>
      </tr>
      <tr class="row-t3">
        <td class="quarter-odd">T3</td>
        <td class="number assets-non-t3">0</td>
        <td class="number assets-cur-t3">0</td>
        <td class="number assets-tot-t3">0</td>
        <td class="number equity-t3">0</td>
        <td class="number liabilities-non-t3">0</td>
        <td class="number liabilities-cur-t3">0</td>
        <td class="number liabilities-tot-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
      </tr>
      <tr class="row-h2">
        <td class="quarter-odd">H2</td>
        <td class="number assets-non-h2">0</td>
        <td class="number assets-cur-h2">0</td>
        <td class="number assets-tot-h2">0</td>
        <td class="number equity-h2">0</td>
        <td class="number liabilities-non-h2">0</td>
        <td class="number liabilities-cur-h2">0</td>
        <td class="number liabilities-tot-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
      </tr>
      <tr class="row-q1">
        <td class="quarter-odd">Q1</td>
        <td class="number assets-non-q1">0</td>
        <td class="number assets-cur-q1">0</td>
        <td class="number assets-tot-q1">0</td>
        <td class="number equity-q1">0</td>
        <td class="number liabilities-non-q1">0</td>
        <td class="number liabilities-cur-q1">0</td>
        <td class="number liabilities-tot-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
      </tr>
      <tr class="row-a4">
        <td rowspan="4" class="year-even">2017</td>
        <td class="quarter-even">A4</td>
        <td class="number assets-non-a4">7,004,617,664</td>
        <td class="number assets-cur-a4">2,239,856,112</td>
        <td class="number assets-tot-a4">9,244,473,776</td>
        <td class="number equity-a4">7,484,021,682</td>
        <td class="number liabilities-non-a4">1,308,218,811</td>
        <td class="number liabilities-cur-a4">452,233,283</td>
        <td class="number liabilities-tot-a4">1,760,452,094</td>
        <td class="number revenue-a4">1,932,904,452</td>
        <td class="number revenue-a4">-1,554,912,157</td>
        <td class="number revenue-a4">377,992,295</td>
        <td class="number revenue-a4">-18,452,089</td>
        <td class="number revenue-a4">359,540,206</td>
        <td class="number revenue-a4">-53,002,126</td>
        <td class="number revenue-a4">306,538,080</td>
        <td class="number revenue-a4">703,726</td>
        <td class="number revenue-a4">307,241,806</td>
        <td class="number cashflow-a4">893,951,156</td>
        <td class="number cashflow-a4">-207,308,352</td>
        <td class="number cashflow-a4">-432,958,123</td>
        <td class="number cashflow-a4">253,684,682</td>
        <td class="number cashflow-a4">1,460,149,397</td>
        <td class="number cashflow-a4">1,713,834,079</td>
        <td class="number cashflow-a4">-147,338,985</td>
        <td class="number cashflow-a4">746,612,171</td>
      </tr>
      <tr class="row-t3">
        <td class="quarter-even">T3</td>
        <td class="number assets-non-t3">0</td>
        <td class="number assets-cur-t3">0</td>
        <td class="number assets-tot-t3">0</td>
        <td class="number equity-t3">0</td>
        <td class="number liabilities-non-t3">0</td>
        <td class="number liabilities-cur-t3">0</td>
        <td class="number liabilities-tot-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
      </tr>
      <tr class="row-h2">
        <td class="quarter-even">H2</td>
        <td class="number assets-non-h2">0</td>
        <td class="number assets-cur-h2">0</td>
        <td class="number assets-tot-h2">0</td>
        <td class="number equity-h2">0</td>
        <td class="number liabilities-non-h2">0</td>
        <td class="number liabilities-cur-h2">0</td>
        <td class="number liabilities-tot-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
      </tr>
      <tr class="row-q1">
        <td class="quarter-even">Q1</td>
        <td class="number assets-non-q1">0</td>
        <td class="number assets-cur-q1">0</td>
        <td class="number assets-tot-q1">0</td>
        <td class="number equity-q1">0</td>
        <td class="number liabilities-non-q1">0</td>
        <td class="number liabilities-cur-q1">0</td>
        <td class="number liabilities-tot-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
      </tr>
      <tr class="row-a4">
        <td rowspan="4" class="year-odd">2016</td>
        <td class="quarter-odd">A4</td>
        <td class="number assets-non-a4">7,293,088,871</td>
        <td class="number assets-cur-a4">1,992,714,672</td>
        <td class="number assets-tot-a4">9,285,803,543</td>
        <td class="number equity-a4">7,335,955,282</td>
        <td class="number liabilities-non-a4">1,512,955,959</td>
        <td class="number liabilities-cur-a4">436,892,302</td>
        <td class="number liabilities-tot-a4">1,949,848,261</td>
        <td class="number revenue-a4">1,680,222,450</td>
        <td class="number revenue-a4">-1,524,984,337</td>
        <td class="number revenue-a4">155,238,113</td>
        <td class="number revenue-a4">-29,012,636</td>
        <td class="number revenue-a4">126,225,477</td>
        <td class="number revenue-a4">-14,721,958</td>
        <td class="number revenue-a4">111,503,519</td>
        <td class="number revenue-a4">-59,915</td>
        <td class="number revenue-a4">111,443,604</td>
        <td class="number cashflow-a4">575,935,553</td>
        <td class="number cashflow-a4">-260,462,824</td>
        <td class="number cashflow-a4">-311,215,537</td>
        <td class="number cashflow-a4">4,257,191</td>
        <td class="number cashflow-a4">259,943,096</td>
        <td class="number cashflow-a4">264,200,287</td>
        <td class="number cashflow-a4">-119,320,125</td>
        <td class="number cashflow-a4">456,615,428</td>
      </tr>
      <tr class="row-t3">
        <td class="quarter-odd">T3</td>
        <td class="number assets-non-t3">0</td>
        <td class="number assets-cur-t3">0</td>
        <td class="number assets-tot-t3">0</td>
        <td class="number equity-t3">0</td>
        <td class="number liabilities-non-t3">0</td>
        <td class="number liabilities-cur-t3">0</td>
        <td class="number liabilities-tot-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
      </tr>
      <tr class="row-h2">
        <td class="quarter-odd">H2</td>
        <td class="number assets-non-h2">0</td>
        <td class="number assets-cur-h2">0</td>
        <td class="number assets-tot-h2">0</td>
        <td class="number equity-h2">0</td>
        <td class="number liabilities-non-h2">0</td>
        <td class="number liabilities-cur-h2">0</td>
        <td class="number liabilities-tot-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
      </tr>
      <tr class="row-q1">
        <td class="quarter-odd">Q1</td>
        <td class="number assets-non-q1">0</td>
        <td class="number assets-cur-q1">0</td>
        <td class="number assets-tot-q1">0</td>
        <td class="number equity-q1">0</td>
        <td class="number liabilities-non-q1">0</td>
        <td class="number liabilities-cur-q1">0</td>
        <td class="number liabilities-tot-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
      </tr>
    </tbody>
  </table>
</div>

## Valuation

**Coming soon...**
