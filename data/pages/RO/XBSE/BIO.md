---
title: "BIOFARM S.A. (BIO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xbse/">XBSE</a></td></tr>
    <tr><td>Name</td><td>BIOFARM S.A.</td></tr>
    <tr><td>Symbol</td><td>BIO</td></tr>
    <tr><td>Web</td><td><a href="https://www.biofarm.ro">www.biofarm.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.022 |
| 2019 | 0.021 |
| 2018 | 0.01 |
| 2018 | 0.01 |
| 2017 | 0.019 |
| 2016 | 0.017 |
| 2015 | 0.016 |
| 2014 | 0.015 |
| 2012 | 0.013 |
| 2011 | 0.01 |

### Reports

<style>
  .reports-table {
    width: 100%;
    display: block;
    overflow-x: auto;
    overflow-y: scroll;
    scrollbar-width: none;
    max-height: 50rem;
    border: 0.5rem solid #D8D8D8;
  }
  .reports-table table {
    border: none;
    padding: 0;
  }
  .reports-table th {
    border: none;
    padding: 0;
  }
  .reports-table td {
    border: none
    padding: 0;
  }

  table.reports {
    border-collapse: collapse;
  }

  table.reports thead th, table.reports tbody td {
    padding: 3px 5px;
  }

  table.reports td {
    border: 0px solid #FFFFFF;
  }

  table.reports thead .header-top {
    background-color: #000000;
    color: #FFFFFF;
  }

  table.reports thead .header-assets {
    background-color: #3D85C6;
    color: #FFFFFF;
  }

  table.reports thead .header-assets-non {
    background-color: #A2C4C9;
  }

  table.reports thead .header-assets-cur {
    background-color: #9FC5E8;
  }

  table.reports thead .header-assets-tot {
    background-color: #3D85C6;
    color: #FFFFFF;
  }

  table.reports thead .header-equity {
    background-color: #6AA84F;
    color: #FFFFFF;
  }

  table.reports thead .header-lia-non {
    background-color: #D5A6BD;
  }

  table.reports thead .header-lia-cur {
    background-color: #EA9999;
  }

  table.reports thead .header-lia-tot {
    background-color: #A64D79;
    color: #FFFFFF;
  }

  table.reports thead .header-equity {
    background-color: #6AA84F;
    color: #FFFFFF;
  }

  table.reports thead .header-liabilities {
    background-color: #A64D79;
    color: #FFFFFF;
  }

  table.reports thead .header-revenue {
    background-color: #674EA6;
    color: #FFFFFF;
  }

  table.reports thead .header-cashflow {
    background-color: #E69138;
    color: #FFFFFF;
  }

  table.reports tbody td.number {
    text-align: right;
  }

  table.reports tbody td.year-even, table.reports tbody td.quarter-even {
    text-align: center;
    background-color: #B7B7B7;
  }

  table.reports tbody td.year-odd, table.reports tbody td.quarter-odd {
    text-align: center;
    background-color: #D9D9D9;
  }

  table.reports tbody td.assets-non-h2 {
    background-color: #D0E0E3;
  }
  table.reports tbody td.assets-cur-h2 {
    background-color: #CFE2F3;
  }
  table.reports tbody td.assets-tot-h2 {
    background-color: #CFE2F3;
  }
  table.reports tbody td.assets-non-t3 {
    background-color: #A2C4C9;
  }
  table.reports tbody td.assets-cur-t3 {
    background-color: #9FC5E8;
  }
  table.reports tbody td.assets-tot-t3 {
    background-color: #9FC5E8;
  }
  table.reports tbody td.assets-non-a4 {
    background-color: #76A5AF;
  }
  table.reports tbody td.assets-cur-a4 {
    background-color: #6FA8DC;
  }
  table.reports tbody td.assets-tot-a4 {
    background-color: #6FA8DC;
  }

  table.reports tbody td.equity-h2 {
    background-color: #D9EAD3;
  }
  table.reports tbody td.equity-t3 {
    background-color: #B6D7A8;
  }
  table.reports tbody td.equity-a4 {
    background-color: #93C47D;
  }

  table.reports tbody td.liabilities-non-h2 {
    background-color: #EAD1DC;
  }
  table.reports tbody td.liabilities-cur-h2 {
    background-color: #F4CCCC;
  }
  table.reports tbody td.liabilities-tot-h2 {
    background-color: #F4CCCC;
  }
  table.reports tbody td.liabilities-non-t3 {
    background-color: #D5A6BD;
  }
  table.reports tbody td.liabilities-cur-t3 {
    background-color: #EA9999;
  }
  table.reports tbody td.liabilities-tot-t3 {
    background-color: #EA9999;
  }
  table.reports tbody td.liabilities-non-a4 {
    background-color: #C27BA0;
  }
  table.reports tbody td.liabilities-cur-a4 {
    background-color: #E06666;
  }
  table.reports tbody td.liabilities-tot-a4 {
    background-color: #E06666;
  }

  table.reports tbody td.revenue-h2 {
    background-color: #D9D2E9;
  }
  table.reports tbody td.revenue-t3 {
    background-color: #B4A7D6;
  }
  table.reports tbody td.revenue-a4 {
    background-color: #8E7CC3;
  }

  table.reports tbody td.cashflow-h2 {
    background-color: #FCE5CD;
  }
  table.reports tbody td.cashflow-t3 {
    background-color: #F9CB9C;
  }
  table.reports tbody td.cashflow-a4 {
    background-color: #F6B26B;
  }
</style>

<div class="reports-table">
  <table class="reports">
    <thead>
      <tr>
        <th rowspan="3" class="header-top">Year</th>
        <th rowspan="3" class="header-top">Quarter</th>
        <th colspan="7" class="header-top">Balance</th>
        <th colspan="9" class="header-top">Revenue</th>
        <th colspan="8" class="header-top">Cashflow</th>
      </tr>
      <tr>
        <th colspan="3" class="header-assets">Assets</th>
        <th             class="header-equity">Equity</th>
        <th colspan="3" class="header-liabilities">Liabilities</th>
        <th rowspan="2" class="header-revenue">Sales</th>
        <th rowspan="2" class="header-revenue">Operating Expenses</th>
        <th rowspan="2" class="header-revenue">Operating Profit</th>
        <th rowspan="2" class="header-revenue">Financial Resut</th>
        <th rowspan="2" class="header-revenue">Profit Before Taxes</th>
        <th rowspan="2" class="header-revenue">Tax On Gains</th>
        <th rowspan="2" class="header-revenue">Net Profit</th>
        <th rowspan="2" class="header-revenue">Other</th>
        <th rowspan="2" class="header-revenue">Total Result</th>
        <th rowspan="2" class="header-cashflow">Operating</th>
        <th rowspan="2" class="header-cashflow">Investment</th>
        <th rowspan="2" class="header-cashflow">Financing</th>
        <th rowspan="2" class="header-cashflow">Total</th>
        <th rowspan="2" class="header-cashflow">Cash at Start</th>
        <th rowspan="2" class="header-cashflow">Cash at End</th>
        <th rowspan="2" class="header-cashflow">Capex</th>
        <th rowspan="2" class="header-cashflow">Free Cash Flow</th>
      </tr>
      <tr>
        <th class="header-assets-non">Noncurrent</th>
        <th class="header-assets-cur">Current</th>
        <th class="header-assets-tot">Total</th>
        <th class="header-equity">Total</th>
        <th class="header-lia-non">Noncurrent</th>
        <th class="header-lia-cur">Current</th>
        <th class="header-lia-tot">Total</th>
      <tr>
    </thead>
    <tbody>
      <tr class="row-q1">
        <td rowspan="1" class="year-even">2021</td>
        <td class="quarter-even">Q1</td>
        <td class="number assets-non-q1">164,422,306</td>
        <td class="number assets-cur-q1">226,057,663</td>
        <td class="number assets-tot-q1">390,479,969</td>
        <td class="number equity-q1">314,021,453</td>
        <td class="number liabilities-non-q1">620,888</td>
        <td class="number liabilities-cur-q1">75,837,628</td>
        <td class="number liabilities-tot-q1">76,458,516</td>
        <td class="number revenue-q1">62,772,754</td>
        <td class="number revenue-q1">-36,921,957</td>
        <td class="number revenue-q1">25,850,797</td>
        <td class="number revenue-q1">228,030</td>
        <td class="number revenue-q1">26,078,827</td>
        <td class="number revenue-q1">-3,608,905</td>
        <td class="number revenue-q1">22,469,922</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">22,469,922</td>
        <td class="number cashflow-q1">22,342,395</td>
        <td class="number cashflow-q1">15,579,992</td>
        <td class="number cashflow-q1">-29,562</td>
        <td class="number cashflow-q1">37,892,825</td>
        <td class="number cashflow-q1">54,543,584</td>
        <td class="number cashflow-q1">92,436,409</td>
        <td class="number cashflow-q1">-5,752,239</td>
        <td class="number cashflow-q1">16,590,156</td>
      </tr>
      <tr class="row-a4">
        <td rowspan="4" class="year-odd">2020</td>
        <td class="quarter-odd">A4</td>
        <td class="number assets-non-a4">160,536,594</td>
        <td class="number assets-cur-a4">211,839,520</td>
        <td class="number assets-tot-a4">372,376,114</td>
        <td class="number equity-a4">291,551,531</td>
        <td class="number liabilities-non-a4">620,888</td>
        <td class="number liabilities-cur-a4">80,203,695</td>
        <td class="number liabilities-tot-a4">80,824,583</td>
        <td class="number revenue-a4">216,722,331</td>
        <td class="number revenue-a4">-153,054,027</td>
        <td class="number revenue-a4">63,668,304</td>
        <td class="number revenue-a4">1,177,052</td>
        <td class="number revenue-a4">64,845,356</td>
        <td class="number revenue-a4">-10,580,776</td>
        <td class="number revenue-a4">54,264,580</td>
        <td class="number revenue-a4">0</td>
        <td class="number revenue-a4">54,264,580</td>
        <td class="number cashflow-a4">39,509,024</td>
        <td class="number cashflow-a4">-47,058,263</td>
        <td class="number cashflow-a4">-29,035,816</td>
        <td class="number cashflow-a4">-36,585,055</td>
        <td class="number cashflow-a4">91,128,639</td>
        <td class="number cashflow-a4">54,543,584</td>
        <td class="number cashflow-a4">-27,489,822</td>
        <td class="number cashflow-a4">12,019,202</td>
      </tr>
      <tr class="row-t3">
        <td class="quarter-odd">T3</td>
        <td class="number assets-non-t3">0</td>
        <td class="number assets-cur-t3">0</td>
        <td class="number assets-tot-t3">0</td>
        <td class="number equity-t3">0</td>
        <td class="number liabilities-non-t3">0</td>
        <td class="number liabilities-cur-t3">0</td>
        <td class="number liabilities-tot-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
      </tr>
      <tr class="row-h2">
        <td class="quarter-odd">H2</td>
        <td class="number assets-non-h2">0</td>
        <td class="number assets-cur-h2">0</td>
        <td class="number assets-tot-h2">0</td>
        <td class="number equity-h2">0</td>
        <td class="number liabilities-non-h2">0</td>
        <td class="number liabilities-cur-h2">0</td>
        <td class="number liabilities-tot-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
      </tr>
      <tr class="row-q1">
        <td class="quarter-odd">Q1</td>
        <td class="number assets-non-q1">0</td>
        <td class="number assets-cur-q1">0</td>
        <td class="number assets-tot-q1">0</td>
        <td class="number equity-q1">0</td>
        <td class="number liabilities-non-q1">0</td>
        <td class="number liabilities-cur-q1">0</td>
        <td class="number liabilities-tot-q1">0</td>
        <td class="number revenue-q1">64,180,595</td>
        <td class="number revenue-q1">-40,814,317</td>
        <td class="number revenue-q1">23,366,278</td>
        <td class="number revenue-q1">463,896</td>
        <td class="number revenue-q1">23,830,174</td>
        <td class="number revenue-q1">-5,241,257</td>
        <td class="number revenue-q1">18,588,917</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">18,588,917</td>
        <td class="number cashflow-q1">18,097,042</td>
        <td class="number cashflow-q1">-4,301,837</td>
        <td class="number cashflow-q1">-9,283,655</td>
        <td class="number cashflow-q1">4,511,550</td>
        <td class="number cashflow-q1">91,128,639</td>
        <td class="number cashflow-q1">95,640,189</td>
        <td class="number cashflow-q1">-4,771,217</td>
        <td class="number cashflow-q1">13,325,825</td>
      </tr>
      <tr class="row-a4">
        <td rowspan="4" class="year-even">2019</td>
        <td class="quarter-even">A4</td>
        <td class="number assets-non-a4">143,825,906</td>
        <td class="number assets-cur-a4">202,790,764</td>
        <td class="number assets-tot-a4">346,616,670</td>
        <td class="number equity-a4">257,230,331</td>
        <td class="number liabilities-non-a4">564,624</td>
        <td class="number liabilities-cur-a4">88,821,715</td>
        <td class="number liabilities-tot-a4">89,386,339</td>
        <td class="number revenue-a4">197,355,236</td>
        <td class="number revenue-a4">-136,463,226</td>
        <td class="number revenue-a4">60,892,010</td>
        <td class="number revenue-a4">1,576,422</td>
        <td class="number revenue-a4">62,468,452</td>
        <td class="number revenue-a4">-11,586,633</td>
        <td class="number revenue-a4">50,881,819</td>
        <td class="number revenue-a4">0</td>
        <td class="number revenue-a4">50,881,819</td>
        <td class="number cashflow-a4">72,959,729</td>
        <td class="number cashflow-a4">-18,024,424</td>
        <td class="number cashflow-a4">-9,439,244</td>
        <td class="number cashflow-a4">45,496,061</td>
        <td class="number cashflow-a4">45,632,578</td>
        <td class="number cashflow-a4">91,128,639</td>
        <td class="number cashflow-a4">-19,795,610</td>
        <td class="number cashflow-a4">53,164,119</td>
      </tr>
      <tr class="row-t3">
        <td class="quarter-even">T3</td>
        <td class="number assets-non-t3">0</td>
        <td class="number assets-cur-t3">0</td>
        <td class="number assets-tot-t3">0</td>
        <td class="number equity-t3">0</td>
        <td class="number liabilities-non-t3">0</td>
        <td class="number liabilities-cur-t3">0</td>
        <td class="number liabilities-tot-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
      </tr>
      <tr class="row-h2">
        <td class="quarter-even">H2</td>
        <td class="number assets-non-h2">0</td>
        <td class="number assets-cur-h2">0</td>
        <td class="number assets-tot-h2">0</td>
        <td class="number equity-h2">0</td>
        <td class="number liabilities-non-h2">0</td>
        <td class="number liabilities-cur-h2">0</td>
        <td class="number liabilities-tot-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
      </tr>
      <tr class="row-q1">
        <td class="quarter-even">Q1</td>
        <td class="number assets-non-q1">0</td>
        <td class="number assets-cur-q1">0</td>
        <td class="number assets-tot-q1">0</td>
        <td class="number equity-q1">0</td>
        <td class="number liabilities-non-q1">0</td>
        <td class="number liabilities-cur-q1">0</td>
        <td class="number liabilities-tot-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
      </tr>
      <tr class="row-a4">
        <td rowspan="4" class="year-odd">2018</td>
        <td class="quarter-odd">A4</td>
        <td class="number assets-non-a4">0</td>
        <td class="number assets-cur-a4">0</td>
        <td class="number assets-tot-a4">0</td>
        <td class="number equity-a4">0</td>
        <td class="number liabilities-non-a4">0</td>
        <td class="number liabilities-cur-a4">0</td>
        <td class="number liabilities-tot-a4">0</td>
        <td class="number revenue-a4">0</td>
        <td class="number revenue-a4">0</td>
        <td class="number revenue-a4">0</td>
        <td class="number revenue-a4">0</td>
        <td class="number revenue-a4">0</td>
        <td class="number revenue-a4">0</td>
        <td class="number revenue-a4">0</td>
        <td class="number revenue-a4">0</td>
        <td class="number revenue-a4">0</td>
        <td class="number cashflow-a4">0</td>
        <td class="number cashflow-a4">0</td>
        <td class="number cashflow-a4">0</td>
        <td class="number cashflow-a4">0</td>
        <td class="number cashflow-a4">0</td>
        <td class="number cashflow-a4">0</td>
        <td class="number cashflow-a4">0</td>
        <td class="number cashflow-a4">0</td>
      </tr>
      <tr class="row-t3">
        <td class="quarter-odd">T3</td>
        <td class="number assets-non-t3">0</td>
        <td class="number assets-cur-t3">0</td>
        <td class="number assets-tot-t3">0</td>
        <td class="number equity-t3">0</td>
        <td class="number liabilities-non-t3">0</td>
        <td class="number liabilities-cur-t3">0</td>
        <td class="number liabilities-tot-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
      </tr>
      <tr class="row-h2">
        <td class="quarter-odd">H2</td>
        <td class="number assets-non-h2">0</td>
        <td class="number assets-cur-h2">0</td>
        <td class="number assets-tot-h2">0</td>
        <td class="number equity-h2">0</td>
        <td class="number liabilities-non-h2">0</td>
        <td class="number liabilities-cur-h2">0</td>
        <td class="number liabilities-tot-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
      </tr>
      <tr class="row-q1">
        <td class="quarter-odd">Q1</td>
        <td class="number assets-non-q1">0</td>
        <td class="number assets-cur-q1">0</td>
        <td class="number assets-tot-q1">0</td>
        <td class="number equity-q1">0</td>
        <td class="number liabilities-non-q1">0</td>
        <td class="number liabilities-cur-q1">0</td>
        <td class="number liabilities-tot-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
      </tr>
      <tr class="row-a4">
        <td rowspan="4" class="year-even">2017</td>
        <td class="quarter-even">A4</td>
        <td class="number assets-non-a4">0</td>
        <td class="number assets-cur-a4">0</td>
        <td class="number assets-tot-a4">0</td>
        <td class="number equity-a4">0</td>
        <td class="number liabilities-non-a4">0</td>
        <td class="number liabilities-cur-a4">0</td>
        <td class="number liabilities-tot-a4">0</td>
        <td class="number revenue-a4">0</td>
        <td class="number revenue-a4">0</td>
        <td class="number revenue-a4">0</td>
        <td class="number revenue-a4">0</td>
        <td class="number revenue-a4">0</td>
        <td class="number revenue-a4">0</td>
        <td class="number revenue-a4">0</td>
        <td class="number revenue-a4">0</td>
        <td class="number revenue-a4">0</td>
        <td class="number cashflow-a4">0</td>
        <td class="number cashflow-a4">0</td>
        <td class="number cashflow-a4">0</td>
        <td class="number cashflow-a4">0</td>
        <td class="number cashflow-a4">0</td>
        <td class="number cashflow-a4">0</td>
        <td class="number cashflow-a4">0</td>
        <td class="number cashflow-a4">0</td>
      </tr>
      <tr class="row-t3">
        <td class="quarter-even">T3</td>
        <td class="number assets-non-t3">0</td>
        <td class="number assets-cur-t3">0</td>
        <td class="number assets-tot-t3">0</td>
        <td class="number equity-t3">0</td>
        <td class="number liabilities-non-t3">0</td>
        <td class="number liabilities-cur-t3">0</td>
        <td class="number liabilities-tot-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
      </tr>
      <tr class="row-h2">
        <td class="quarter-even">H2</td>
        <td class="number assets-non-h2">0</td>
        <td class="number assets-cur-h2">0</td>
        <td class="number assets-tot-h2">0</td>
        <td class="number equity-h2">0</td>
        <td class="number liabilities-non-h2">0</td>
        <td class="number liabilities-cur-h2">0</td>
        <td class="number liabilities-tot-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
      </tr>
      <tr class="row-q1">
        <td class="quarter-even">Q1</td>
        <td class="number assets-non-q1">0</td>
        <td class="number assets-cur-q1">0</td>
        <td class="number assets-tot-q1">0</td>
        <td class="number equity-q1">0</td>
        <td class="number liabilities-non-q1">0</td>
        <td class="number liabilities-cur-q1">0</td>
        <td class="number liabilities-tot-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
      </tr>
      <tr class="row-a4">
        <td rowspan="4" class="year-odd">2016</td>
        <td class="quarter-odd">A4</td>
        <td class="number assets-non-a4">0</td>
        <td class="number assets-cur-a4">0</td>
        <td class="number assets-tot-a4">0</td>
        <td class="number equity-a4">0</td>
        <td class="number liabilities-non-a4">0</td>
        <td class="number liabilities-cur-a4">0</td>
        <td class="number liabilities-tot-a4">0</td>
        <td class="number revenue-a4">0</td>
        <td class="number revenue-a4">0</td>
        <td class="number revenue-a4">0</td>
        <td class="number revenue-a4">0</td>
        <td class="number revenue-a4">0</td>
        <td class="number revenue-a4">0</td>
        <td class="number revenue-a4">0</td>
        <td class="number revenue-a4">0</td>
        <td class="number revenue-a4">0</td>
        <td class="number cashflow-a4">0</td>
        <td class="number cashflow-a4">0</td>
        <td class="number cashflow-a4">0</td>
        <td class="number cashflow-a4">0</td>
        <td class="number cashflow-a4">0</td>
        <td class="number cashflow-a4">0</td>
        <td class="number cashflow-a4">0</td>
        <td class="number cashflow-a4">0</td>
      </tr>
      <tr class="row-t3">
        <td class="quarter-odd">T3</td>
        <td class="number assets-non-t3">0</td>
        <td class="number assets-cur-t3">0</td>
        <td class="number assets-tot-t3">0</td>
        <td class="number equity-t3">0</td>
        <td class="number liabilities-non-t3">0</td>
        <td class="number liabilities-cur-t3">0</td>
        <td class="number liabilities-tot-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
      </tr>
      <tr class="row-h2">
        <td class="quarter-odd">H2</td>
        <td class="number assets-non-h2">0</td>
        <td class="number assets-cur-h2">0</td>
        <td class="number assets-tot-h2">0</td>
        <td class="number equity-h2">0</td>
        <td class="number liabilities-non-h2">0</td>
        <td class="number liabilities-cur-h2">0</td>
        <td class="number liabilities-tot-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
      </tr>
      <tr class="row-q1">
        <td class="quarter-odd">Q1</td>
        <td class="number assets-non-q1">0</td>
        <td class="number assets-cur-q1">0</td>
        <td class="number assets-tot-q1">0</td>
        <td class="number equity-q1">0</td>
        <td class="number liabilities-non-q1">0</td>
        <td class="number liabilities-cur-q1">0</td>
        <td class="number liabilities-tot-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
      </tr>
    </tbody>
  </table>
</div>

## Valuation

**Coming soon...**
