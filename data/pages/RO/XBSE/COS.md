---
title: "COS TARGOVISTE S.A. (COS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xbse/">XBSE</a></td></tr>
    <tr><td>Name</td><td>COS TARGOVISTE S.A.</td></tr>
    <tr><td>Symbol</td><td>COS</td></tr>
    <tr><td>Web</td><td><a href="https://www.cos-tgv.ro">www.cos-tgv.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
