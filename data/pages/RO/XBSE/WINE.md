---
title: "PURCARI WINERIES PUBLIC COMPANY LIMITED (WINE)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xbse/">XBSE</a></td></tr>
    <tr><td>Name</td><td>PURCARI WINERIES PUBLIC COMPANY LIMITED</td></tr>
    <tr><td>Symbol</td><td>WINE</td></tr>
    <tr><td>Web</td><td><a href="https://www.purcari.wine">www.purcari.wine</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 1.3 |
| 2018 | 0.95 |

### Reports

<style>
  .reports-table {
    width: 100%;
    display: block;
    overflow-x: auto;
    overflow-y: scroll;
    scrollbar-width: none;
    max-height: 50rem;
    border: 0.5rem solid #D8D8D8;
  }
  .reports-table table {
    border: none;
    padding: 0;
  }
  .reports-table th {
    border: none;
    padding: 0;
  }
  .reports-table td {
    border: none
    padding: 0;
  }

  table.reports {
    border-collapse: collapse;
  }

  table.reports thead th, table.reports tbody td {
    padding: 3px 5px;
  }

  table.reports td {
    border: 0px solid #FFFFFF;
  }

  table.reports thead .header-top {
    background-color: #000000;
    color: #FFFFFF;
  }

  table.reports thead .header-assets {
    background-color: #3D85C6;
    color: #FFFFFF;
  }

  table.reports thead .header-assets-non {
    background-color: #A2C4C9;
  }

  table.reports thead .header-assets-cur {
    background-color: #9FC5E8;
  }

  table.reports thead .header-assets-tot {
    background-color: #3D85C6;
    color: #FFFFFF;
  }

  table.reports thead .header-equity {
    background-color: #6AA84F;
    color: #FFFFFF;
  }

  table.reports thead .header-lia-non {
    background-color: #D5A6BD;
  }

  table.reports thead .header-lia-cur {
    background-color: #EA9999;
  }

  table.reports thead .header-lia-tot {
    background-color: #A64D79;
    color: #FFFFFF;
  }

  table.reports thead .header-equity {
    background-color: #6AA84F;
    color: #FFFFFF;
  }

  table.reports thead .header-liabilities {
    background-color: #A64D79;
    color: #FFFFFF;
  }

  table.reports thead .header-revenue {
    background-color: #674EA6;
    color: #FFFFFF;
  }

  table.reports thead .header-cashflow {
    background-color: #E69138;
    color: #FFFFFF;
  }

  table.reports tbody td.number {
    text-align: right;
  }

  table.reports tbody td.year-even, table.reports tbody td.quarter-even {
    text-align: center;
    background-color: #B7B7B7;
  }

  table.reports tbody td.year-odd, table.reports tbody td.quarter-odd {
    text-align: center;
    background-color: #D9D9D9;
  }

  table.reports tbody td.assets-non-h2 {
    background-color: #D0E0E3;
  }
  table.reports tbody td.assets-cur-h2 {
    background-color: #CFE2F3;
  }
  table.reports tbody td.assets-tot-h2 {
    background-color: #CFE2F3;
  }
  table.reports tbody td.assets-non-t3 {
    background-color: #A2C4C9;
  }
  table.reports tbody td.assets-cur-t3 {
    background-color: #9FC5E8;
  }
  table.reports tbody td.assets-tot-t3 {
    background-color: #9FC5E8;
  }
  table.reports tbody td.assets-non-a4 {
    background-color: #76A5AF;
  }
  table.reports tbody td.assets-cur-a4 {
    background-color: #6FA8DC;
  }
  table.reports tbody td.assets-tot-a4 {
    background-color: #6FA8DC;
  }

  table.reports tbody td.equity-h2 {
    background-color: #D9EAD3;
  }
  table.reports tbody td.equity-t3 {
    background-color: #B6D7A8;
  }
  table.reports tbody td.equity-a4 {
    background-color: #93C47D;
  }

  table.reports tbody td.liabilities-non-h2 {
    background-color: #EAD1DC;
  }
  table.reports tbody td.liabilities-cur-h2 {
    background-color: #F4CCCC;
  }
  table.reports tbody td.liabilities-tot-h2 {
    background-color: #F4CCCC;
  }
  table.reports tbody td.liabilities-non-t3 {
    background-color: #D5A6BD;
  }
  table.reports tbody td.liabilities-cur-t3 {
    background-color: #EA9999;
  }
  table.reports tbody td.liabilities-tot-t3 {
    background-color: #EA9999;
  }
  table.reports tbody td.liabilities-non-a4 {
    background-color: #C27BA0;
  }
  table.reports tbody td.liabilities-cur-a4 {
    background-color: #E06666;
  }
  table.reports tbody td.liabilities-tot-a4 {
    background-color: #E06666;
  }

  table.reports tbody td.revenue-h2 {
    background-color: #D9D2E9;
  }
  table.reports tbody td.revenue-t3 {
    background-color: #B4A7D6;
  }
  table.reports tbody td.revenue-a4 {
    background-color: #8E7CC3;
  }

  table.reports tbody td.cashflow-h2 {
    background-color: #FCE5CD;
  }
  table.reports tbody td.cashflow-t3 {
    background-color: #F9CB9C;
  }
  table.reports tbody td.cashflow-a4 {
    background-color: #F6B26B;
  }
</style>

<div class="reports-table">
  <table class="reports">
    <thead>
      <tr>
        <th rowspan="3" class="header-top">Year</th>
        <th rowspan="3" class="header-top">Quarter</th>
        <th colspan="7" class="header-top">Balance</th>
        <th colspan="9" class="header-top">Revenue</th>
        <th colspan="8" class="header-top">Cashflow</th>
      </tr>
      <tr>
        <th colspan="3" class="header-assets">Assets</th>
        <th             class="header-equity">Equity</th>
        <th colspan="3" class="header-liabilities">Liabilities</th>
        <th rowspan="2" class="header-revenue">Sales</th>
        <th rowspan="2" class="header-revenue">Operating Expenses</th>
        <th rowspan="2" class="header-revenue">Operating Profit</th>
        <th rowspan="2" class="header-revenue">Financial Resut</th>
        <th rowspan="2" class="header-revenue">Profit Before Taxes</th>
        <th rowspan="2" class="header-revenue">Tax On Gains</th>
        <th rowspan="2" class="header-revenue">Net Profit</th>
        <th rowspan="2" class="header-revenue">Other</th>
        <th rowspan="2" class="header-revenue">Total Result</th>
        <th rowspan="2" class="header-cashflow">Operating</th>
        <th rowspan="2" class="header-cashflow">Investment</th>
        <th rowspan="2" class="header-cashflow">Financing</th>
        <th rowspan="2" class="header-cashflow">Total</th>
        <th rowspan="2" class="header-cashflow">Cash at Start</th>
        <th rowspan="2" class="header-cashflow">Cash at End</th>
        <th rowspan="2" class="header-cashflow">Capex</th>
        <th rowspan="2" class="header-cashflow">Free Cash Flow</th>
      </tr>
      <tr>
        <th class="header-assets-non">Noncurrent</th>
        <th class="header-assets-cur">Current</th>
        <th class="header-assets-tot">Total</th>
        <th class="header-equity">Total</th>
        <th class="header-lia-non">Noncurrent</th>
        <th class="header-lia-cur">Current</th>
        <th class="header-lia-tot">Total</th>
      <tr>
    </thead>
    <tbody>
      <tr class="row-q1">
        <td rowspan="1" class="year-even">2021</td>
        <td class="quarter-even">Q1</td>
        <td class="number assets-non-q1">194,832,258</td>
        <td class="number assets-cur-q1">186,272,405</td>
        <td class="number assets-tot-q1">381,104,663</td>
        <td class="number equity-q1">236,894,972</td>
        <td class="number liabilities-non-q1">48,634,267</td>
        <td class="number liabilities-cur-q1">95,575,424</td>
        <td class="number liabilities-tot-q1">144,209,691</td>
        <td class="number revenue-q1">50,040,035</td>
        <td class="number revenue-q1">-33,763,880</td>
        <td class="number revenue-q1">16,276,155</td>
        <td class="number revenue-q1">-3,013,653</td>
        <td class="number revenue-q1">13,262,502</td>
        <td class="number revenue-q1">-2,981,054</td>
        <td class="number revenue-q1">10,281,448</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">10,281,448</td>
        <td class="number cashflow-q1">14,821,228</td>
        <td class="number cashflow-q1">-3,265,445</td>
        <td class="number cashflow-q1">-12,072,553</td>
        <td class="number cashflow-q1">-516,770</td>
        <td class="number cashflow-q1">50,788,605</td>
        <td class="number cashflow-q1">50,271,835</td>
        <td class="number cashflow-q1">-3,313,440</td>
        <td class="number cashflow-q1">11,507,788</td>
      </tr>
      <tr class="row-a4">
        <td rowspan="4" class="year-odd">2020</td>
        <td class="quarter-odd">A4</td>
        <td class="number assets-non-a4">192,406,366</td>
        <td class="number assets-cur-a4">189,331,073</td>
        <td class="number assets-tot-a4">381,737,439</td>
        <td class="number equity-a4">222,630,377</td>
        <td class="number liabilities-non-a4">52,883,989</td>
        <td class="number liabilities-cur-a4">106,223,073</td>
        <td class="number liabilities-tot-a4">159,107,062</td>
        <td class="number revenue-a4">205,561,964</td>
        <td class="number revenue-a4">-156,954,914</td>
        <td class="number revenue-a4">48,607,050</td>
        <td class="number revenue-a4">21,453,611</td>
        <td class="number revenue-a4">70,060,661</td>
        <td class="number revenue-a4">-10,830,895</td>
        <td class="number revenue-a4">59,229,766</td>
        <td class="number revenue-a4">-13,022,005</td>
        <td class="number revenue-a4">46,207,761</td>
        <td class="number cashflow-a4">36,309,891</td>
        <td class="number cashflow-a4">18,985,192</td>
        <td class="number cashflow-a4">-17,080,253</td>
        <td class="number cashflow-a4">38,214,830</td>
        <td class="number cashflow-a4">12,573,775</td>
        <td class="number cashflow-a4">50,788,605</td>
        <td class="number cashflow-a4">-21,614,014</td>
        <td class="number cashflow-a4">14,695,877</td>
      </tr>
      <tr class="row-t3">
        <td class="quarter-odd">T3</td>
        <td class="number assets-non-t3">0</td>
        <td class="number assets-cur-t3">0</td>
        <td class="number assets-tot-t3">0</td>
        <td class="number equity-t3">0</td>
        <td class="number liabilities-non-t3">0</td>
        <td class="number liabilities-cur-t3">0</td>
        <td class="number liabilities-tot-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
      </tr>
      <tr class="row-h2">
        <td class="quarter-odd">H2</td>
        <td class="number assets-non-h2">0</td>
        <td class="number assets-cur-h2">0</td>
        <td class="number assets-tot-h2">0</td>
        <td class="number equity-h2">0</td>
        <td class="number liabilities-non-h2">0</td>
        <td class="number liabilities-cur-h2">0</td>
        <td class="number liabilities-tot-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
      </tr>
      <tr class="row-q1">
        <td class="quarter-odd">Q1</td>
        <td class="number assets-non-q1">0</td>
        <td class="number assets-cur-q1">0</td>
        <td class="number assets-tot-q1">0</td>
        <td class="number equity-q1">0</td>
        <td class="number liabilities-non-q1">0</td>
        <td class="number liabilities-cur-q1">0</td>
        <td class="number liabilities-tot-q1">0</td>
        <td class="number revenue-q1">46,359,781</td>
        <td class="number revenue-q1">-32,288,058</td>
        <td class="number revenue-q1">14,071,723</td>
        <td class="number revenue-q1">-4,452,892</td>
        <td class="number revenue-q1">9,618,831</td>
        <td class="number revenue-q1">-1,585,799</td>
        <td class="number revenue-q1">8,033,032</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">8,033,032</td>
        <td class="number cashflow-q1">7,441,993</td>
        <td class="number cashflow-q1">-239,097</td>
        <td class="number cashflow-q1">-5,796,422</td>
        <td class="number cashflow-q1">1,406,473</td>
        <td class="number cashflow-q1">12,573,775</td>
        <td class="number cashflow-q1">13,980,248</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">7,441,993</td>
      </tr>
      <tr class="row-a4">
        <td rowspan="4" class="year-even">2019</td>
        <td class="quarter-even">A4</td>
        <td class="number assets-non-a4">206,455,629</td>
        <td class="number assets-cur-a4">147,723,473</td>
        <td class="number assets-tot-a4">354,179,102</td>
        <td class="number equity-a4">177,269,754</td>
        <td class="number liabilities-non-a4">76,645,369</td>
        <td class="number liabilities-cur-a4">100,263,979</td>
        <td class="number liabilities-tot-a4">176,909,348</td>
        <td class="number revenue-a4">200,134,829</td>
        <td class="number revenue-a4">-145,342,114</td>
        <td class="number revenue-a4">54,792,715</td>
        <td class="number revenue-a4">-5,854,382</td>
        <td class="number revenue-a4">48,938,333</td>
        <td class="number revenue-a4">-8,474,858</td>
        <td class="number revenue-a4">40,463,475</td>
        <td class="number revenue-a4">6,089,567</td>
        <td class="number revenue-a4">46,553,042</td>
        <td class="number cashflow-a4">51,845,919</td>
        <td class="number cashflow-a4">-39,519,596</td>
        <td class="number cashflow-a4">-21,555,792</td>
        <td class="number cashflow-a4">-9,229,466</td>
        <td class="number cashflow-a4">21,803,241</td>
        <td class="number cashflow-a4">12,573,775</td>
        <td class="number cashflow-a4">-43,356,566</td>
        <td class="number cashflow-a4">8,489,353</td>
      </tr>
      <tr class="row-t3">
        <td class="quarter-even">T3</td>
        <td class="number assets-non-t3">0</td>
        <td class="number assets-cur-t3">0</td>
        <td class="number assets-tot-t3">0</td>
        <td class="number equity-t3">0</td>
        <td class="number liabilities-non-t3">0</td>
        <td class="number liabilities-cur-t3">0</td>
        <td class="number liabilities-tot-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
      </tr>
      <tr class="row-h2">
        <td class="quarter-even">H2</td>
        <td class="number assets-non-h2">0</td>
        <td class="number assets-cur-h2">0</td>
        <td class="number assets-tot-h2">0</td>
        <td class="number equity-h2">0</td>
        <td class="number liabilities-non-h2">0</td>
        <td class="number liabilities-cur-h2">0</td>
        <td class="number liabilities-tot-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
      </tr>
      <tr class="row-q1">
        <td class="quarter-even">Q1</td>
        <td class="number assets-non-q1">0</td>
        <td class="number assets-cur-q1">0</td>
        <td class="number assets-tot-q1">0</td>
        <td class="number equity-q1">0</td>
        <td class="number liabilities-non-q1">0</td>
        <td class="number liabilities-cur-q1">0</td>
        <td class="number liabilities-tot-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
      </tr>
      <tr class="row-a4">
        <td rowspan="4" class="year-odd">2018</td>
        <td class="quarter-odd">A4</td>
        <td class="number assets-non-a4">147,043,579</td>
        <td class="number assets-cur-a4">163,390,318</td>
        <td class="number assets-tot-a4">310,433,897</td>
        <td class="number equity-a4">154,121,633</td>
        <td class="number liabilities-non-a4">77,693,595</td>
        <td class="number liabilities-cur-a4">78,618,669</td>
        <td class="number liabilities-tot-a4">156,312,264</td>
        <td class="number revenue-a4">168,378,465</td>
        <td class="number revenue-a4">-121,690,333</td>
        <td class="number revenue-a4">46,688,132</td>
        <td class="number revenue-a4">2,020,010</td>
        <td class="number revenue-a4">48,708,142</td>
        <td class="number revenue-a4">-6,975,212</td>
        <td class="number revenue-a4">41,732,930</td>
        <td class="number revenue-a4">5,081,824</td>
        <td class="number revenue-a4">46,814,754</td>
        <td class="number cashflow-a4">6,197,981</td>
        <td class="number cashflow-a4">-27,651,649</td>
        <td class="number cashflow-a4">21,828,694</td>
        <td class="number cashflow-a4">375,026</td>
        <td class="number cashflow-a4">21,428,215</td>
        <td class="number cashflow-a4">21,803,241</td>
        <td class="number cashflow-a4">-30,135,842</td>
        <td class="number cashflow-a4">-23,937,861</td>
      </tr>
      <tr class="row-t3">
        <td class="quarter-odd">T3</td>
        <td class="number assets-non-t3">0</td>
        <td class="number assets-cur-t3">0</td>
        <td class="number assets-tot-t3">0</td>
        <td class="number equity-t3">0</td>
        <td class="number liabilities-non-t3">0</td>
        <td class="number liabilities-cur-t3">0</td>
        <td class="number liabilities-tot-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
      </tr>
      <tr class="row-h2">
        <td class="quarter-odd">H2</td>
        <td class="number assets-non-h2">0</td>
        <td class="number assets-cur-h2">0</td>
        <td class="number assets-tot-h2">0</td>
        <td class="number equity-h2">0</td>
        <td class="number liabilities-non-h2">0</td>
        <td class="number liabilities-cur-h2">0</td>
        <td class="number liabilities-tot-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
      </tr>
      <tr class="row-q1">
        <td class="quarter-odd">Q1</td>
        <td class="number assets-non-q1">0</td>
        <td class="number assets-cur-q1">0</td>
        <td class="number assets-tot-q1">0</td>
        <td class="number equity-q1">0</td>
        <td class="number liabilities-non-q1">0</td>
        <td class="number liabilities-cur-q1">0</td>
        <td class="number liabilities-tot-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
      </tr>
      <tr class="row-a4">
        <td rowspan="4" class="year-even">2017</td>
        <td class="quarter-even">A4</td>
        <td class="number assets-non-a4">96,220,709</td>
        <td class="number assets-cur-a4">134,472,962</td>
        <td class="number assets-tot-a4">230,693,671</td>
        <td class="number equity-a4">108,435,947</td>
        <td class="number liabilities-non-a4">16,257,931</td>
        <td class="number liabilities-cur-a4">105,999,793</td>
        <td class="number liabilities-tot-a4">122,257,724</td>
        <td class="number revenue-a4">142,756,644</td>
        <td class="number revenue-a4">-106,685,025</td>
        <td class="number revenue-a4">36,071,619</td>
        <td class="number revenue-a4">-1,192,791</td>
        <td class="number revenue-a4">34,878,828</td>
        <td class="number revenue-a4">-5,919,894</td>
        <td class="number revenue-a4">28,958,934</td>
        <td class="number revenue-a4">5,008,717</td>
        <td class="number revenue-a4">33,967,645</td>
        <td class="number cashflow-a4">9,135,606</td>
        <td class="number cashflow-a4">-15,180,822</td>
        <td class="number cashflow-a4">14,205,457</td>
        <td class="number cashflow-a4">8,160,241</td>
        <td class="number cashflow-a4">13,267,974</td>
        <td class="number cashflow-a4">21,428,215</td>
        <td class="number cashflow-a4">-16,890,208</td>
        <td class="number cashflow-a4">-7,754,602</td>
      </tr>
      <tr class="row-t3">
        <td class="quarter-even">T3</td>
        <td class="number assets-non-t3">0</td>
        <td class="number assets-cur-t3">0</td>
        <td class="number assets-tot-t3">0</td>
        <td class="number equity-t3">0</td>
        <td class="number liabilities-non-t3">0</td>
        <td class="number liabilities-cur-t3">0</td>
        <td class="number liabilities-tot-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
      </tr>
      <tr class="row-h2">
        <td class="quarter-even">H2</td>
        <td class="number assets-non-h2">0</td>
        <td class="number assets-cur-h2">0</td>
        <td class="number assets-tot-h2">0</td>
        <td class="number equity-h2">0</td>
        <td class="number liabilities-non-h2">0</td>
        <td class="number liabilities-cur-h2">0</td>
        <td class="number liabilities-tot-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
      </tr>
      <tr class="row-q1">
        <td class="quarter-even">Q1</td>
        <td class="number assets-non-q1">0</td>
        <td class="number assets-cur-q1">0</td>
        <td class="number assets-tot-q1">0</td>
        <td class="number equity-q1">0</td>
        <td class="number liabilities-non-q1">0</td>
        <td class="number liabilities-cur-q1">0</td>
        <td class="number liabilities-tot-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
      </tr>
      <tr class="row-a4">
        <td rowspan="4" class="year-odd">2016</td>
        <td class="quarter-odd">A4</td>
        <td class="number assets-non-a4">76,596,673</td>
        <td class="number assets-cur-a4">90,359,554</td>
        <td class="number assets-tot-a4">166,956,227</td>
        <td class="number equity-a4">76,786,092</td>
        <td class="number liabilities-non-a4">16,212,377</td>
        <td class="number liabilities-cur-a4">73,957,758</td>
        <td class="number liabilities-tot-a4">90,170,135</td>
        <td class="number revenue-a4">107,983,825</td>
        <td class="number revenue-a4">-76,284,154</td>
        <td class="number revenue-a4">31,699,671</td>
        <td class="number revenue-a4">-4,725,831</td>
        <td class="number revenue-a4">26,973,840</td>
        <td class="number revenue-a4">-3,861,453</td>
        <td class="number revenue-a4">23,112,387</td>
        <td class="number revenue-a4">1,966,015</td>
        <td class="number revenue-a4">25,078,402</td>
        <td class="number cashflow-a4">17,572,880</td>
        <td class="number cashflow-a4">-3,326,715</td>
        <td class="number cashflow-a4">-2,444,495</td>
        <td class="number cashflow-a4">11,801,670</td>
        <td class="number cashflow-a4">1,466,304</td>
        <td class="number cashflow-a4">13,267,974</td>
        <td class="number cashflow-a4">-4,118,847</td>
        <td class="number cashflow-a4">13,454,033</td>
      </tr>
      <tr class="row-t3">
        <td class="quarter-odd">T3</td>
        <td class="number assets-non-t3">0</td>
        <td class="number assets-cur-t3">0</td>
        <td class="number assets-tot-t3">0</td>
        <td class="number equity-t3">0</td>
        <td class="number liabilities-non-t3">0</td>
        <td class="number liabilities-cur-t3">0</td>
        <td class="number liabilities-tot-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
      </tr>
      <tr class="row-h2">
        <td class="quarter-odd">H2</td>
        <td class="number assets-non-h2">0</td>
        <td class="number assets-cur-h2">0</td>
        <td class="number assets-tot-h2">0</td>
        <td class="number equity-h2">0</td>
        <td class="number liabilities-non-h2">0</td>
        <td class="number liabilities-cur-h2">0</td>
        <td class="number liabilities-tot-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
      </tr>
      <tr class="row-q1">
        <td class="quarter-odd">Q1</td>
        <td class="number assets-non-q1">0</td>
        <td class="number assets-cur-q1">0</td>
        <td class="number assets-tot-q1">0</td>
        <td class="number equity-q1">0</td>
        <td class="number liabilities-non-q1">0</td>
        <td class="number liabilities-cur-q1">0</td>
        <td class="number liabilities-tot-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
      </tr>
    </tbody>
  </table>
</div>

## Valuation

**Coming soon...**
