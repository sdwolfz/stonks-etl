---
title: "XBSE (Main Market)"
type: invest
---

Main Market.

| Resource           | Link                                                                                                   |
|--------------------|--------------------------------------------------------------------------------------------------------|
| Financial Calendar | [XBSE.ics](/invest/ro/XBSE.ics) |
