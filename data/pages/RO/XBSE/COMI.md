---
title: "CONDMAG S.A. (COMI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xbse/">XBSE</a></td></tr>
    <tr><td>Name</td><td>CONDMAG S.A.</td></tr>
    <tr><td>Symbol</td><td>COMI</td></tr>
    <tr><td>Web</td><td><a href="https://www.condmag.ro">www.condmag.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2004 | 0.018 |
| 2003 | 0.0238 |
| 2001 | 0.0238 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
