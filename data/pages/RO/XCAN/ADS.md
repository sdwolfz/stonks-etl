---
title: "ADIDAS AG  (ADS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>ADIDAS AG </td></tr>
    <tr><td>Symbol</td><td>ADS</td></tr>
    <tr><td>Web</td><td><a href="https://www.adidas-Group.com">www.adidas-Group.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
