---
title: "XCAN (AeRO Secondary Market)"
type: invest
---

The "Alternative" Market.

| Resource           | Link                                                                                                   |
|--------------------|--------------------------------------------------------------------------------------------------------|
| Financial Calendar | [XCAN.ics](/invest/ro/XCAN.ics) |
