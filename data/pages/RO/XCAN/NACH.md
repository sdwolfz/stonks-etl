---
title: "NAPOCHIM SA Cluj (NACH)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>NAPOCHIM SA Cluj</td></tr>
    <tr><td>Symbol</td><td>NACH</td></tr>
    <tr><td>Web</td><td><a href="https://www.napochim.ro">www.napochim.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
