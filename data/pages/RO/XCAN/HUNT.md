---
title: "IHUNT TECHNOLOGY IMPORT-EXPORT S.A. (HUNT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>IHUNT TECHNOLOGY IMPORT-EXPORT S.A.</td></tr>
    <tr><td>Symbol</td><td>HUNT</td></tr>
    <tr><td>Web</td><td><a href="https://www.ihunt.ro">www.ihunt.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 0.02106 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
