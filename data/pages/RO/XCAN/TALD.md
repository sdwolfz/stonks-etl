---
title: "TALC DOLOMITA SA HUNEDOARA (TALD)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>TALC DOLOMITA SA HUNEDOARA</td></tr>
    <tr><td>Symbol</td><td>TALD</td></tr>
    <tr><td>Web</td><td><a href="https://www.talcdolomita.ro">www.talcdolomita.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2003 | 2.7677 |
| 2002 | 0.4295 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
