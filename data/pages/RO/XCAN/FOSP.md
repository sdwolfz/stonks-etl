---
title: "FORAJ SONDE SA ERNEI (FOSP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>FORAJ SONDE SA ERNEI</td></tr>
    <tr><td>Symbol</td><td>FOSP</td></tr>
    <tr><td>Web</td><td><a href="https://www.tmdrill.ro">www.tmdrill.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
