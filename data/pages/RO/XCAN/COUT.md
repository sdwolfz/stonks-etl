---
title: "COMTURIST SA Bucuresti (COUT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>COMTURIST SA Bucuresti</td></tr>
    <tr><td>Symbol</td><td>COUT</td></tr>
    <tr><td>Web</td><td><a href="https://www.comturist.ro">www.comturist.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2001 | 0.139 |
| 2000 | 1.0404 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
