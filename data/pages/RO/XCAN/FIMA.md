---
title: "FIMARO SA Cluj (FIMA)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>FIMARO SA Cluj</td></tr>
    <tr><td>Symbol</td><td>FIMA</td></tr>
    <tr><td>Web</td><td><a href="https://www.fimaro.ro">www.fimaro.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
