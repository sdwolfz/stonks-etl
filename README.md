# Stonks ETL

A stock data scraper for the following stock exchanges:

- Bucharest Stock Exchange
- London Stock Exchange
- New York Stock Exchange
- NASDAQ

## Prerequisites

| Name     | Link                                    |
|----------|-----------------------------------------|
| `bash`   | https://www.gnu.org/software/bash/      |
| `docker` | https://docs.docker.com/engine/install/ |
| `make`   | https://www.gnu.org/software/make/      |

## Development

### Gems

Check outdated gems:

```sh
make outdated
```

Update gems with:

```sh
make gems
```

### Build

```sh
make build
```

### Shell

```sh
make shell
```

#### Jobs

Single:

```sh
time make shell/run ONLY=symbol   GEO=RO
time make shell/run ONLY=metadata GEO=RO
time make shell/run ONLY=dividend GEO=RO
time make shell/run ONLY=calendar GEO=RO
time make shell/run ONLY=price    GEO=RO
```

Multiple:

```sh
time make shell/run ONLY=symbol,metadata GEO=RO,UK,US
```

All:

```sh
make shell/run
```

#### Other

Run one of the commands:

```sh
make console
make generate

time make shell/generate ONLY=data
time make shell/dividends GEO=RO
```

Run one of the scripts:

```sh
make tier
make shell/tier RELEVANCE=2021Q1 GEO=RO

make valuation GEO=RO

ruby ./scripts/valuation/generate.rb SYMBOL
```

### Artifacts

Copy generated pages:

```sh
\cp -f -R data/pages/* ../sdwolfz.gitlab.io/content/invest

\cp -f artifacts/calendar/{XBSE,XCAN}.ics ../sdwolfz.gitlab.io/content/invest/BVB
\cp -f artifacts/calendar/XLON.ics        ../sdwolfz.gitlab.io/content/invest/LSE
```
