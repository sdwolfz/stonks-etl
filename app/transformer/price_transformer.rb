# frozen_string_literal: true

require 'nokogiri'

module Transformer
  class PriceTransformer < BaseTransformer
    def call(exchange, box)
      case exchange
      when 'BVB'
        call_bvb(box)
      when 'LSE'
        call_lse(box)
      when 'NYSE'
        call_nyse(box)
      else
        raise "Unknown exchange: #{exchange}"
      end
    end

    private

    BVB_PRICE_SELECTOR = 'strong.value'

    def call_bvb(box)
      data   = box.input
      result = []

      data.each do |element|
        id = element['id']

        if box.stash.key?(id)
          output = box.stash[id]
        else
          price  = element['value']['price'].to_f
          shares = element['value']['shares'].gsub(',', '').to_i

          output = {
            'id'    => id,
            'value' => {
              'symbol' => id,
              'price'  => price,
              'shares' => shares
            }
          }
          @logger.info("Transformed: #{output}")

          box.stash[id] = output
        end

        result << output
      end

      result
    end

    def call_lse(box)
      data   = box.input
      result = []

      data.each do |element|
        id = element['id']

        if box.stash.key?(id)
          output = box.stash[id]
        else
          price  = element['value']['price'].to_f
          shares = element['value']['shares'].gsub(',', '')

          share_count = shares.to_f
          case shares[-1]
          when 'b'
            share_count *= 1_000_000_000
          when 'm'
            share_count *= 1_000_000
          when 'k'
            share_count *= 1_000
          else
            raise "Unknown share count multiplier: #{shares[-1]}"
          end
          share_count = share_count.to_i

          output = {
            'id'    => id,
            'value' => {
              'symbol' => id,
              'price'  => price,
              'shares' => share_count
            }
          }
          @logger.info("Transformed: #{output}")

          box.stash[id] = output
        end

        result << output
      end

      result
    end

    def call_nyse(box)
      data   = box.input
      result = []

      data.each do |element|
        id = element['id']

        if box.stash.key?(id)
          output = box.stash[id]
        else
          price  = element['value']['price'].to_f
          shares = element['value']['shares'].gsub(',', '')

          share_count = shares.to_f
          case shares[-1]
          when 'B'
            share_count *= 1_000_000_000
          when 'M'
            share_count *= 1_000_000
          else
            raise "Unknown share count multiplier: #{shares[-1]}"
          end
          share_count = share_count.to_i

          output = {
            'id'    => id,
            'value' => {
              'symbol' => id,
              'price'  => price,
              'shares' => share_count
            }
          }
          @logger.info("Transformed: #{output}")

          box.stash[id] = output
        end

        result << output
      end

      result
    end
  end
end
