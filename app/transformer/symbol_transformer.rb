# frozen_string_literal: true

require 'csv'
require 'json'

module Transformer
  class SymbolTransformer < BaseTransformer
    def call(exchange, box)
      case exchange
      when 'BVB'
        call_bvb(box)
      when 'LSE'
        call_lse(box)
      when 'NYSE'
        call_nyse(box)
      else
        raise "Unknown exchange: #{exchange}"
      end
    end

    private

    BVB_SYMBOL_URL = 'https://www.bvb.ro/FinancialInstruments/Details/FinancialInstrumentsDetails.aspx?s=%{symbol}'

    def call_bvb(box)
      data   = box.input
      result = []
      client = HTTPClient.new

      data.each do |element|
        value  = element['value']
        csv    = CSV.parse(value, col_sep: ';')
        result = []

        content = csv[1..]
        content.each do |row|
          id = row[0]

          if box.stash.key?(id)
            output = box.stash[id]
          else
            url       = BVB_SYMBOL_URL % { symbol: row[0] }
            name      = row[1]
            geography = 'RO'

            response = client.get(url)
            raise "Request error, expecting 200, got: #{response.code}" unless response.code == '200'

            html = Nokogiri::HTML.parse(response.body)
            mic  = html.css('table.date tr:nth-child(4) td:nth-child(2)').text.strip

            mic = case mic
                  when 'Main'
                    'XBSE'
                  when 'MTS'
                    'XCAN'
                  else
                    raise "Unknown mic for #{id}: #{mic}"
                  end

            output = {
              'id'    => id,
              'value' => {
                'symbol'    => id,
                'url'       => url,
                'name'      => name,
                'geography' => geography,
                'mic'       => mic
              }
            }
            @logger.debug("Transformed: #{output}")

          end

          result << output
        end
      end

      result
    end

    LSE_ROOT_URL    = 'https://www.londonstockexchange.com'
    ROWS_SELECTOR   = 'table.sp-constituents__table tr'
    SYMBOL_SELECTOR = 'td:nth-of-type(1) a:nth-of-type(1)'
    NAME_SELECTOR   = 'td:nth-of-type(1) a:nth-of-type(2)'

    def call_lse(box)
      data   = box.input
      result = []
      client = HTTPClient.new

      data.each do |element|
        value = element['value']
        html  = Nokogiri::HTML.parse(value)
        rows  = html.css(ROWS_SELECTOR).to_a

        rows[1..].each do |row|
          link = row.css(SYMBOL_SELECTOR)
          id   = link.attribute('data-tidm').value

          if box.stash.key?(id)
            output = box.stash[id]
          else
            url       = LSE_ROOT_URL + "/stock/#{id}/0/company-page"
            name      = row.css(NAME_SELECTOR).text
            name      = name.gsub(/^(.*?)\s\(#{id}\)$/, '\1')
            geography = 'UK'

            response = client.get(url)
            sleep 0.5
            raise "Request error, expecting 301, got: #{response.code}" unless response.code == '301'

            url      = LSE_ROOT_URL + response['location']
            response = client.get(url)
            raise "Request error, expecting 200, got: #{response.code}" unless response.code == '200'

            html  = Nokogiri::HTML.parse(response.body)
            boxes = html.css('app-index-item.index-item').to_a
            raise "Could not find MIC row for #{id}" unless rows.any?

            mic = nil
            boxes.each do |item|
              info = item.css('.more-on-this').text.strip

              if info == 'Market identifier code (MIC)'
                mic = item.css('div').text.strip
                raise "Unknown mic for #{id}: #{mic}" if mic != 'XLON'

                break
              end
            end

            output = {
              'id'    => id,
              'value' => {
                'symbol'    => id,
                'url'       => url,
                'name'      => name,
                'geography' => geography,
                'mic'       => mic
              }
            }
            @logger.debug("Transformed: #{output}")

            sleep 0.5
          end

          result << output
        end
      end

      result
    end

    def call_nyse(box)
      data   = box.input
      result = []

      data.each do |element|
        value = element['value']
        rows  = JSON.parse(value)

        rows.each do |row|
          id = row['symbolTicker']

          if box.stash.key?(id)
            output = box.stash[id]
          else
            next unless row['instrumentType'] == 'COMMON_STOCK'

            url       = row['url']
            name      = row['instrumentName']
            geography = 'US'

            mic = row['micCode']
            mic = 'XNAS' if mic != 'XNYS'

            output = {
              'id'    => id,
              'value' => {
                'symbol'    => id,
                'url'       => url,
                'name'      => name,
                'geography' => geography,
                'mic'       => mic
              }
            }
            @logger.debug("Transformed: #{output}")
          end

          result << output
        end
      end

      result
    end
  end
end
