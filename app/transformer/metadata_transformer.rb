# frozen_string_literal: true

require 'nokogiri'

module Transformer
  class MetadataTransformer < BaseTransformer
    def call(exchange, box)
      case exchange
      when 'BVB'
        call_bvb(box)
      when 'LSE'
        call_lse(box)
      when 'NYSE'
        call_nyse(box)
      else
        raise "Unknown exchange: #{exchange}"
      end
    end

    private

    BVB_SELECTORS = {
      website: '#ctl00_body_ctl02_CompanyProfile_dvIssProfile tbody tr:nth-of-type(4) td:nth-of-type(2)',
      mic: {
        selector: '#ctl00_body_ctl02_TradingHeaderInfo_divInfo tbody tr',
        text:     'Trading venue (MIC)'
      }
    }.freeze

    def call_bvb(box)
      data   = box.input
      result = []

      data.each do |element|
        id = element['id']

        if box.stash.key?(id)
          output = box.stash[id]
        else
          html = element['value']

          @logger.info("Transforming: #{id}")

          profile = html['profile']
          page = Nokogiri::HTML.parse(profile)

          websites = page.css(BVB_SELECTORS[:website]).text
          websites = websites.split(';')
                       .map(&:strip)
                       .map { |url| extract_uri_host(url) }
                       .compact

          market = html['market']
          page   = Nokogiri::HTML.parse(market)
          mic    = ''

          rows = page.css(BVB_SELECTORS[:mic][:selector]).to_a
          rows.each do |row|
            row_text = row.css('td:nth-of-type(1)').text.strip

            if row_text == BVB_SELECTORS[:mic][:text]
              mic = row.css('td:nth-of-type(2)').text
            end
          end

          output = {
            'id'    => id,
            'value' => {
              'websites' => websites,
              'mic'      => mic
            }
          }
          @logger.info("Transformed: #{output}")
        end

        box.stash[id] = output
        result << output
      end

      result
    end

    LSE_SELECTORS = {
      mic_element:     'div',
      website_element: 'a'
    }.freeze

    def call_lse(box)
      data   = box.input
      result = []

      data.each do |element|
        id = element['id']

        if box.stash.key?(id)
          output = box.stash[id]
        else
          html = element['value']

          @logger.info("Transforming: #{id}")

          profile = html['profile']
          page = Nokogiri::HTML.parse(profile)

          websites = page.css(LSE_SELECTORS[:website_element]).first&.attribute('href')&.value
          websites = [extract_uri_host(websites)].compact if websites
          websites ||= []

          market = html['market']
          page   = Nokogiri::HTML.parse(market)

          mic = page.css(LSE_SELECTORS[:mic_element]).first&.text || ''

          output = {
            'id'    => id,
            'value' => {
              'websites' => websites,
              'mic'      => mic
            }
          }
          @logger.info("Transformed: #{output}")
        end

        box.stash[id] = output
        result << output
      end

      result
    end

    NYSE_SELECTORS = {
      mic_rows:  'tbody.summary-data__table-body tr.summary-data__row',
      mic_title: 'td.summary-data__cellheading',
      mic_value: 'td.summary-data__cell',
      website:   '.stock__profile-contact a'
    }.freeze

    def call_nyse(box)
      data   = box.input
      result = []

      data.each do |element|
        id = element['id']

        if box.stash.key?(id)
          output = box.stash[id]
        else
          html = element['value']

          @logger.info("Transforming: #{id}")

          market = html['market']
          exchange = extract_exchange(market)

          mic = nil
          mic = 'XNYS' if exchange == 'NYSE'
          mic = 'XNAS' if exchange&.include?('NASDAQ')
          next unless mic

          profile = html['profile']
          website = extract_websites(profile)
          next unless website

          output = {
            'id'    => id,
            'value' => {
              'websites' => [website],
              'mic'      => mic
            }
          }
          @logger.info("Transformed: #{output}")

          box.stash[id] = output
        end

        result << output
      end

      result
    end

    private def extract_exchange(market)
      page = Nokogiri::HTML.parse(market)
      rows = page.css(NYSE_SELECTORS[:mic_rows]).to_a

      rows.each do |row|
        name  = row.css(NYSE_SELECTORS[:mic_title]).text
        value = row.css(NYSE_SELECTORS[:mic_value]).text

        return value if name == 'Exchange'
      end

      nil
    end

    private def extract_websites(profile)
      page    = Nokogiri::HTML.parse(profile)
      website = page.css(NYSE_SELECTORS[:website])&.to_a&.last&.attribute('href')&.value
      return unless website

      extract_uri_host(website)
    end

    def extract_uri_host(url)
      url.gsub!(%r{^https?://}, '')

      split = URI.split(url)
      if split[0]
        split[2]
      else
        URI.parse("https://#{split[5]}").host
      end
    end
  end
end
