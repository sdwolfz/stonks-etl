# frozen_string_literal: true

module Transformer
  class BaseTransformer
    def initialize(logger)
      @logger = logger
    end

    def call
      raise NotImplementedError
    end
  end
end
