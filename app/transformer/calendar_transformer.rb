# frozen_string_literal: true

require 'nokogiri'

module Transformer
  class CalendarTransformer < BaseTransformer
    def call(exchange, box)
      case exchange
      when 'BVB'
        call_bvb(box)
      when 'LSE'
        call_lse(box)
      end
    end

    private

    BVB_SELECTORS = {
      calendar: 'table#gvCF',
      rows:     'tbody tr td>div',
      date:     'div:nth-of-type(1)',
      name:     'div:nth-of-type(2)'
    }.freeze

    def call_bvb(box)
      data   = box.input
      result = []

      data.each do |element|
        id = element['id']

        if box.stash.key?(id)
          payload = box.stash[id]
        else
          tables = element['value']
          output = []

          @logger.info("Transforming: #{id}")

          tables.each do |table|
            parsed = Nokogiri::HTML.parse(table)

            calendar = parsed.css(BVB_SELECTORS[:calendar])
            rows     = calendar.css(BVB_SELECTORS[:rows])

            rows.each do |row|
              date = row.css(BVB_SELECTORS[:date]).text.strip.split("\n")[0].strip
              name = row.css(BVB_SELECTORS[:name]).text.strip

              data = {
                'date' => date,
                'name' => name
              }
              output << data
            end
          end

          payload = {
            'id'    => id,
            'value' => {
              'symbol'   => id,
              'calendar' => output
            }
          }
          @logger.debug("Transformed: #{payload}")

          box.stash[id] = payload
          result << payload
        end
      end

      result
    end

    LSE_SELECTORS = {
      sections: '.financial-diary__section',
      rows:     'table tr',
      name:     'td:nth-of-type(1)',
      date:     'td:nth-of-type(2)'
    }.freeze

    def call_lse(box)
      data   = box.input
      result = []

      data.each do |element|
        id = element['id']

        if box.stash.key?(id)
          payload = box.stash[id]
        else
          html   = element['value']
          output = []

          @logger.info("Transforming: #{id}")

          page     = Nokogiri::HTML.parse(html)
          sections = page.css(LSE_SELECTORS[:sections]).to_a
          sections.each do |section|
            rows = section.css(LSE_SELECTORS[:rows])

            rows.each do |row|
              name = row.css(LSE_SELECTORS[:name]).text.strip
              date = row.css(LSE_SELECTORS[:date]).text.strip

              next if date == 'n/a'

              data = {
                'date' => date,
                'name' => name
              }
              output << data
            end
          end

          payload = {
            'id'    => id,
            'value' => {
              'symbol'   => id,
              'calendar' => output
            }
          }

          @logger.debug("Transformed: #{payload}")

          box.stash[id] = payload
          result << payload
        end
      end

      result
    end
  end
end
