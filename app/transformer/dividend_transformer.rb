# frozen_string_literal: true

require 'nokogiri'
require 'date'

module Transformer
  class DividendTransformer < BaseTransformer
    def call(exchange, box)
      case exchange
      when 'BVB'
        call_bvb(box)
      when 'LSE'
        call_lse(box)
      when 'NYSE'
        call_nyse(box)
      else
        raise "Unknown exchange: #{exchange}"
      end
    end

    private

    BVB_SELECTORS = {
      dividends: 'table#gvDividende tbody tr'
    }.freeze

    def call_bvb(box)
      data   = box.input
      result = []

      data.each do |element|
        id = element['id']

        if box.stash.key?(id)
          payload = box.stash[id]
        else
          html   = element['value']
          output = []

          @logger.info("Transforming: #{id}")

          page = Nokogiri::HTML.parse(html)
          dividends = page.css(BVB_SELECTORS[:dividends]).to_a
          dividends.each do |dividend|
            year = dividend.css('td:nth-of-type(1)').text
            next if year == 'No data.'

            value = dividend.css('td:nth-of-type(2)').text
            value.gsub!(',', '')
            value = 0 if value == 'N/A'

            output << {
              'year'  => Integer(year),
              'value' => Float(value)
            }
          end

          payload = {
            'id'    => id,
            'value' => {
              'symbol'    => id,
              'dividends' => output
            }
          }
          @logger.info("Transformed: #{payload}")
        end

        box.stash[id] = payload
        result << payload
      end

      result
    end

    LSE_SELECTORS = {
      dividends_table: 'table#income-statement-accordion',
      dates_rows:      'thead tr',
      data_rows: {
        selector: 'tbody tr',
        text:     "Dividend per share"
      }
    }.freeze

    def call_lse(box)
      data   = box.input
      result = []

      data.each do |element|
        id = element['id']

        if box.stash.key?(id)
          payload = box.stash[id]
        else
          html   = element['value']
          output = []

          @logger.info("Transforming: #{id}")

          if html != ''
            page  = Nokogiri::HTML.parse(html)
            table = page.css(LSE_SELECTORS[:dividends_table])

            years     = []
            dates_row = table.css(LSE_SELECTORS[:dates_rows]).to_a.first
            ths       = dates_row.css('th').to_a

            ths[1..-2].each do |td|
              year = td.text.strip.split(' ')[0].split('.')[2].to_i + 2000

              years << year
            end

            dividends = []

            data_rows = table.css(LSE_SELECTORS[:data_rows][:selector]).to_a
            data_rows.each do |row|
              tds = row.css('td').to_a
              text = tds[0].text.strip

              next unless text == LSE_SELECTORS[:data_rows][:text]

              tds[1..-2].each do |td|
                match = td.text.strip.match(/\d+\.\d+/)
                value = match ? match[0] : 0

                dividends << Float(value)
              end
            end

            years.zip(dividends) do |year, dividend|
              output << {
                'year'  => year,
                'value' => dividend
              }
            end
          end

          payload = {
            'id'    => id,
            'value' => {
              'symbol'    => id,
              'dividends' => output
            }
          }
          @logger.info("Transformed: #{payload}")
        end

        box.stash[id] = payload
        result << payload
      end

      result
    end

    NYSE_SELECTORS = {
      table:   'table.dividend-history__table',
      rows:    'tbody.dividend-history__table-body tr.dividend-history__row',
      columns: '.dividend-history__cell'
    }.freeze

    def call_nyse(box)
      data   = box.input
      result = []

      data.each do |element|
        id = element['id']

        if box.stash.key?(id)
          payload = box.stash[id]
        else
          html   = element['value']
          output = []

          @logger.info("Transforming: #{id}")

          page  = Nokogiri::HTML.parse(html)
          table = page.css(NYSE_SELECTORS[:table])
          rows  = table.css(NYSE_SELECTORS[:rows]).to_a

          dividends = {}

          rows.each do |row|
            columns = row.css(NYSE_SELECTORS[:columns]).to_a

            date = columns[0].text.strip
            cash = columns[2].text.strip

            next if date == '' || cash == ''

            year  = Date.strptime(date, '%m/%d/%Y').year
            value = Float(cash.gsub('$', ''))

            dividends[year] = dividends[year].nil? ? value : dividends[year] + value
          end

          payload = {
            'id'    => id,
            'value' => {
              'symbol'    => id,
              'dividends' => dividends.to_a.map { |year, value| { 'year' => year, 'value' => value.round(3) } }
            }
          }
          @logger.info("Transformed: #{payload}")
        end

        box.stash[id] = payload
        result << payload
      end

      result
    end
  end
end
