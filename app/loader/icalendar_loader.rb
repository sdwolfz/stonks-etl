# frozen_string_literal: true

require 'date'
require 'fileutils'
require 'json'

require 'icalendar'

module Loader
  class IcalendarLoader < BaseLoader
    def call(path, data)
      path = File.join(Dir.pwd, path)
      dir  = File.dirname(path)
      FileUtils.mkdir_p(dir)
      FileUtils.touch(path)

      @logger.info("Loading: #{path}")

      calendar = ::Icalendar::Calendar.new
      today    = Date.today

      data.each do |element|
        symbol = element['symbol']
        events = element['calendar']

        events.each do |event|
          date = ::Date.parse(event['date'])
          next if date < today

          name = event['name']

          calendar.event do |e|
            e.dtstart  = ::Icalendar::Values::Date.new(date)
            e.dtend    = ::Icalendar::Values::Date.new(date)
            e.summary  = "#{symbol}: #{name}"
            e.ip_class = 'PRIVATE'
          end
        end
      end

      output = calendar.to_ical
      File.write(path, output)

      data
    end
  end
end
