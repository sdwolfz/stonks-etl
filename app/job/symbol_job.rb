# frozen_string_literal: true

module Job
  class SymbolJob
    def initialize(logger)
      @logger = logger
    end

    def call(geo)
      run_bvb_chain  if geo.include?('RO') || geo.empty?
      run_lse_chain  if geo.include?('UK') || geo.empty?
      run_nyse_chain if geo.include?('US') || geo.empty?
    end

    private

    def run_bvb_chain
      chain = CacheBox::Chain.new('symbol/BVB')

      chain.add(:extractor) do |box|
        output = 'schemas/extractor/text-extractor-schema.json'
        schema = SchemaValidator.new(@logger, output: output)

        result = Extractor::SymbolExtractor.new(@logger).call('BVB', box)
        schema.validate_output!(result)

        result
      end
      chain.add(:transformer) do |box|
        input  = 'schemas/extractor/text-extractor-schema.json'
        output = 'schemas/transformer/symbol-transformer-schema.json'
        schema = SchemaValidator.new(@logger, input: input, output: output)

        schema.validate_input!(box.input)
        result = Transformer::SymbolTransformer.new(@logger).call('BVB', box)
        schema.validate_output!(result)

        result
      end
      chain.add(:loader) do |box|
        input  = 'schemas/transformer/symbol-transformer-schema.json'
        schema = SchemaValidator.new(@logger, input: input)

        schema.validate_input!(box.input)
        path = 'artifacts/symbol/RO.json'
        Loader::JSONLoader.new(@logger).call(path, box.input)

        nil
      end

      chain.run!
    end

    def run_lse_chain
      chain = CacheBox::Chain.new('symbol/LSE')

      chain.add(:extractor) do |box|
        output = 'schemas/extractor/text-extractor-schema.json'
        schema = SchemaValidator.new(@logger, output: output)

        result = Extractor::SymbolExtractor.new(@logger).call('LSE', box)
        schema.validate_output!(result)

        result
      end
      chain.add(:transformer) do |box|
        input  = 'schemas/extractor/text-extractor-schema.json'
        output = 'schemas/transformer/symbol-transformer-schema.json'
        schema = SchemaValidator.new(@logger, input: input, output: output)

        schema.validate_input!(box.input)
        result = Transformer::SymbolTransformer.new(@logger).call('LSE', box)
        schema.validate_output!(result)

        result
      end
      chain.add(:loader) do |box|
        input  = 'schemas/transformer/symbol-transformer-schema.json'
        schema = SchemaValidator.new(@logger, input: input)

        schema.validate_input!(box.input)
        path = 'artifacts/symbol/UK.json'
        Loader::JSONLoader.new(@logger).call(path, box.input)

        nil
      end

      chain.run!
    end

    def run_nyse_chain
      chain = CacheBox::Chain.new('symbol/NYSE')

      chain.add(:extractor) do |box|
        output = 'schemas/extractor/text-extractor-schema.json'
        schema = SchemaValidator.new(@logger, output: output)

        result = Extractor::SymbolExtractor.new(@logger).call('NYSE', box)
        schema.validate_output!(result)

        result
      end
      chain.add(:transformer) do |box|
        input  = 'schemas/extractor/text-extractor-schema.json'
        output = 'schemas/transformer/symbol-transformer-schema.json'
        schema = SchemaValidator.new(@logger, input: input, output: output)

        schema.validate_input!(box.input)
        result = Transformer::SymbolTransformer.new(@logger).call('NYSE', box)
        schema.validate_output!(result)

        result
      end
      chain.add(:loader) do |box|
        input  = 'schemas/transformer/symbol-transformer-schema.json'
        schema = SchemaValidator.new(@logger, input: input)

        schema.validate_input!(box.input)
        path = 'artifacts/symbol/US.json'
        Loader::JSONLoader.new(@logger).call(path, box.input)

        nil
      end

      chain.run!
    end
  end
end
