# frozen_string_literal: true

require 'erb'
require 'fileutils'

module Generator
  class Renderer
    def initialize(data:, metadata:, template:, reports: nil)
      @data     = data
      @metadata = metadata
      @template = template
      @reports  = reports

      @renderer = ERB.new(@template, nil, '-')
    end

    def call(path)
      puts "Rendering: #{path}"
      result = @renderer.result(binding)

      FileUtils.mkdir_p(File.dirname(path))
      File.write(path, result)
    end

    private

    def format(number)
      sign = number.negative? ? '-' : ''
      sign + number.abs.to_s.gsub(/\B(?=(...)*\b)/, ',')
    end
  end
end
