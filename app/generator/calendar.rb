# frozen_string_literal: true

require_relative '../loader'

module Generator
  class Calendar
    def call
      generate_data_files('RO')
      generate_data_files('UK')
    end

    private

    def generate_data_files(id)
      artifacts = [
        ['calendar', "#{id}.json"],
        ['metadata', "#{id}.json"],
        ['symbol',   "#{id}.json"]
      ]
      registry = DataJoiner.new.call(artifacts)

      registry.group_by { |e| e[1]['mic'] }.each_pair do |mic, data|
        path = "artifacts/calendar/ICS/#{mic}.ics"
        data = data.map { |e| e[1] }

        Loader::IcalendarLoader.new(nil).call(path, data)
      end
    end
  end
end
