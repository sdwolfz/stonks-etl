# frozen_string_literal: true

module Generator
  class Page
    METADATA = {
      'geographies' => [
        'RO',
        'UK',
        'US'
      ],
      'content' => {
        'RO' => {
          'name'        => 'Bucharest Stock Exchange',
          'symbol'      => 'RO',
          'location'    => '/invest/ro/',
          'description' => 'The stock exchange of Romania.',
          'markets'     => ['XBSE', 'XCAN'],
          'content'     => {
            'XBSE' => {
              'geography'          => 'RO',
              'geography_location' => '/invest/ro/',
              'name'               => 'XBSE (Main Market)',
              'symbol'             => 'XBSE',
              'location'           => '/invest/ro/xbse/',
              'description'        => 'Main Market.',
            },
            'XCAN' => {
              'geography'          => 'RO',
              'geography_location' => '/invest/ro/',
              'name'               => 'XCAN (AeRO Secondary Market)',
              'symbol'             => 'XCAN',
              'location'           => '/invest/ro/xcan/',
              'description'        => 'The "Alternative" Market.',
            }
          }
        },
        'UK' => {
          'name'        => 'London Stock Exchange',
          'symbol'      => 'UK',
          'location'    => '/invest/uk/',
          'description' => 'The stock exchange of the United Kingdom.',
          'markets'     => ['XLON'],
          'content'     => {
            'XLON' => {
              'geography'          => 'UK',
              'geography_location' => '/invest/uk/',
              'name'               => 'XLON (Main Market)',
              'symbol'             => 'XLON',
              'location'           => '/invest/uk/xlon/',
              'description'        => 'Main Market.'
            },
          }
        },
        'US' => {
          'name'        => 'American Stock Exchanges',
          'symbol'      => 'US',
          'location'    => '/invest/us/',
          'description' => 'Stock exchanges in USA.',
          'markets'     => ['XNYS', 'XNAS'],
          'content'     => {
            'XNYS' => {
              'geography'          => 'US',
              'geography_location' => '/invest/us/',
              'name'               => 'NYSE (New York Stock Exchange)',
              'symbol'             => 'XNYS',
              'location'           => '/invest/us/xnys/',
              'description'        => 'New York Stock Exchange.',
            },
            'XNAS' => {
              'geography'          => 'US',
              'geography_location' => '/invest/us/',
              'name'               => 'NASDAQ (you know... NASDAQ)',
              'symbol'             => 'XNAS',
              'location'           => '/invest/us/xnas/',
              'description'        => 'NASDAQ (you know... NASDAQ).',
            }
          }
        },
      }
    }.freeze

    def call
      data     = METADATA['geographies']
      metadata = METADATA['content']

      generate_root_page(data, metadata)
      generate_geography_pages(data, metadata)
    end

    private

    def generate_root_page(data, metadata)
      renderer = Renderer.new(
        data:     data,
        metadata: metadata,
        template: File.read(File.join('app', 'generator', 'template', 'root.md.erb'))
      )

      renderer.call(File.join('data', 'pages', '_index.md'))
    end

    def generate_geography_pages(data, metadata)
      data.each do |geography|
        artifacts = [
          ['dividend', "#{geography}.json"],
          ['metadata', "#{geography}.json"],
          ['price',    "#{geography}.json"],
          ['symbol',   "#{geography}.json"]
        ]
        datalake = DataJoiner.new.call(artifacts)

        generate_geography_page(datalake, metadata[geography])
      end
    end

    def generate_geography_page(data, metadata)
      symbol = metadata['symbol']

      renderer = Renderer.new(
        data:     data,
        metadata: metadata,
        template: File.read(File.join('app', 'generator', 'template', 'geography.md.erb'))
      )
      renderer.call(File.join('data', 'pages', "#{symbol}.md"))

      renderer = Renderer.new(
        data:     data,
        metadata: metadata,
        template: File.read(File.join('app', 'generator', 'template', 'geography_index.md.erb'))
      )
      renderer.call(File.join('data', 'pages', symbol, '_index.md'))

      generate_market_pages(data, metadata)
    end

    def generate_market_pages(data, metadata)
      markets = metadata['markets']
      markets.each do |market|
        generate_market_page(data, metadata['content'][market])
      end
    end

    def generate_market_page(data, metadata)
      geography = metadata['geography']
      symbol    = metadata['symbol']

      renderer = Renderer.new(
        data:     data,
        metadata: metadata,
        template: File.read(File.join('app', 'generator', 'template', 'segment.md.erb'))
      )
      renderer.call(File.join('data', 'pages', geography, "#{symbol}.md"))

      renderer = Renderer.new(
        data:     data,
        metadata: metadata,
        template: File.read(File.join('app', 'generator', 'template', 'segment_index.md.erb'))
      )
      renderer.call(File.join('data', 'pages', geography, symbol, '_index.md'))

      generate_share_pages(data, metadata)
    end

    def generate_share_pages(data, metadata)
      data.to_a.sort_by { |e| e[0] }.each do |symbol, element|
        geography = metadata['geography']
        market    = metadata['symbol']

        next unless market == element['mic']

        @reports ||= ReportDataJoiner.new.call
        renderer = Renderer.new(
          data:     element,
          reports:  @reports,
          metadata: metadata,
          template: File.read(File.join('app', 'generator', 'template', 'stock.md.erb'))
        )

        renderer.call(File.join('data', 'pages', geography, market, "#{symbol}.md"))
      end
    end
  end
end
