# frozen_string_literal: true

module Loader
end

require_relative './loader/base_loader'

require_relative './loader/json_loader'
require_relative './loader/icalendar_loader'
