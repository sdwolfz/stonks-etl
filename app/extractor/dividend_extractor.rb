# frozen_string_literal: true

module Extractor
  class DividendExtractor < BaseExtractor
    def initialize(logger, driver)
      super(logger)

      @driver = driver
    end

    def call(exchange, box)
      case exchange
      when 'BVB'
        call_bvb(box)
      when 'LSE'
        call_lse(box)
      when 'NYSE'
        call_nyse(box)
      else
        raise "Unknown exchange: #{exchange}"
      end
    end

    private

    BVB_SELECTORS = {
      button: "input[value='Issuer profile']",
      table:  'table#gvDividende'
    }.freeze

    def call_bvb(box)
      data   = box.input
      result = []

      data.each do |element|
        value = element['value']
        id    = value['symbol']

        if box.stash.key?(id)
          result << box.stash[id]
        else
          url = value['url']
          @logger.info("Extracting: #{url}")

          extracted = @driver.with_retries(default: '') do
            @driver.navigate.to(url)
            sleep 1

            profile_button = @driver.find_element(:css, BVB_SELECTORS[:button])
            @driver.scroll_to_view(profile_button)
            profile_button.click
            sleep 2

            @driver.find_element(:css, BVB_SELECTORS[:table]).attribute('outerHTML')
          end

          output = {
            'id'    => id,
            'value' => extracted
          }

          box.stash[id] = output
          result << output
        end
      end

      result
    end

    LSE_SELECTORS = {
      button:  "#tab-nav li a",
      cookies: '#ccc-notify-accept',
      table:   'table#income-statement-accordion'
    }.freeze

    def call_lse(box)
      data   = box.input
      result = []

      data.each do |element|
        value = element['value']
        id    = value['symbol']

        if box.stash.key?(id)
          result << box.stash[id]
        else
          url = value['url']
          @logger.info("Extracting: #{url}")

          extracted = @driver.with_retries(default: '') do
            @driver.navigate.to(url)
            sleep 1

            begin
              cookie_button = @driver.find_element(:css, LSE_SELECTORS[:cookies])
              cookie_button.click
            rescue Selenium::WebDriver::Error::NoSuchElementError
              # Nothing to do
            end

            buttons = @driver.find_elements(:css, LSE_SELECTORS[:button])
            button = buttons.find { |e| e.text == 'Fundamentals' }

            if button
              @driver.scroll_to_view(button)
              button.click
              sleep 2

              table = @driver.find_element(:css, LSE_SELECTORS[:table])
              table.attribute('outerHTML')
            else
              ''
            end
          rescue Selenium::WebDriver::Error::NoSuchElementError
            ''
          end

          output = {
            'id'    => id,
            'value' => extracted
          }

          box.stash[id] = output
          result << output
        end
      end

      result
    end

    NYSE_SELECTORS = {
      table: 'table.dividend-history__table'
    }.freeze

    def call_nyse(box)
      data   = box.input
      result = []

      data.each do |element|
        value = element['value']
        id    = value['symbol']

        if box.stash.key?(id)
          result << box.stash[id]
        else
          url = "https://www.nasdaq.com/market-activity/stocks/#{id}/dividend-history"
          @logger.info("Extracting: #{url}")

          output = @driver.with_retries(default: '') do
            @driver.navigate.to(url)
            sleep 1

            table = @driver.find_element(:css, NYSE_SELECTORS[:table])
            table.attribute('outerHTML')
          end

          payload = {
            'id'    => id,
            'value' => output
          }
          box.stash[id] = payload
          result << payload
        end
      end

      result
    end
  end
end
