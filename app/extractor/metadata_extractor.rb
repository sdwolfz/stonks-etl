# frozen_string_literal: true

module Extractor
  class MetadataExtractor < BaseExtractor
    def initialize(logger, driver)
      super(logger)

      @driver = driver
    end

    def call(exchange, box)
      case exchange
      when 'BVB'
        call_bvb(box)
      when 'LSE'
        call_lse(box)
      when 'NYSE'
        call_nyse(box)
      else
        raise "Unknown exchange: #{exchange}"
      end
    end

    private

    BVB_SELECTORS = {
      profile_button:  "input[value='Issuer profile']",
      company_profile: '#ctl00_body_ctl02_CompanyProfile_dvIssProfile',

      trading_button: "input[value='Trading']",
      trading_table:  '#ctl00_body_ctl02_TradingHeaderInfo_divInfo',
    }

    def call_bvb(box)
      data   = box.input
      result = []

      data.each do |element|
        value = element['value']
        id    = value['symbol']

        if box.stash.key?(id)
          result << box.stash[id]
        else
          url = value['url']
          @logger.info("Extracting: #{url}")

          profile = @driver.with_retries(default: '') do
            @driver.navigate.to(url)
            sleep 1

            profile_button = @driver.find_element(:css, BVB_SELECTORS[:profile_button])
            profile_button.click
            sleep 2

            @driver.find_element(:css, BVB_SELECTORS[:company_profile]).attribute('outerHTML')
          end

          market = @driver.with_retries(default: '') do
            @driver.navigate.to(url)
            sleep 1

            trading_button = @driver.find_element(:css, BVB_SELECTORS[:trading_button])
            trading_button.click
            sleep 2

            @driver.find_element(:css, BVB_SELECTORS[:trading_table]).attribute('outerHTML')
          end

          profile = '' if profile.nil?
          market  = '' if market.nil?

          output = {
            'id'    => id,
            'value' => {
              'profile' => profile,
              'market'  => market
            }
          }

          box.stash[id] = output
          result << output
        end
      end

      result
    end

    LSE_SELECTORS = {
      mic_element:     '.chart-table-instrument-information .index-item:nth-of-type(7) div',
      website_element: '.ccc-website div a'
    }

    def call_lse(box)
      data   = box.input
      result = []

      data.each do |element|
        value = element['value']
        id    = value['symbol']

        if box.stash.key?(id)
          result << box.stash[id]
        else
          url = value['url']

          market = @driver.with_retries(default: '') do
            @logger.info("Extracting: #{url}")
            @driver.navigate.to(url)
            sleep 1

            @driver.find_element(:css, LSE_SELECTORS[:mic_element]).attribute('outerHTML')
          end

          profile = @driver.with_retries(default: '') do
            url = url.gsub(/company-page$/, 'our-story')
            @logger.info("Extracting: #{url}")
            @driver.navigate.to(url)
            sleep 1

            @driver.find_element(:css, LSE_SELECTORS[:website_element]).attribute('outerHTML')
          end

          market  = '' if market.nil?
          profile = '' if profile.nil?

          output = {
            'id'    => id,
            'value' => {
              'profile' => profile,
              'market'  => market
            }
          }

          box.stash[id] = output
          result << output
        end
      end

      result
    end

    NYSE_SELECTORS = {
      mic_element:     'div.summary-data',
      mic_rows:        'tbody.summary-data__table-body tr.summary-data__row',
      mic_title:       'td.summary-data__cellheading',
      mic_value:       'td.summary-data__cell',
      website_element: 'div.stock__profile'
    }.freeze

    def call_nyse(box)
      data   = box.input
      result = []

      data.each do |element|
        value = element['value']
        id    = value['symbol']

        if box.stash.key?(id)
          result << box.stash[id]
        else
          @logger.info("Extracting metadata for: #{id}")

          # --------------------------------------------------------------------
          # MIC

          url = "https://www.nasdaq.com/market-activity/stocks/#{id}"
          @logger.info("Extracting: #{url}")

          market = @driver.with_retries do
            @driver.navigate.to(url)
            sleep 1

            if @driver.current_url.include?("https://www.nasdaq.com/market-activity/stocks/")
              item = @driver.find_element(:css, NYSE_SELECTORS[:mic_element])
              @driver.scroll_to_view(item)

              sleep 2

              item.attribute('outerHTML')
            else
              ''
            end
          end
          market = '' if market.nil?

          profile = if market == ''
            ''
          else
            exchange = extract_exchange(market)

            mic = nil
            mic = 'XNYS' if exchange == 'NYSE'
            mic = 'XNAS' if exchange&.include?('NASDAQ')

            if mic
              # --------------------------------------------------------------------
              # Website

              url = "https://www.morningstar.com/stocks/#{mic}/#{id}/quote"
              @logger.info("Extracting: #{url}")

              output = @driver.with_retries do
                @driver.navigate.to(url)
                sleep 1

                item = @driver.find_element(:css, NYSE_SELECTORS[:website_element])
                @driver.scroll_to_view(item)

                sleep 1
                item.attribute('outerHTML')
              end

              output = '' if output.nil?
              output
            else
              ''
            end
          end

          output = {
            'id'    => id,
            'value' => {
              'profile' => profile,
              'market'  => market
            }
          }

          box.stash[id] = output
          result << output
        end
      end

      result
    end

    # --------------------------------------------------------------------------
    # Private
    # --------------------------------------------------------------------------

    private

    def extract_exchange(market)
      page = Nokogiri::HTML.parse(market)
      rows = page.css(NYSE_SELECTORS[:mic_rows]).to_a

      rows.each do |row|
        name  = row.css(NYSE_SELECTORS[:mic_title]).text
        value = row.css(NYSE_SELECTORS[:mic_value]).text

        return value if name == 'Exchange'
      end

      nil
    end
  end
end
