# frozen_string_literal: true

require 'yaml'

module Extractor
  class YAMLExtractor < BaseExtractor
    # Input:
    #
    # path = String # Path
    #
    # Output:
    #
    # result = YAML
    def call(path)
      @logger.info("Reading: #{path}")

      content = File.read(path)
      YAML.safe_load(content)
    end
  end
end
