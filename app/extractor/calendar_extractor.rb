# frozen_string_literal: true

module Extractor
  class CalendarExtractor < BaseExtractor
    def initialize(logger, driver)
      super(logger)

      @driver = driver
    end

    def call(exchange, box)
      case exchange
      when 'BVB'
        call_bvb(box)
      when 'LSE'
        call_lse(box)
      end
    end

    private

    BVB_SELECTORS = {
      button:   "input[value='Financials']",
      calendar: 'table#gvCF',
      next:     '#gvCF_next'
    }.freeze

    def call_bvb(box)
      data   = box.input
      result = []

      data.each do |element|
        value = element['value']
        id    = value['symbol']

        if box.stash.key?(id)
          result << box.stash[id]
        else
          url    = value['url']
          output = []

          @logger.info("Extracting: #{url}")

          @driver.with_retries do
            @driver.navigate.to(url)
            sleep 1

            financials_button = @driver.find_element(:css, BVB_SELECTORS[:button])
            financials_button.click
            sleep 2

            counter = 0
            limit   = 100
            loop do
              calendar = @driver.find_element(:css, BVB_SELECTORS[:calendar])
              output << calendar.attribute('outerHTML')

              next_button = @driver.find_element(:css, BVB_SELECTORS[:next])
              @driver.scroll_to_view(next_button)

              next_dissabled = next_button.attribute(:class).split(' ').include?('disabled')
              break if next_dissabled

              next_button.click
              raise "Counter exceeded loop limit of #{limit}" if counter > limit

              counter += 1
            end
          end

          payload = {
            'id'    => id,
            'value' => output
          }

          box.stash[id] = payload
          result << payload
        end
      end

      result
    end

    YEAR         = '2021'
    URL_TEMPLATE = 'https://www.lse.co.uk/FinanceDiary.asp?year=%{year}&shareprice=%{symbol}'
    LSE_SELECTOR = '.financial-diary__section'

    def call_lse(box)
      data   = box.input
      result = []
      client = HTTPClient.new

      data.each do |element|
        value = element['value']
        id    = value['symbol']

        if box.stash.key?(id)
          result << box.stash[id]
        else
          url = URL_TEMPLATE % { year: YEAR, symbol: id }

          @logger.info("Extracting: #{url}")

          response = client.get(url)
          if response.code == '404'
            @logger.debug("Calendar for #{id} #{YEAR} not found!")

            content = ''
          else
            raise "Request error: #{response.code}" unless response.code == '200'

            page    = Nokogiri::HTML.parse(response.body)
            content = page.css(LSE_SELECTOR).to_s
          end

          payload = {
            'id'    => id,
            'value' => content
          }

          box.stash[id] = payload
          result << payload

          sleep 0.5
        end
      end

      result
    end
  end
end
