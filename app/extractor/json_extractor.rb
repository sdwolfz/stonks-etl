# frozen_string_literal: true

require 'json'

module Extractor
  class JSONExtractor < BaseExtractor
    # Input:
    #
    # path = String # Path
    #
    # Output:
    #
    # result = JSON
    def call(path)
      @logger.info("Reading: #{path}")

      content = File.read(path)
      JSON.parse(content)
    end
  end
end
