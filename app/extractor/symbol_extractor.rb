# frozen_string_literal: true

require 'nokogiri'

module Extractor
  class SymbolExtractor < BaseExtractor
    def call(exchange, box)
      case exchange
      when 'BVB'
        call_bvb(box)
      when 'LSE'
        call_lse(box)
      when 'NYSE'
        call_nyse(box)
      else
        raise "Unknown exchange: #{exchange}"
      end
    end

    private

    def call_bvb(box)
      result = []
      id     = 'BVB-csv'
      url    = 'https://www.bvb.ro/FinancialInstruments/Markets/SharesListForDownload.ashx'
      client = HTTPClient.new

      if box.stash.key?(id)
        result << box.stash[id]
      else
        @logger.info("Extracting: #{url}")

        response = client.get(url)
        raise "Request error: #{response.code}" unless response.code == '200'

        content = response.body
        item    = {
          'id'    => id,
          'value' => content
        }

        box.stash[id] = item
        result << item
      end

      result
    end

    def call_lse(box)
      result = []
      id     = 'LSE-page'
      url    = 'https://www.lse.co.uk/share-prices/indices/ftse-all-share/constituents.html'
      client = HTTPClient.new

      if box.stash.key?(id)
        result << box.stash[id]
      else
        @logger.info("Extracting: #{url}")

        response = client.get(url)
        raise "Request error: #{response.code}" unless response.code == '200'

        page    = Nokogiri::HTML.parse(response.body)
        content = page.css('table.sp-constituents__table').to_s

        output  = {
          'id'    => id,
          'value' => content
        }
        box.stash[id] = output

        result << output
      end

      result
    end

    def call_nyse(box)
      result = []
      client = HTTPClient.new(headers: { 'Content-Type': 'application/json' })
      body   = {
        'instrumentType'    => 'EQUITY',
        'pageNumber'        => 1,
        'sortColumn'        => 'NORMALIZED_TICKER',
        'sortOrder'         => 'ASC',
        'maxResultsPerPage' => 50_000,
        'filterToken'       => ''
      }.to_json

      id  = 'NYSE-data'
      url = 'https://www.nyse.com/api/quotes/filter'

      if box.stash.key?(id)
        result << box.stash[id]
      else
        @logger.info("Extracting: #{url}")

        response = client.post(url, body: body)
        raise "Request error: #{response.code}" unless response.code == '200'

        content = response.body
        item    = {
          'id'    => id,
          'value' => content
        }
        box.stash[id] = item
        result << item
      end

      result
    end
  end
end
