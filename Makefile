#-------------------------------------------------------------------------------
# Settings
#-------------------------------------------------------------------------------

.DEFAULT_GOAL := help

HERE := $(realpath $(dir $(realpath $(firstword $(MAKEFILE_LIST)))))
DOCKER_HERE := docker run --rm -it -u `id -u`:`id -g` -v $(HERE):/work -w /work

REPO_IMAGE := stonks-etl
RUBY_IMAGE := ruby:3.0.1-alpine

#-------------------------------------------------------------------------------
# Goals
#-------------------------------------------------------------------------------

.PHONY: help
help:
	@echo 'Usage: make [<GOAL_1>, ...] [<VARIABLE_1>=value_1, ...]'
	@echo ''
	@echo 'Examples:'
	@echo '  make'
	@echo '  make help'
	@echo ''
	@echo 'Goals:'
	@echo '  - help: Displays this help message.'
	@echo ''

#-------------------------------------------------------------------------------
# Tmux

.PHONY: tmux/symbol
tmux/symbol:
	@sh scripts/tmux/symbol.sh

.PHONY: tmux/other
tmux/other:
	@sh scripts/tmux/other.sh

#-------------------------------------------------------------------------------
# Docker

.PHONY: outdated
outdated:
	@$(DOCKER_HERE) $(RUBY_IMAGE) bundle outdated

.PHONY: gems
gems:
	@$(DOCKER_HERE) $(RUBY_IMAGE) bundle lock --update

.PHONY: build
build:
	@docker build --pull -t $(REPO_IMAGE) $(HERE)

.PHONY: shell
shell:
	@$(DOCKER_HERE) --memory=8g --memory-swap=8g --shm-size=4g $(REPO_IMAGE) sh

.PHONY: shell/run
shell/run:
	@$(DOCKER_HERE) --memory=8g --memory-swap=8g --shm-size=4g $(REPO_IMAGE) make run ONLY=$(ONLY) GEO=$(GEO)

.PHONY: shell/generate
shell/generate:
	@$(DOCKER_HERE) --memory=8g --memory-swap=8g --shm-size=4g $(REPO_IMAGE) make generate ONLY=$(ONLY)

.PHONY: shell/tier
shell/tier:
	@$(DOCKER_HERE)    \
		--memory=8g      \
		--memory-swap=8g \
		--shm-size=4g    \
		$(REPO_IMAGE)    \
		make tier RELEVANCE=$(RELEVANCE) GEO=$(GEO)

.PHONY: shell/valuation
shell/valuation:
	@$(DOCKER_HERE)    \
		--memory=8g      \
		--memory-swap=8g \
		--shm-size=4g    \
		$(REPO_IMAGE)    \
		make valuation GEO=$(GEO)

.PHONY: shell/dividends
shell/dividends:
	@$(DOCKER_HERE)    \
		--memory=8g      \
		--memory-swap=8g \
		--shm-size=4g    \
		$(REPO_IMAGE)    \
		make dividends GEO=$(GEO)

.PHONY: shell/dfcf
shell/dfcf:
	@$(DOCKER_HERE)    \
		--memory=8g      \
		--memory-swap=8g \
		--shm-size=4g    \
		$(REPO_IMAGE)    \
		make dfcf GEO=$(GEO) SYMBOL=$(SYMBOL)

.PHONY: shell/cleanup
shell/cleanup:
	@$(DOCKER_HERE)    \
		--memory=8g      \
		--memory-swap=8g \
		--shm-size=4g    \
		$(REPO_IMAGE)    \
		make cleanup GEO=$(GEO)

.PHONY: shell/quick
shell/quick:
	@$(DOCKER_HERE)    \
		--memory=8g      \
		--memory-swap=8g \
		--shm-size=4g    \
		$(REPO_IMAGE)    \
		make quick GEO=$(GEO)

#-------------------------------------------------------------------------------
# Ruby

.PHONY: console
console:
	@ruby bin/console

.PHONY: run
run:
	@ruby bin/run ONLY=$(ONLY) GEO=$(GEO)

.PHONY: generate
generate:
	@ruby bin/generate $(ONLY)

.PHONY: tier
tier:
	@ruby ./scripts/tier/main.rb RELEVANCE=$(RELEVANCE) GEO=$(GEO)

.PHONY: valuation
valuation:
	@ruby ./scripts/valuation/main.rb $(GEO)

.PHONY: dividends
dividends:
	@ruby bin/dividends $(GEO)

.PHONY: dfcf
dfcf:
	@ruby ./scripts/dfcf/main.rb GEO=$(GEO) SYMBOL=$(SYMBOL)

.PHONY: cleanup
cleanup:
	@ruby ./scripts/cleanup/main.rb $(GEO)

.PHONY: quick
quick:
	@ruby ./scripts/quick/main.rb $(GEO)
