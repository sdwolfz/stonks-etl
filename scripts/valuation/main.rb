# frozen_string_literal: true

require 'fileutils'
require 'pp'
require 'yaml'

require_relative 'lib/exporter'
require_relative 'lib/reconciler'

exporter   = Exporter.new
reconciler = Reconciler.new

geographies = ARGV[0]&.split(',') || []

data = {}

geographies.each do |geography|
  files = Dir.glob("./data/valuation/#{geography}/*/*/*")

  files.each do |file|
    parts   = file.split('/')
    symbol  = parts[4]
    year    = parts[5]
    quarter = parts[6].split('.')[0]

    raw  = File.read(file)
    yml  = YAML.safe_load(raw)

    data[geography] = {} unless data[geography]
    data[geography][symbol] = {} unless data[geography][symbol]
    data[geography][symbol][year] = {} unless data[geography][symbol][year]
    data[geography][symbol][year][quarter] = yml

    begin
      reconciler.call(yml)
    rescue Reconciler::ReconcilerError => e
      puts file
      pp yml
      puts e.message

      exit 1
    end
  end
end

# exporter.call(data)
