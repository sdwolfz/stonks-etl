# frozen_string_literal: true

class Reconciler
  class ReconcilerError < StandardError; end

  def call(data)
    reconcile_balance(data['balance'])
    reconcile_revenue(data['revenue'])
    reconcile_cashflow(data['cashflow'])
  end

  private

  # ----------------------------------------------------------------------------
  # Balance

  def reconcile_balance(balance)
    assets      = balance['assets']
    equity      = balance['equity']
    liabilities = balance['liabilities']

    noncurrent = assets['noncurrent']
    current    = assets['current']
    total      = assets['total']
    reconcile_total('assets', total, noncurrent, current)

    noncurrent = liabilities['noncurrent']
    current    = liabilities['current']
    total      = liabilities['total']
    reconcile_total('liabilities', total, noncurrent, current)

    reconcile_total('balance', assets['total'], equity['total'], liabilities['total'])
  end

  # ----------------------------------------------------------------------------
  # Revenue

  def reconcile_revenue(revenue)
    sales              = revenue['sales']
    operating_expenses = revenue['operating_expenses'] * -1
    operating_profit   = revenue['operating_profit']
    reconcile_total('sales', sales, operating_expenses, operating_profit)

    financial_resut     = revenue['financial_resut'] * -1
    profit_before_taxes = revenue['profit_before_taxes']
    reconcile_total('financial', operating_profit, financial_resut, profit_before_taxes)

    tax_on_gains = revenue['tax_on_gains'] * -1
    net_profit   = revenue['net_profit']
    reconcile_total('tax', profit_before_taxes, tax_on_gains, net_profit)

    other        = revenue['other']
    total_result = revenue['total_result']
    reconcile_total('result', total_result, net_profit, other)
  end

  # ----------------------------------------------------------------------------
  # Cashflow

  def reconcile_cashflow(cashflow)
    operating  = cashflow['operating']
    investment = cashflow['investment']
    financing  = cashflow['financing']
    total      = cashflow['total']

    reconcile_total('flow', total, operating, investment, financing)

    cash_at_start = cashflow['cash_at_start']
    cash_at_end   = cashflow['cash_at_end']

    reconcile_total('cash', cash_at_end, cash_at_start, total)

    capex = cashflow['capex']
    fcf   = cashflow['fcf']
    reconcile_total('fcf', fcf, operating, capex)
  end

  # ----------------------------------------------------------------------------
  # Generic

  ALLOWED_ERROR = 1.1e-5

  def reconcile_total(id, total, *rest)
    right = 0
    rest.each { |value| right += value }

    result = total - right
    max    = (rest + [total]).map(&:abs).max
    error  = !max.zero? ? result.abs.to_f / max : 0
    return if error < ALLOWED_ERROR

    message = [
      "[#{id}] Sum does not reconcile: #{total} != #{rest.join(' + ')}",
      "Got: #{right}",
      "Error: #{error}",
      "Delta: #{-result}"
    ].join(' | ')

    raise ReconcilerError, message
  end
end
