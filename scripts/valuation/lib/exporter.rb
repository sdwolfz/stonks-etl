# frozen_string_literal: true

require 'erb'
require 'fileutils'

# Create a Hugo friendly markdown file with report data.
class Exporter
  TEMPLATE = File.read('./scripts/valuation/templates/page.md.erb')

  def call(data)
    pp data

    data.each_pair do |geography, geography_data|
      pp geography

      geography_data.each_pair do |symbol, symbol_data|
        pp symbol

        renderer = ERB.new(TEMPLATE, trim_mode: '-')
        result   = renderer.result(binding)
        path     = File.join('scripts', 'valuation', 'result', geography, "#{symbol}.md")

        FileUtils.mkdir_p(File.dirname(path))
        File.write(path, result)

        command = "docker-here sdwolfz/pandoc pandoc #{path} -o #{path}.html"
        pp command

        `#{command}`
      end
    end
  end

  def format(number)
    sign = number.negative? ? '-' : ''
    sign + number.abs.to_s.gsub(/\B(?=(...)*\b)/, ',')
  end
end
