#!/usr/bin/env sh

# 4x4
tmux split-window -h

tmux select-pane -t 0
tmux split-window -v
tmux select-pane -t 2
tmux split-window -v

tmux select-pane -t 0
tmux split-window -v
tmux select-pane -t 2
tmux split-window -v
tmux select-pane -t 4
tmux split-window -v
tmux select-pane -t 6
tmux split-window -v

tmux select-pane -t 1
tmux split-window -h
tmux select-pane -t 3
tmux split-window -h
tmux select-pane -t 5
tmux split-window -h
tmux select-pane -t 7
tmux split-window -h
tmux select-pane -t 9
tmux split-window -h

# Calendar
tmux send-keys -t 1 'time make shell/run ONLY=calendar GEO=RO' Enter
tmux send-keys -t 2 'time make shell/run ONLY=calendar GEO=UK' Enter
tmux send-keys -t 3 'time make shell/run ONLY=calendar GEO=US' Enter

# Dividend
tmux send-keys -t 4 'time make shell/run ONLY=dividend GEO=RO' Enter
tmux send-keys -t 5 'time make shell/run ONLY=dividend GEO=UK' Enter
tmux send-keys -t 6 'time make shell/run ONLY=dividend GEO=US' Enter

# Metadata
tmux send-keys -t 7 'time make shell/run ONLY=metadata GEO=RO' Enter
tmux send-keys -t 8 'time make shell/run ONLY=metadata GEO=UK' Enter
tmux send-keys -t 9 'time make shell/run ONLY=metadata GEO=US' Enter

# Price
tmux send-keys -t 10 'time make shell/run ONLY=price GEO=RO' Enter
tmux send-keys -t 11 'time make shell/run ONLY=price GEO=UK' Enter
tmux send-keys -t 12 'time make shell/run ONLY=price GEO=US' Enter

# Main
tmux select-pane -t 0
tmux send-keys -t 0 'reset' Enter
