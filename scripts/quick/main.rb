# frozen_string_literal: true

require 'erb'
require 'fileutils'
require 'yaml'

require 'pry'

require './lib/data_joiner'

geo = ARGV[0]

existing   = Dir.glob("./data/registry/#{geo}/*")
discovered = existing.map { |path| path.split('/')[-1] }

@template = File.join('scripts', 'tier', 'template.yml.erb')
@raw      = File.read(@template)
@renderer = ERB.new(@raw, trim_mode: '-')

count = 0

discovered.each do |symbol|
  # paths = Dir.glob("./data/registry/#{geo}/#{symbol}/{2018,2019,2020,2021}")
  # next unless paths.empty?

  tier_path = File.join('data', 'registry', geo, symbol, 'tier.yml')
  content   = YAML.safe_load(File.read(tier_path))

  if content['tier'] == 'D'
    payload = {
      symbol:    content['symbol'],
      relevance: '2021Q1',
      tier:      'D',
      reason:    content['reason']
    }

    result = @renderer.result(binding)
    File.write(tier_path, result)

    count += 1
  end
end

puts "Processed: #{count}"
