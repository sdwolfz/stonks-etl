# frozen_string_literal: true

require 'erb'
require 'json'
require 'yaml'

require 'pry'

# ------------------------------------------------------------------------------
# Args

args = { relevance: nil, geo: nil }
ARGV.each do |arg|
  if (match = /^RELEVANCE=(.*?)$/.match(arg))
    if match[1] != ''
      args[:relevance] = match[1]
    else
      args[:relevance] = 'preliminary'
    end
  end

  if (match = /^GEO=(.*?)$/.match(arg))
    if match[1] != ''
      args[:geo] = match[1].split(',')
    else
      args[:geo] = ['RO', 'UK', 'US']
    end
  end
end

class Tier
  TIERS = ['S', 'A', 'B', 'C', 'D', 'F', 'U'].freeze

  def initialize
    @template = File.join('scripts', 'tier', 'template.yml.erb')
    @raw      = File.read(@template)
    @renderer = ERB.new(@raw, trim_mode: '-')

    @data       = {}
    @relevances = []
  end

  def call(geographies, relevance)
    geographies.each do |geography|
      process_geo(geography, relevance)
    end

    present_data(@data)
  end

  private

  def process_symbol(element, geography, relevance)
    symbol_path = File.join('./data/registry', geography, element['symbol'])
    unless File.exist?(symbol_path)
      puts "  #{element['symbol']}: No registry data for symbol!"

      return
    end

    tier_path = File.join(symbol_path, 'tier.yml')
    unless File.exist?(tier_path)
      create_tier_file(element, geography, tier_path)

      return
    end

    content = File.read(tier_path)
    payload = YAML.safe_load(content)

    if @data.key?(geography)
      @data[geography][element['symbol']] = payload
    else
      @data[geography] = {
        element['symbol'] => payload
      }
    end

    unless @relevances.include?(@data[geography][element['symbol']]['relevance'])
      @relevances << @data[geography][element['symbol']]['relevance']
    end

    unless @data[geography][element['symbol']]['relevance'] == relevance
      @data[geography][element['symbol']]['tier'] = 'U'
    end
  end

  def create_tier_file(element, geography, tier_path)
    payload = {
      'symbol'    => element['symbol'],
      'relevance' => 'preliminary',
      'tier'      => 'U'
    }
    if @data.key?(geography)
      @data[geography][element['symbol']] = payload
    else
      @data[geography] = {
        element['symbol'] => payload
      }
    end
    result = @renderer.result(binding)

    File.write(tier_path, result)
  end

  def process_geo(geography, relevance)
    puts '###########################'
    print geography

    path = File.join('artifacts', 'symbol', "#{geography}.json")

    raw = File.read(path)
    parsed = JSON.parse(raw)

    @total = parsed.size
    puts " (#{@total})"
    puts '###########################'

    parsed.each do |element|
      process_symbol(element['value'], geography, relevance)
    end
  end

  def present_data(data)
    data.each_pair do |geography, shares|
      puts '***************************'
      puts geography
      puts '***************************'

      tiers = {
        'S' => [],
        'A' => [],
        'B' => [],
        'C' => [],
        'D' => [],
        'F' => [],
        'U' => []
      }

      count = 0

      shares.each_pair do |_, payload|
        tier   = payload['tier']
        symbol = payload['symbol']

        unless TIERS.include?(tier)
          puts "Unknow tier: #{tier} for symbol: #{symbol}"
        end

        tiers[tier] << symbol
      end

      tiers.each_pair do |tier, elements|
        puts '---------------------------'
        puts "#{tier} (#{elements.size})"
        puts elements.sort.join(' ')

        count += elements.size
      end

      puts '---------------------------'
      puts "Processed: #{count}/#{@total}"

      puts '---------------------------'
      puts "Available Periods: #{@relevances.join(', ')}"
    end
  end
end

Tier.new.call(args[:geo], args[:relevance])
